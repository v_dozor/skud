﻿using ARDevGroup.VotumHardware.Core;
using ARDevGroup.VotumHardware.Core.Abstracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Diagnostics;

namespace DozorUsbLib
{
    /// <summary>
    /// Singleton class for RFID cards reading 
    /// </summary>
    public class RfidReader 
    {
        private Devices devices;
        /// <summary>
        /// Rfid update event
        /// </summary>
        public event EventHandler<RfidReaderEventArgs> Rfid_Updated;
        public event EventHandler DiveceAttached;

        private void OnDiveceAttached(object sender, EventArgs e)
        {
            if(DiveceAttached == null)
                return;
            DiveceAttached(sender, e);
        }

        public RfidReader()
        {
            devices = new Devices();

            //Command's waiting counter 0.2 sec * 5 = 1 sec. 
            //By default = 5
            ARDevGroup.VotumHardware.Core.UsbDeviceReciever.HidWaitMaxCounter = 5;

            try
            {
                devices.SearchDevices();
                devices.OpenDevices();
                devices.DataRecieved += dataReceived;
                devices.DeviceAttached += Devices_DeviceAttached;
            }
            catch(Exception ex)
            {
                try
                {
                    DateTime eventTime = DateTime.Now;
                    File.AppendAllText("errors_readers_update_thread.txt", "\r\n" + eventTime.ToString() + " " + ex.Message + " " + ex.StackTrace + "\r\n");
                }
                catch (Exception ex1)
                {

                }
            }            
        }

        private void Devices_DeviceAttached(object sender, EventArgs e)
        {
            OnDiveceAttached(sender, e);
        }

        private void dataReceived(object sender, DeviceDataArgs deviceEventArgs)
        {
            //определяю с какого устройства пришло сообщение
            var device = devices.DevicesList.FirstOrDefault(
                x => 
                {
                    return x.DeviceID == deviceEventArgs.Data.DeviceID;
                }
                );
            try
            {
                RfidReaderEventArgs eventArgs = new RfidReaderEventArgs(
                    deviceEventArgs.Data.ButtonsCodes,
                    Convert.ToInt32(deviceEventArgs.Data.DeviceID),
                    deviceEventArgs.Data.SerialId,
                    device.DeviceID ?? XmlConvert.EncodeName(device.PortName)
                    );
                Rfid_Updated(this, eventArgs);
            }
            catch (Exception ex)
            {
                try
                {
                    DateTime eventTime = DateTime.Now;
                    File.AppendAllText("errors_readers_update_thread.txt", "\r\n" + eventTime.ToString() + " " + ex.Message + " " + ex.StackTrace + "\r\n");
                }
                catch (Exception ex1)
                {

                }
            }
        }

        /// <summary>
        /// Closes all devices
        /// </summary>
        public void CloseDevices()
        {
            devices.DataRecieved -= dataReceived;
            devices.CloseDevices();
        }

        public void SearchDevices()
        {
            devices.SearchDevices();
            devices.OpenDevices();
        }

        private string hubIdMatch = string.Empty;
        public string HubIdMatch
        {
            get
            {
                return hubIdMatch;
            }
            set
            {
                hubIdMatch = value;
            }
        }

    }
}
