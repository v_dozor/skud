﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DozorUsbLib
{
    public class RfidReaderEventArgs : EventArgs
    {
        public String Rfid { get; internal set; }
        public Int32 ReceiverId { get; internal set; }
        public String PortName { get; internal set; }
        public int SerialId { get; internal set; }

        public RfidReaderEventArgs(string rfid, int receiverId, int serialId, string portName)
        {
            Rfid = rfid;
            ReceiverId = receiverId;
            PortName = portName;
            SerialId = serialId;
        }
    }
}
