﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKUDDatabase.DataModels
{
    /// <summary>
    /// Модель данных таблицы проходов
    /// </summary>
    public class Gate : IEquatable<Gate>
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long ControllerId { get; set; }

        public Gate() { }

        public Gate(long _Id, string _Name, long _ControllerId)
        {
            Id = _Id;
            Name = _Name;
            ControllerId = _ControllerId;
        }

        public bool Equals(Gate other)
        {
            if (other == null)
                return false;
            return this.Id == other.Id &&
                   this.Name == other.Name &&
                   this.ControllerId == other.ControllerId;
        }
    }
}
