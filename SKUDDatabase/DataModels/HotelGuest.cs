﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKUDDatabase.DataModels
{
    public class HotelGuest
    {
        public long Id { get; set; }
        public long RfidLabelId { get; set; }
        public string HotelId { get; set; }

        public HotelGuest()
        {

        }

        public HotelGuest(long _Id, long _RfidLabelId, string _HotelId)
        {
            Id = _Id;
            RfidLabelId = _RfidLabelId;
            HotelId = _HotelId;
        }
    }
}
