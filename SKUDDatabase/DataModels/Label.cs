﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKUDDatabase.DataModels
{
    /// <summary>
    /// Модель данных таблицы меток
    /// </summary>
    public class Label
    {
        public long Id { get; set; }
        public string Rfid6 { get; set; }
        public string Rfid7 { get; set; }
        public string TimeAllowedFrom { get; set; }
        public string TimeAllowedUntil { get; set; }

        public Label()
        {

        }

        public Label(long _Id, string _Rfid6, string _Rfid7, string _TimeAllowedFrom = "00:00", string _TimeAllowedUntil = "00:00")
        {
            Id = _Id;
            Rfid6 = _Rfid6;
            Rfid7 = _Rfid7;
            TimeAllowedFrom = _TimeAllowedFrom;
            TimeAllowedUntil = _TimeAllowedUntil;
        }
    }
}
