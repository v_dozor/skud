﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKUDDatabase.DataModels
{
    /// <summary>
    /// Модель данных таблицы логов
    /// </summary>
    public class Log : IEquatable<Log>
    {
        public int Id { get; set; }
        public string Rfid6 { get; set; }
        public string Serial { get; set; }
        public int EventType { get; set; }
        public string EventTypeString { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Direction { get; set; }
        public int RfidReaderId { get; set; }

        public Log()
        {

        }

        public Log(int id, string rfid, string serial, int event_type, string event_type_string, DateTime date, string description, string direction)
        {
            this.Id = id;
            this.Rfid6 = rfid;
            this.Serial = serial;
            this.EventType = event_type;
            this.EventTypeString = event_type_string;
            this.Date = date;
            this.Description = description;
            this.Direction = direction;
        }

        public bool Equals(Log other)
        {
            if (other.Rfid6 == Rfid6 &&
                other.Serial == Serial &&
                other.EventType == EventType &&
                other.EventTypeString == EventTypeString &&
                other.Date == Date &&
                other.Description == Description &&
                other.Direction == Direction)
                return true;
            return false;
        }
    }
}
