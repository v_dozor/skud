﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKUDDatabase.DataModels
{
    public class CsvPerson
    {
        public int Cid { get; set; }
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string Rfid { get; set; }

        public CsvPerson()
        {

        }


        public CsvPerson(int Cid, string Name, string SecondName, string LastName, string Rfid)
        {
            this.Cid = Cid;
            this.Name = Name;
            this.SecondName = SecondName;
            this.LastName = LastName;
            this.Rfid = Rfid;
        }
    }
}
