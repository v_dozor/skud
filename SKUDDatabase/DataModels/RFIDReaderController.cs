﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKUDDatabase.DataModels
{
    /// <summary>
    /// Модель данных таблицы контроллеров для считывателей RFID-меток
    /// </summary>
    public class RFIDReaderController : IEquatable<RFIDReaderController>
    {
        public int Id { get; set; }
        public string SerialNumber { get; set; }
        public string Ip { get; set; }
        public int Port { get; set; }

        public RFIDReaderController()
        {

        }

        public RFIDReaderController(int _Id, string _SerialNumber, string _Ip, int _Port)
        {
            Id = _Id;
            SerialNumber = _SerialNumber;
            Ip = _Ip;
            Port = _Port;
        }

        public bool Equals(RFIDReaderController other)
        {
            if (other == null)
                return false;
            return this.Id == other.Id &&
                   this.SerialNumber == other.SerialNumber &&
                   this.Ip == other.Ip &&
                   this.Port == other.Port;
        }
    }
}
