﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKUDDatabase
{
    public static class DbConstants
    {
        #region Tables

        public static String GatesTableName = "GATES_TABLE";
        public static String LabelsTableName = "LABELS_TABLE";
        public static String LogTableName = "LOG_TABLE";
        public static String ReadersGatesTable = "READERS_GATES_TABLE";
        public static String RfidReadersTable = "RFID_READERS_TABLE";

        #endregion

        #region Columns

        #endregion
    }
}
