﻿using MySql.Data.MySqlClient;
using SKUDDatabase.DataModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Votum.Types;

namespace SKUDDatabase
{
    public class SKUDDatabaseConnection
    {
        private MySqlConnection connection;
        public MySqlConnection Connection
        {
            get { return connection; }
            private set { }
        }

        public SKUDDatabaseConnection(String ip, int port = 3306, bool isLocalhost = false)
        {
            String server = isLocalhost ? "localhost" : ip;
            String databaseName = "skud";
            String user = "skud";
            String password = "Votum_SKUD-1";
            String connectionString = "SERVER=" + server + ";" + "PORT=" + port.ToString() + ";" + "DATABASE=" +
            databaseName + ";" + "UID=" + user + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
        }

        //Открывает соединение с БД
        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        //MessageBox.Show("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        //MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        /// <summary>
        /// Закрывает соединение
        /// </summary>
        /// <returns></returns>
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {                
                return false;
            }
        }

        /// <summary>
        /// Операция добавления записи в БД 
        /// </summary>
        /// <param name="query">Запрос</param>
        public long Insert(String query)
        {
            long res;
            
            if (this.OpenConnection() == true)
            {
                try
                {
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    cmd.ExecuteNonQuery();
                    res = cmd.LastInsertedId;

                    this.CloseConnection();
                    return res;
                }
                catch(Exception ex)
                {
                    this.CloseConnection();
                    throw ex;
                }
            }
            return -1;            
        }

        /// <summary>
        /// Операция обновления записи в БД
        /// </summary>
        /// <param name="query">Запрос</param>
        public void Update(String query)
        {            
            if (this.OpenConnection() == true)
            {                
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandText = query;
                cmd.Connection = connection;

                cmd.ExecuteNonQuery();

                this.CloseConnection();
            }
        }

        /// <summary>
        /// Операция удаления записи из БД
        /// </summary>
        /// <param name="query">Запрос</param>
        public void Delete(String query)
        {
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();
                this.CloseConnection();
            }
        }

        public ObservableCollection<Log> GetLogs(DateTime dateStart, DateTime dateEnd, string[] rfids6)
        {
            string rifdWhereClause = "";
            foreach(string item in rfids6)
                rifdWhereClause += string.Format("'{0}',", item);
            if(rifdWhereClause.Length == 0)
            {
                return null;
            }
            rifdWhereClause = rifdWhereClause.Remove(rifdWhereClause.Length - 1, 1);
            string request = string.Format(
                "SELECT log_table.ID as ID, log_table.DATE as DATE, log_table.DIRECTION as DIRECTION, labels_table.RFID6 as RFID6, log_table.RFID_READER_ID as RFID_READER_ID " +
                "FROM log_table " +
                "JOIN labels_table ON log_table.LABEL_ID = labels_table.ID AND " +
                "log_table.DATE >= '{0}' AND log_table.DATE <= '{1}' " +
                "WHERE labels_table.RFID6 IN ({2})", 
                dateStart.ToString("yyyy-MM-dd HH:mm:ss"),
                dateEnd.ToString("yyyy-MM-dd HH:mm:ss"),
                rifdWhereClause);

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(request, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                ObservableCollection<Log> logs = new ObservableCollection<Log>();
                while (dataReader.Read())
                {
                    Log log = new Log();

                    log.Id = (int)dataReader["ID"];
                    log.Rfid6 = (string)dataReader["RFID6"];
                    log.Direction = (string)dataReader["DIRECTION"];
                    log.Date = (DateTime)dataReader["DATE"];
                    log.RfidReaderId = (int)dataReader["RFID_READER_ID"];
                    logs.Add(log);
                }
                this.CloseConnection();
                return logs;
            }
            return null;
        }

        public IEnumerable<MySqlDataReader> GetGuestsByIds(DateTime dateStart, DateTime dateEnd, string[] ids)
        {
            List<HotelGuest> geusts = new List<HotelGuest>();
            string rifdWhereClause = "";
            foreach (string item in ids)
                rifdWhereClause += string.Format("'{0}',", item);
            if (rifdWhereClause.Length == 0)
            {
                yield break;
            }
            rifdWhereClause = rifdWhereClause.Remove(rifdWhereClause.Length - 1, 1);

            string request = string.Format(
                "SELECT log_table.DIRECTION as DIRECTION, hotel_guests_table.HOTEL_ID as HOTEL_ID, log_table.DATE as DATE " +
                "FROM hotel_guests_table, log_table " +
                "JOIN labels_table ON log_table.LABEL_ID = labels_table.ID AND " +
                "log_table.DATE >= '{0}' AND log_table.DATE <= '{1}' " +
                "WHERE hotel_guests_table.HOTEL_ID IN ({2}) ORDER BY HOTEL_ID",
                dateStart.ToString("yyyy-MM-dd HH:mm:ss"),
                dateEnd.ToString("yyyy-MM-dd HH:mm:ss"),
                rifdWhereClause);

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(request, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                ObservableCollection<Log> logs = new ObservableCollection<Log>();
                while (dataReader.Read())
                {
                    yield return dataReader;
                }
                this.CloseConnection();
                yield break;
            }

            yield break;
        }

        #region SELECT

        /// <summary>
        /// Возвращает коллекцию контроллеров для считывателей меток
        /// </summary>
        /// <returns></returns>
        public ObservableCollection<RFIDReaderController> GetRfidReaderControllers()
        {
            string query = "SELECT * FROM RFID_READERS_TABLE";

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                ObservableCollection<RFIDReaderController> controllers = new ObservableCollection<RFIDReaderController>();
                while (dataReader.Read())
                {
                    RFIDReaderController controller = new RFIDReaderController();
                    controller.Id = (int)dataReader["ID"];
                    controller.SerialNumber = (String)dataReader["SERIAL_NUMBER"];
                    controller.Ip = (String)dataReader["IP"];
                    controller.Port = (int)dataReader["PORT"];
                    controllers.Add(controller);
                }
                this.CloseConnection();
                return controllers;
            }
            return null;
        }

        /// <summary>
        /// Возвращает коллекцию всех проходных
        /// </summary>
        /// <returns></returns>
        public ObservableCollection<Gate> GetGates()
        {
            string query = "SELECT * FROM GATES_TABLE";

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                ObservableCollection<Gate> gates = new ObservableCollection<Gate>();
                while (dataReader.Read())
                {
                    Gate gate = new Gate();
                    gate.Id = (int)dataReader["ID"];
                    gate.Name = (String)dataReader["NAME"];
                    gate.ControllerId = (int)dataReader["RFID_READER_ID"];
                    gates.Add(gate);
                }
                this.CloseConnection();
                return gates;
            }
            return null;
        }

        /// <summary>
        /// Возвращает коллекцию проходных, в Белом списке которых присутствует заданная метка
        /// </summary>
        /// <param name="rfidLabel">Метка, по которой осуществляется отбор проходных</param>
        /// <returns></returns>
        public ObservableCollection<Gate> GetGatesByRfidLabel(RfidLabelZ5R rfidLabel)
        {
            // получаем коллекцию всех проходных
            ObservableCollection<Gate> allGates = GetGates();
            if (allGates == null)
                return null;
            if (allGates.Count == 0)
                return allGates;

            // получаем ID контроллеров, в белом списке которых присутствует метка
            int labelId = IsLabelExist(rfidLabel.IdString);
            string query = "SELECT * FROM LABELS_READERS_TABLE WHERE LABEL_ID='" + labelId + "'";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                ObservableCollection<Gate> gates = new ObservableCollection<Gate>();
                while(dataReader.Read())
                {
                    int rfidReaderId = (int) dataReader["RFID_READER_ID"];

                    // добавляем в коллекцию проходные с данным контроллером
                    foreach(Gate gate in allGates)
                    {
                        if(!gates.Contains(gate))
                        {
                            if(gate.ControllerId == rfidReaderId)
                            {
                                gates.Add(gate);
                            }
                        }
                    }
                }                
                this.CloseConnection();
                return gates;
            }
            return null;
        }

        /// <summary>
        /// Возвращает коллекцию всех проходных
        /// </summary>
        /// <returns></returns>
        public RFIDReaderController GetControllerById(long id)
        {
            string query = "SELECT * FROM RFID_READERS_TABLE WHERE ID=" + id;

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                RFIDReaderController controller = new RFIDReaderController();
                while (dataReader.Read())
                {
                    controller.Id = (int)dataReader["ID"];
                    controller.Ip = (String)dataReader["IP"];
                    controller.Port = (int)dataReader["PORT"];
                    controller.SerialNumber = (String)dataReader["SERIAL_NUMBER"];
                }
                this.CloseConnection();
                return controller;
            }
            return null;
        }

        /// <summary>
        /// Проверяет наличие метки с указанным ID в базе данных
        /// </summary>
        /// <param name="Rfid"></param>
        /// <returns></returns>
        public int IsLabelExist(String Rfid, LabelTypeZ5R labelType = LabelTypeZ5R.bytes7)
        {
            String rfidType = "RFID7";
            if(labelType == LabelTypeZ5R.bytes6)
            {
                rfidType = "RFID6";
            }
            string query = "SELECT ID FROM LABELS_TABLE WHERE " + rfidType + "='" + Rfid + "'";

            int id = -1;

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                while(dataReader.Read())
                {
                    id = (int)dataReader["ID"];
                }

                this.CloseConnection();
            }
            return id;
        }

        /// <summary>
        /// Возвращает метку по ее идентификатору в таблице меток
        /// </summary>
        /// <param name="id">ID метки</param>
        /// <returns></returns>
        public Label GetRfidById(long id)
        {
            string query = "SELECT * FROM LABELS_TABLE WHERE ID=" + id;

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                Label label = new Label();
                while (dataReader.Read())
                {
                    label.Id = (int)dataReader["ID"];
                    label.Rfid6 = (string)dataReader["RFID6"];
                    label.Rfid7 = (string)dataReader["RFID7"];
                    label.TimeAllowedFrom = (string)dataReader["TIME_ALLOWED_FROM"];
                    label.TimeAllowedUntil = (string)dataReader["TIME_ALLOWED_UNTIL"];
                }
                this.CloseConnection();
                return label;
            }
            return null;
        }

        /// <summary>
        /// Проверяет, установлены ли контроллер на проходе
        /// </summary>
        /// <param name="gateId"></param>
        /// <returns></returns>
        public bool IsControllerInstalledOnGate(long controllerId)
        {
            string query = "SELECT COUNT(*) FROM GATES_TABLE WHERE RFID_READER_ID='" + controllerId + "'";

            int Count = -1;

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);

                Count = int.Parse(cmd.ExecuteScalar() + "");

                this.CloseConnection();

                if (Count > 0)
                {
                    return true;
                }
            }
            return false;
        }        

        /// <summary>
        /// Возвращает коллекцию проходных, которыми управляет контроллер с заданным ID
        /// </summary>
        /// <param name="gateId">ID контроллера</param>
        /// <returns></returns>
        public ObservableCollection<Gate> GetGatesByControllerId(long controllerId)
        {
            string query = "SELECT * FROM RFID_READERS_TABLE WHERE ID=" + controllerId;

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                ObservableCollection<Gate> gates = new ObservableCollection<Gate>();
                while (dataReader.Read())
                {
                    Gate gate = new Gate();
                    gate.Id = (int)dataReader["ID"];
                    gate.Name = (String)dataReader["NAME"];
                    gate.ControllerId = controllerId;
                    gates.Add(gate);
                }
                this.CloseConnection();
                return gates;
            }
            return null;
        }

        public ObservableCollection<Log> GetLogs()
        {
            string sql = "SELECT ID, (SELECT RFID FROM labels_table WHERE labels_table.ID = log_table.LABEL_ID) as RFID, DIRECTION FROM log_table";

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(sql, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                ObservableCollection<Log> logs = new ObservableCollection<Log>();
                while (dataReader.Read())
                {
                    Log log = new Log();
                    
                    log.Id = (int)dataReader["ID"];
                    log.Rfid6 = (string)dataReader["RFID"];
                    log.Direction = (string)dataReader["DIRECTION"];
                    logs.Add(log);
                }
                this.CloseConnection();
                return logs;
            }
            return null;

        }

        /// <summary>
        /// Возвращает список гостей отеля
        /// </summary>
        /// <returns></returns>
        public ObservableCollection<HotelGuest> GetHotelGuests()
        {
            string query = "SELECT * FROM HOTEL_GUESTS_TABLE";

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                ObservableCollection<HotelGuest> hotelGuests = new ObservableCollection<HotelGuest>();
                while (dataReader.Read())
                {
                    HotelGuest hotelGuest = new HotelGuest();
                    hotelGuest.Id = (int)dataReader["ID"];
                    hotelGuest.RfidLabelId = (int)dataReader["RFID_LABEL_ID"];
                    hotelGuest.HotelId = (string)dataReader["HOTEL_ID"];
                    hotelGuests.Add(hotelGuest);
                }
                this.CloseConnection();
                return hotelGuests;
            }
            return null;
        }

        /// <summary>
        /// Возвращает гостя отеля по его RFID
        /// </summary>
        /// <param name="rfidId"></param>
        /// <returns></returns>
        public HotelGuest GetHotelGuestByRfidId(long rfidId)
        {
            string query = "SELECT * FROM HOTEL_GUESTS_TABLE WHERE RFID_LABEL_ID=" + rfidId;

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                HotelGuest hotelGuest = null;
                while (dataReader.Read())
                {
                    hotelGuest = new HotelGuest();
                    hotelGuest.Id = (int)dataReader["ID"];
                    hotelGuest.RfidLabelId = (int)dataReader["RFID_LABEL_ID"];
                    hotelGuest.HotelId = (string)dataReader["HOTEL_ID"];
                }
                this.CloseConnection();
                return hotelGuest;
            }
            return null;
        }

        /// <summary>
        /// Возвращает гостя отеля по его номеру в учетной книге гостиницы
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns></returns>
        public HotelGuest GetHotelGuestByHotelId(string hotelId)
        {
            string query = "SELECT * FROM HOTEL_GUESTS_TABLE WHERE HOTEL_ID='" + hotelId + "'";

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();

                HotelGuest hotelGuest = null;
                while (dataReader.Read())
                {
                    hotelGuest = new HotelGuest();
                    hotelGuest.Id = (int)dataReader["ID"];
                    hotelGuest.RfidLabelId = (int)dataReader["RFID_LABEL_ID"];
                    hotelGuest.HotelId = (string)dataReader["HOTEL_ID"];
                }
                this.CloseConnection();
                return hotelGuest;
            }
            return null;
        }

        #endregion

        #region INSERT

        public bool AddPersonRange(IEnumerable<CsvPerson> persons)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("INSERT IGNORE INTO persons_table(CID, NAME, SECOND_NAME, LAST_NAME, RFID) VALUES" + Environment.NewLine);

            foreach(CsvPerson person in persons)
            {
                sql.Append("(");
                sql.Append(person.Cid);
                sql.Append(", ");
                sql.Append("'" + person.Name + "'");
                sql.Append(", ");
                sql.Append("'" + person.SecondName + "'");
                sql.Append(", ");
                sql.Append("'" + person.LastName + "'");
                sql.Append(", ");
                sql.Append("(SELECT id FROM labels_table WHERE labels_table.RFID = '" + person.Rfid + "')");
                sql.Append(")," + Environment.NewLine);
            }
            sql.Remove(sql.Length - 3, 2);
            Insert(sql.ToString());

            return true;
        }

        public bool AddRfidRange(IEnumerable<Label> labels)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("INSERT IGNORE INTO " + DbConstants.LabelsTableName + " (RFID7) VALUES " + Environment.NewLine);
            foreach(Label label in labels)
            {
                sql.Append("('" + label.Rfid7 + "')," + Environment.NewLine);
            }
            //remove comma
            sql.Remove(sql.Length - 3, 2);
            Insert(sql.ToString());
            return true;
        }

        public void AddLogRange(IEnumerable<Log> events)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("INSERT INTO log_table " + 
                " (LABEL_ID, RFID_READER_ID, EVENT_TYPE, EVENT_TYPE_STRING, DATE, DESCRIPTION, DIRECTION)" + 
                " VALUES ");
            foreach(var ev in events)
                sql.AppendFormat(
                        "((SELECT ID FROM labels_table WHERE labels_table.RFID6 = '{0}')," +
                        " (SELECT ID FROM rfid_readers_table WHERE rfid_readers_table.SERIAL_NUMBER = '{1}')," + 
                        " {2}, '{3}', '{4}', '{5}', '{6}'),",
                    ev.Rfid6,
                    ev.Serial,
                    ev.EventType,
                    ev.EventTypeString,
                    ev.Date.ToString("yyyy-MM-dd HH:mm:ss"),
                    ev.Description,
                    ev.Direction);
            sql.Remove(sql.Length - 1, 1);
            Insert(sql.ToString());
        }

        /// <summary>
        /// Добавляет запись в таблицу меток, если метки с таким rfid еще нет
        /// </summary>
        /// <param name="rfid">RFID метки</param>
        /// <param name="timeAllowedFrom">Время, начиная с которого разрешен проход</param>
        /// <param name="timeAllowedUntil">Время, до которого разрешен проход</param>
        public long InsertLabelIfNotExists(RfidLabelZ5R rfid, String timeAllowedFrom = "00:00:00", String timeAllowedUntil = "00:00:00")
        {
            if(IsLabelExist(rfid.IdString) == -1)
            {
                string rfid6 = rfid.Id6String;
                string rfid7 = rfid.IdString;
                String query = "INSERT INTO LABELS_TABLE " +
                               "(RFID6, RFID7, TIME_ALLOWED_FROM, TIME_ALLOWED_UNTIL) VALUES " +
                               "('" + rfid6 + "','" + rfid7 +"','" + timeAllowedFrom + "','" + timeAllowedUntil + "');";
                return Insert(query);
            }
            return -1;
        }

        /// <summary>
        /// Добавляет запись в таблицу соответствий меток и контроллеров
        /// </summary>
        /// <param name="labelId">ID записи в таблице меток</param>
        /// <param name="rfidReaderId">ID записи в таблице контроллеров</param>
        public long InsertLabelReader(long labelId, long rfidReaderId)
        {
            String query = "INSERT INTO LABELS_READERS_TABLE " +
                           "(LABEL_ID,RFID_READER_ID) VALUES " +
                           "(" + labelId + "," + rfidReaderId + ");";
            return Insert(query);
        }

        /// <summary>
        /// Добавляет запись в таблицу гостей отеля
        /// </summary>
        /// <param name="rfidLabelId">ID в таблице меток</param>
        /// <param name="hotelId">Номер в учетной книге гостиницы</param>
        /// <returns></returns>
        public long InsertHotelGuest(long rfidLabelId, string hotelId)
        {
            String query = "INSERT INTO HOTEL_GUESTS_TABLE " +
                           "(RFID_LABEL_ID,HOTEL_ID) VALUES " +
                           "(" + rfidLabelId + ",'" + hotelId + "');";
            return Insert(query);
        }

        #endregion

        #region UPDATE

        /// <summary>
        /// Обновляет запись контроллера в БД по ее ID
        /// </summary>
        /// <param name="controller"></param>
        public void UpdateControllerById(RFIDReaderController controller)
        {
            string query = "UPDATE RFID_READERS_TABLE SET SERIAL_NUMBER='" +
                            controller.SerialNumber + "', IP='" +
                            controller.Ip + "', PORT='" +                            
                            controller.Port +  
                            "' WHERE ID=" + controller.Id;

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandText = query;
                cmd.Connection = connection;

                cmd.ExecuteNonQuery();

                this.CloseConnection();
            }
        }

        /// <summary>
        /// Обновляет запись прохода в БД по ее ID
        /// </summary>
        /// <param name="gate"></param>
        public void UpdateGateById(Gate gate)
        {
            string query = "UPDATE GATES_TABLE SET NAME='" +
                            gate.Name + "', RFID_READER_ID=" +
                            gate.ControllerId +
                            " WHERE ID=" + gate.Id;

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand();
                cmd.CommandText = query;
                cmd.Connection = connection;

                cmd.ExecuteNonQuery();

                this.CloseConnection();
            }
        }

        #endregion

        #region DELETE

        /// <summary>
        /// Удалить запись по ID
        /// </summary>
        /// <param name="tableName">Название таблицы</param>
        /// <param name="Id">ID записи</param>
        public void DeleteById(String tableName, long Id)
        {
            string query = "DELETE FROM " + tableName + " WHERE id=" + Id;

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();
                this.CloseConnection();
            }
        }

        /// <summary>
        /// Удалить запись из таблицы соответствия меток и контроллеров
        /// </summary>
        /// <param name="labelId">ID записи в таблице меток</param>
        /// <param name="rfidReaderId">ID записи в таблице контроллеров</param>
        public void DeleteLabelReader(long labelId, long rfidReaderId)
        {
            String query = "DELETE FROM LABELS_READERS_TABLE WHERE " +
                           "LABEL_ID=" + labelId + " AND " +
                           "RFID_READER_ID=" + rfidReaderId;

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();
                this.CloseConnection();
            }
        }

        #endregion       
    }
}
