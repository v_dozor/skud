﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SKUDDatabase
{
    public class Settings : IDisposable
    {
        public static readonly string FileName = "settings.xml";

        // MySql
        public string IP = "";
        public int Port = 3306;
        public bool IsLocalhost = true;

        // 1C
        public string Url = "";
        public string Username = "";
        public string Password = "";

        //сохранение настроек
        public void Save()
        {
            using (Stream writer = new FileStream(FileName, FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                serializer.Serialize(writer, this);
            }
        }

        //загрузка настроек
        public static Settings Load()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Settings));

            if (File.Exists(FileName))
            {
                using (Stream stream = new FileStream(FileName, FileMode.Open))
                {
                    return (Settings)serializer.Deserialize(stream);
                }
            }
            return null;
        }

        public void Dispose()
        {
            
        }
    }
}
