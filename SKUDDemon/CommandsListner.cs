﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SKUDDemon
{
    /// <summary>
    /// Get commands from network and run it
    /// </summary>
    public class CommandsListner
    {
        private TcpListener listner = null;
        private Thread acceptThread;
        ManualResetEvent acceptSync = new ManualResetEvent(true);

        private object allowClientLock = new object();
        private bool _allowClient = true;
        private bool allowClient
        {
            get
            {
                lock(allowClientLock)
                { 
                    return _allowClient;
                }
            }
            set
            {
                lock (allowClientLock)
                {
                    _allowClient = value;
                }
            }
        }

        private object allowAcceptingLock = new object();
        private bool _allowAccepting = true;
        private bool allowAccepting
        {
            get
            {
                lock (allowAcceptingLock)
                {
                    return _allowAccepting;
                }
            }
            set
            {
                lock (allowAcceptingLock)
                {
                    _allowAccepting = value;
                }
            }
        }

        private TcpClient client = null;
        private NetworkStream clientStream = null;
        private Thread clientThread = null;

        byte[] buffer = new byte[4096];

        public event Action ForceRunCmd;

        public CommandsListner(IPAddress ip, int port)
        {
            listner = new TcpListener(ip, port);
        }

        public void ForceRunResult(bool done)
        {
            byte[] buf = new byte[4096];
            buf[0] = 0xA0;
            buf[1] = 1;
            buf[2] = done == true ? (byte)1 : (byte)0;
            try
            {
                clientStream.Write(buf, 0, buf.Length);
            }
            catch(Exception ex)
            {

            }
        }

        public bool Start()
        {
            if (acceptThread != null &&
                (acceptThread.ThreadState == ThreadState.Running ||
                acceptThread.ThreadState == ThreadState.WaitSleepJoin))
                return false;
            
            acceptThread = new Thread(AcceptThread);
            acceptThread.Name = "CommandsListner_AcceptNewClient_Thread";

            acceptThread.Start();

            return true;
        }

        public void Stop()
        {
            allowClient = false;
            allowAccepting = false;

            listner.Stop();
            acceptThread.Interrupt();
            acceptThread.Join();

           if(client != null)
            { 
                clientThread.Interrupt();
                if(clientStream != null)
                    clientStream.Close();
                if (client != null)
                    client.Close();
                clientThread.Join();
            }
        }
        
        private void AcceptThread()
        {
            TcpClient cl = null;
            while(allowAccepting)
            {
                acceptSync.WaitOne();
                acceptSync.Reset();
                if (!allowAccepting)
                    return;

                listner.Start();
                try
                {
                    cl = listner.AcceptTcpClient();
                }
                catch (Exception ex) { }
                listner.Stop();

                if (allowClient)
                {
                    client = cl;
                    clientStream = client.GetStream();

                    clientThread = new Thread(ClientThread);
                    clientThread.Name = "CommandsListner_ClientInteraction_Thread";

                    if (clientThread.IsAlive == true)
                    {
                        allowClient = false;
                        clientThread.Join();
                    }

                    allowClient = true;
                    clientThread = new Thread(ClientThread);
                    clientThread.Start();
                }
            }
        }

        private void ClientThread()
        {
            while(allowClient)
            {
                int readed = 0;
                try
                {
                    readed = clientStream.Read(buffer, 0, buffer.Length);
                }
                catch(Exception ex)
                {
                    client.Close();
                    clientStream.Close();
                    acceptSync.Set();
                    return;
                }
                HandleBuffer();
                if (client.Connected == false || readed == 0)
                {
                    client.Close();
                    clientStream.Close();
                    acceptSync.Set();
                    return;
                }
                    
            }
        }

        private void HandleBuffer()
        {
            //first package must be 0xA0
            if(buffer[0] == 0xA0)
            {
                //demon force run cmd
                switch(buffer[1])
                {
                    case 1:
                        if (ForceRunCmd != null)
                            ForceRunCmd.Invoke();
                        break;
                }
            }
            buffer[0] = 0xFF;
        }

    }
}
