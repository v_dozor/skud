﻿using ControllerConnectors;
using SKUDDatabase;
using SKUDDatabase.DataModels;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using ZGuard;
using System.Timers;
using System.Collections.Generic;
using Votum._1C;
using Votum.Types;
using System.Windows.Forms;

namespace SKUDDemon
{
    public enum DemonState { Stopped, Waiting, Busy }

    public class Demon
    {
        private System.Timers.Timer timer;
        private int controllerTryNumber = 2;
        private int databaseTryNumber = 2;
        private int timeOut = 0;

        SKUDDatabaseConnection dbCon;
        Settings settings;
        ObservableCollection<RFIDReaderController> readers = new ObservableCollection<RFIDReaderController>();
        Z5rwebConnector connector;

        private DemonState state = DemonState.Stopped;
        public DemonState State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
            }
        }

        string logPath = "log.txt";

        public Demon(Settings settings = null)
        {
            if(settings == null)
            {
                this.settings = Settings.Load();
                settings = this.settings;
            }
            else
            {
                this.settings = settings;
            }
            
            controllerTryNumber = settings.controllerTryNumber;
            databaseTryNumber = settings.databaseTryNumber;
            timeOut = settings.timeOutSec;
            logPath = settings.logPath;
            timer = new System.Timers.Timer();
            timer.Elapsed += HandleData;
            timer.Interval = settings.timerIntervalMin * 60 * 1000;
            timer.AutoReset = true;
        }

        public void Start(bool debug = false)
        {
            if(debug)
            {
                timer.Interval = 1;
                timer.AutoReset = false;
                controllerTryNumber = 1;
                databaseTryNumber = 1;
                timeOut = 1;
            }
            timer.Start();
            State = DemonState.Waiting;
        }

        public void Stop()
        {
            timer.Stop();
            State = DemonState.Stopped;
        }

        private ManualResetEvent handleDataSync = new ManualResetEvent(true);

        public bool Force()
        {
            if (State != DemonState.Waiting)
                return false;
            //run timer now
            timer.Stop();
            timer.AutoReset = false;
            timer.Interval = 10;
            timer.Start();
            Thread.Sleep(100);
            //return timer settings
            timer.Interval = settings.timerIntervalMin * 60 * 1000;
            if(settings.debug != "true")
                timer.AutoReset = true;
            timer.Start();
            handleDataSync.Reset();
            handleDataSync.WaitOne();
            return true;
        }

        /// <summary>
        /// This method will grab data from controllers and send it to database
        /// </summary>
        private void HandleData(Object source, ElapsedEventArgs e)
        {
            if (State == DemonState.Busy)
                return;

            handleDataSync.Reset();

            State = DemonState.Busy;
            bool dataSendedToDb = false;

            #region connecting to database
            log("start handling data at " + DateTime.Now.ToString());
            for (int i = 0; i < databaseTryNumber; ++i)
            {
                dbCon = new SKUDDatabaseConnection(settings.ip, settings.port, false);
                try
                {
                    log("trying to connect database and get controllers list ... ");
                    if (readers.Count == 0)
                    {
                        readers = dbCon.GetRfidReaderControllers();
                    }
                    else
                    {
                        log("list already exists");
                    }

                    i = controllerTryNumber;
                    log("done");
                }
                catch (Exception ex)
                {
                    log(String.Format("exception [{0}] while trying to connecto to database, try #{1}",
                                ex.Message,
                                i.ToString()));
                    log(String.Format("timeout for {0} sec", (timeOut / 1000).ToString()));
                    Thread.Sleep(timeOut / 1000);
                }
            }
            #endregion

            foreach (var item in readers)
            {
                #region grabbing data from controllers
                for (int i = 0; i < controllerTryNumber; ++i)
                {
                    log(String.Format("trying to connect to {0} controller, ip {1}, date {2}, try #{3}",
                            item.SerialNumber.ToString(),
                            item.Ip.ToString(),
                            DateTime.Now.ToString(),
                            i.ToString()));
                    try
                    {
                        connector = new Z5rwebConnector(
                            IPAddress.Parse(item.Ip),
                            Convert.ToUInt16(item.SerialNumber),
                            item.Port);
                        connector.Connect();
                        connector.RefreshEventBuffer();

                        i = controllerTryNumber;
                        log("done");

                    }
                    catch (Exception ex)
                    {
                        log(String.Format("exception [{0}] while trying to get data from controller, try #{1}",
                            ex.Message,
                            i.ToString()));
                        log(String.Format("timeout for {0} sec", (timeOut / 1000).ToString()));
                        Thread.Sleep(timeOut / 1000);
                    }
                }
                #endregion

                #region sending to database
                for (int i = 0; i < databaseTryNumber; ++i)
                {
                    try
                    {
                        log("trying to send data to database");
                        //sending data to database, now data have only passage events
                        IEnumerable<Log> passageEvents = connector.EventsBuffer.Values
                            .Where((x) => { return x.EventType == ZG_CTR_EV_TYPE.ZG_EV_PASSAGE; })
                            .Select<IControllerEvent, Log>((x) =>
                            {
                                PassEvent pe = (PassEvent)x;
                                string dir = (pe.Direct == ZG_CTR_DIRECT.ZG_DIRECT_IN ? "IN" : (pe.Direct == ZG_CTR_DIRECT.ZG_DIRECT_OUT ? "OUT" : "UNDEFINED"));
                                return new Log(
                                    0,
                                    new RfidLabelZ5R(pe.RfidKeyString,LabelTypeZ5R.SpecificZ5R).Id6String, 
                                    item.SerialNumber, 
                                    (int)pe.EventType, 
                                    pe.EventTypeString, 
                                    pe.Date, 
                                    pe.ToString(),
                                    dir);
                            });

                        if (passageEvents.Count() != 0)
                            dbCon.AddLogRange(passageEvents);

                        dataSendedToDb = true;

                        i = controllerTryNumber;
                        log("done");
                    }
                    catch (Exception ex)
                    {
                        log(String.Format("exception [{0}] while sending data to database, try #{1}",
                            ex.Message,
                            i.ToString()));
                        log(String.Format("timeout for {0} sec", (timeOut / 1000).ToString()));
                        Thread.Sleep(timeOut / 1000);
                    }
                }
                #endregion

                #region clear and close
                for (int i = 0; i < controllerTryNumber; ++i)
                {
                    try
                    {
                        log("trying to clear controller buffer");
                        if (dataSendedToDb)
                        {
                            connector.ClearControllerEventBuffer();
                            log("done");
                        }
                        else
                        {
                            log("data was not sended to database, buffer cannot be cleared");
                        }

                        log("trying to disconnect");
                        connector.Disconnect();
                        log("done");

                        i = controllerTryNumber;
                    }
                    catch (Exception ex)
                    {
                        log(String.Format("exception [{0}] while trying to clear controller events buffer, try #{1}",
                            ex.Message,
                            i.ToString()));
                        log(String.Format("timeout for {0} sec", (timeOut / 1000).ToString()));
                        Thread.Sleep(timeOut / 1000);
                    }
                }
                #endregion
            }

            State = DemonState.Waiting;

            handleDataSync.Set();
        }

        private void log(string msg)
        {
            if (settings.logging == false)
                return;
            File.AppendAllText(logPath, String.Format("[{0}] {1}" + Environment.NewLine, DateTime.Now, msg));
        }
    }
}
