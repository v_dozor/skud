﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SKUDDatabase;
using SKUDDatabase.DataModels;
using System.Collections.ObjectModel;
using SKUDDatabase.DataModels;
using ControllerConnectors;
using System.Net;
using ZGuard;
using System.IO;

namespace SKUDDemon
{
    public partial class Form1 : Form
    {
        Settings settings = null;
        ObservableCollection<RFIDReaderController> readers = new ObservableCollection<RFIDReaderController>();

        Demon demon = null;
        CommandsListner cmdListner = null;

        Timer timer = new Timer();
        public Form1()
        {
            try
            {
                settings = Settings.Load();
            }
            catch(Exception ex)
            {
                
            }

            if(settings == null)
            { 
                settings = new Settings();
                settings.Save();
            }

            InitializeComponent();
            demon = new Demon(settings);
            demon.Start(settings.debug == "true" ? true : false);
            cmdListner = new CommandsListner(
                IPAddress.Parse(settings.manualRunIP), 
                settings.manualRunPort);
            cmdListner.Start();
            cmdListner.ForceRunCmd += new Action(() => {
                demon.Force();
                cmdListner.ForceRunResult(true);
            });
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBox1.Text +=
                "MySql IP: " + settings.ip + Environment.NewLine +
                "MySql port: " + settings.port + Environment.NewLine +
                "Путь к логам: " + settings.logPath + Environment.NewLine +
                "Обновление раз в (часов): " + (settings.timerIntervalMin / 60) +  Environment.NewLine +
                "Режим отладки: " + (settings.debug == "true" ? "да" : "нет") + Environment.NewLine +
                "Время между попытками выполнить операцию (сек): " + settings.timeOutSec + Environment.NewLine +
                "Ведутся ли логи: " + (settings.logging == true ? "да" : "нет") + Environment.NewLine +
                "Для завершения работы демона закройте окно";
                
                BeginInvoke(new MethodInvoker(delegate
                {
                    Hide();
                }));
        }
        
        public void Start()
        {
            
        }

        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            notifyIcon1.Visible = false;
            this.Visible = true;
            this.WindowState = FormWindowState.Maximized;
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                notifyIcon1.Visible = true;
                this.Visible = false;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            demon.Stop();
            cmdListner.Stop();
        }
    }
}
