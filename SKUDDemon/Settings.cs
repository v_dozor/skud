﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace SKUDDemon
{
    public class Settings : IDisposable
    {
        public static readonly string FileName = "settings.xml";

        public string ip = "127.0.0.1";
        public int port = 3306;

        public int controllerTryNumber = 5;
        public int databaseTryNumber = 5;
        public int timeOutSec = 10; //10 sec
        public int timerIntervalMin = 60; //60 min
        public string debug = "false";
        public string logPath = "log.txt";
        public bool logging = true;

        public string manualRunIP = "127.0.0.1";
        public int manualRunPort = 3524;

        public void Save()
        {
            using (Stream writer = new FileStream(FileName, FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                serializer.Serialize(writer, this);
            }
        }

        public static Settings Load()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Settings));

            if (File.Exists(FileName))
            {
                using (Stream stream = new FileStream(FileName, FileMode.Open))
                {
                    return (Settings)serializer.Deserialize(stream);
                }
            }
            return null;
        }

        public void Dispose()
        {
            
        }
    }
}
