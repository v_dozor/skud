﻿using DevExpress.Mvvm;
using SKUDDatabase;
using SKUDDatabase.DataModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Votum._1C;

namespace SKUDReporting
{
    public class MainViewModel : ViewModelBase
    {
        private Specialty[] specialityList;
        //private Specialization[] specializationList;
        private Student[] studentList;
        private ReportViewModel reportVM;
        SKUDDatabaseConnection conn;
        public MainViewModel()
        {
            conn = new SKUDDatabaseConnection("127.0.0.1");
            Connector_1C connector1c = new Connector_1C();
            specialties = new ObservableCollection<Specialty>(connector1c.GetSpecialities());
            
            studentList = connector1c.GetStudents().ToArray();
            specialityList = connector1c.GetSpecialities().ToArray();
            //specializationList = connector1c.GetSpecializations();
            var ppp = specialityList.Join(
                studentList,
                o => o.RefKey,
                i => i.SpecialtyKey,
                (o,i) => new
                {
                    student = i.Description,
                    speciality = o.Description
                });
            
            int a = 0;
            ++a;
        }

        public void Show()
        {
            MainWindow mw = new MainWindow() { DataContext = this };
            mw.Show();
        }

        private DateTime dateStart = DateTime.Now;
        public DateTime DateStart
        {
            get
            {
                return dateStart;
            }
            set
            {
                dateStart = value;
                RaisePropertiesChanged("DateStart");
            }
        }

        private DateTime dateEnd = DateTime.Now;
        public DateTime DateEnd
        {
            get
            {
                return dateEnd;
            }
            set
            {
                dateEnd = value;
                RaisePropertiesChanged("DateEnd");
            }
        }

        private bool checkedStudents;
        public bool CheckedStudents
        {
            get
            {
                return checkedStudents;
            }
            set
            {
                checkedStudents = value;
                RaisePropertiesChanged("CheckedStudents");
            }
        }

        private bool checkedPersonal;
        public bool CheckedPersonal
        {
            get
            {
                return checkedPersonal;
            }
            set
            {
                checkedPersonal = value;
                RaisePropertiesChanged("CheckedPersonal");
            }
        }

        private bool checkedGuest;
        public bool CheckedGuest
        {
            get
            {
                return checkedGuest;
            }
            set
            {
                checkedGuest = value;
                RaisePropertiesChanged("CheckedGuest");
            }
        }

        ObservableCollection<Report> reports = new ObservableCollection<Report>();

        private DelegateCommand showReportCommand;
        public ICommand ShowReportCommand
        {
            get
            {
                if(showReportCommand == null)
                    showReportCommand = new DelegateCommand(ShowReport);
                return showReportCommand;
            }
        }
        private void ShowReport()
        {
            //string[] rfids = selectedStudent.Select((x) => { return x.Rfid; }).ToArray();
            string[] rfids = new string[] { "06-9F-B8-81-E0-BD-46" };
            var logs = conn.GetLogs(dateStart, dateEnd, rfids);

            reports = new ObservableCollection<Report>(
                logs.Select(
                    (x) => {
                        return new Report {
                            Name = x.Rfid6,
                            PassageDate =x.Date,
                            PassageDirection =x.Direction
                        };
                    })
                );

            if (reportVM == null)
                reportVM = new ReportViewModel(reports);
            reportVM.Reports = reports;
            reportVM.Show();
        }

        private DelegateCommand checkedStudentsClickedCommand;
        public ICommand CheckedStudentsClickedCommand
        {
            get
            {
                if(checkedStudentsClickedCommand == null)
                    checkedStudentsClickedCommand = new DelegateCommand(CheckedStudentsClicked);
                return checkedStudentsClickedCommand;
            }
        }
        public void CheckedStudentsClicked()
        {

        }

        public void ItemChangedEventRised()
        {
            var a = 0;
            ++a;
        }

        private ObservableCollection<Specialty> specialties;
        public ObservableCollection<Specialty> Specialties
        {
            get
            {
                return specialties;
            }
            set
            {
                specialties = value;
                RaisePropertyChanged("Specialties");
            }
        }

        private Specialty selectedSpecialty;
        public Specialty SelectedSpecialty
        {
            get
            {
                return selectedSpecialty;
            }
            set
            {
                selectedSpecialty = value;
                RaisePropertyChanged("SelectedSpecialty");
            }
        }

        private DelegateCommand specialtiesClickedCommand;
        public ICommand SpecialtiesClickedCommand
        {
            get
            {
                if (specialtiesClickedCommand == null)
                    specialtiesClickedCommand = new DelegateCommand(FilterStudents);
                return specialtiesClickedCommand;
            }
        }

        private void FilterStudents()
        {
            var keys = selectedSpecialties.Select((x) => { return x.RefKey; });
            students = new ObservableCollection<Student>(
                studentList.Where( (x) => { return keys.Contains(x.SpecialtyKey); }
                ));
            RaisePropertiesChanged("Students");
        }

        private ObservableCollection<Specialty> selectedSpecialties = new ObservableCollection<Specialty>();
        public ObservableCollection<Specialty> SelectedSpecialties
        {
            get
            {
                return selectedSpecialties;
            }
            set
            {
                selectedSpecialties = value;
                RaisePropertyChanged("SelectedSpecialties");
            }
        }

        private ObservableCollection<Student> students;
        public ObservableCollection<Student> Students
        {
            get
            {
                return students;
            }
            set
            {
                students = value;
                RaisePropertiesChanged("Students");
            }
        }

        private Student selectedStudent;
        public Student SelectedStudent
        {
            get
            {
                return selectedStudent;
            }
            set
            {
                selectedStudent = value;
                RaisePropertiesChanged("SelectedStudent");
            }
        }

        private ObservableCollection<Student> selectedStudents = new ObservableCollection<Student>();
        public ObservableCollection<Student> SelectedStudents
        {
            get
            {
                return selectedStudents;
            }
            set
            {
                selectedStudents = value;
                RaisePropertyChanged("SelectedStudents");
            }
        }

    }

}