﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKUDReporting
{
    public class Report
    {
        public DateTime PassageDate { get; set; }
        public string Name { get; set; }
        public string PassageDirection { get; set; }
    }
}
