﻿using DevExpress.Mvvm;
using SKUDDatabase;
using SKUDDatabase.DataModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Votum._1C;

namespace SKUDReporting
{
    public class ReportViewModel : ViewModelBase
    {
        private ObservableCollection<Report> reports;
        public ObservableCollection<Report> Reports
        {
            get
            {
                return reports;
            }
            set
            {
                reports = value;
                RaisePropertiesChanged("Reports");
            }
        }
        public ReportViewModel(ObservableCollection<Report> reports)
        {
            //this.reports = new ObservableCollection<Report>( reports.Where((x) => { return true; }));
            this.reports = reports;
        }

        public void Show()
        {
            ReportWindow rw = new ReportWindow() { DataContext = this };
            rw.ShowDialog();
        }
    }
}