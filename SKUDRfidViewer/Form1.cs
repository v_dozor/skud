﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using DozorUsbLib;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System;
using System.Runtime.InteropServices;

namespace SKUDRfidViewer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        RfidReader reader;
        private void Form1_Load(object sender, EventArgs e)
        {
            reader = new RfidReader();
            
            reader.Rfid_Updated += Reader_Rfid_Updated;
        }

        private void Reader_Rfid_Updated(object sender, RfidReaderEventArgs e)
        {
            this.Invoke( new Action( () => { textBox1.Text = e.Rfid; } ));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textBox1.Text);
        }
    }
}
