﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Cryptography;
using System.Net.Sockets;
using System.Net;

namespace SKUDWebReporting.Controllers
{
    public class HomeController : Controller
    {
        private bool checkLogin()
        {
            return Request.Cookies["vas"] != null && Request.Cookies["vas"].Value == "ade39e8ef60d8691a6114ae48aeb5660fc2140ac35858ae";
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login(string hashlgn, string hashpwd)
        {
            bool logined = false;
            SHA1 sha = new SHA1CryptoServiceProvider();
            string cookieVal = "ade39e8ef60d8691a6114ae48aeb5660fc2140ac35858ae";//sha.ComputeHash(new Byte[] { (byte)DateTime.Now.Second, (byte)DateTime.Now.Ticks }).ToString();
            
            if (hashlgn == "d033e22ae348aeb5660fc2140aec35850c4da997" && hashpwd == "9ea29e8691611fd511a46def60d81182190fbdba")
            {
                HttpCookie cookie = new HttpCookie("vas");
                cookie.Expires = DateTime.Now + new TimeSpan(24 * 5, 0, 0);
                cookie.Value = cookieVal;

                Response.Cookies.Add(cookie);
                logined = true;
            }
            if(logined)
                return View("~/Views/Home/Index.cshtml");
            else
                return View("~/Views/Home/Login.cshtml");
        }



    }
}