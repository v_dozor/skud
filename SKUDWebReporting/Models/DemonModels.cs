﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.Configuration;

namespace SKUDWebReporting.Models
{
    /// <summary>
    /// This class used to send commands to demon
    /// Use demonIp and demonPort in Web.config to configurate connection
    /// </summary>
    public class DemonCommander
    {
        public List<string> errorList { private set; get; }
        public IPAddress ip { private set; get; }
        public int port { private set; get; }

        private byte[] buf = new byte[4096];
        private TcpClient client = null;
        private NetworkStream nws = null;

        public DemonCommander(IPAddress ip = null, int? port = null)
        {
            try
            {
                this.ip = ip ?? IPAddress.Parse(WebConfigurationManager.AppSettings["demonIp"]);
                this.port = port ?? Convert.ToInt32(WebConfigurationManager.AppSettings["demonPort"]);
            }
            catch (Exception ex)
            {
                errorList.Add("Невозможно преобразовать данные (ip port) из настроек: " + ex.Message);
            }
            errorList = new List<string>();
        }

        /// <summary>
        /// Run demon by ip and port 
        /// </summary>
        /// <returns>Result</returns>
        public byte RunDemonCmd()
        {
            client = new TcpClient();
            try
            {
                client.Connect(ip, port);
                nws = client.GetStream();
                buf[0] = 0xA0;
                buf[1] = 1;

                nws.Write(buf, 0, buf.Length);
                nws.Read(buf, 0, buf.Length);

                client.Close();
            }
            catch (Exception ex)
            {
                errorList.Add("Во время работы произошла ошибка: " + ex.Message);
            }

            return buf[2];
        }
    }
}