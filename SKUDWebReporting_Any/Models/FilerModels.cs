﻿using SKUDDatabase.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Votum._1C;
using System.Runtime;
using System.Runtime.Caching;
using SKUDDatabase;
using Votum.Types;
using System.Web.Configuration;
using System.Net;

namespace SKUDWebReporting.Models
{
    public class StudentDescription
    {
        private Student studnet;
        public Student Student
        {
            get
            {
                return studnet;
            }
            set
            {
                studnet = value;
            }
        }

        private Specialty speciality;
        public Specialty Speciality
        {
            get
            {
                return speciality;
            }
            set
            {
                speciality = value;
            }
        }

        private Group group;
        public Group Group
        {
            get
            {
                return group;
            }
            set
            {
                group = value;
            }
        }

        private Log log;
        public Log Log
        {
            get
            {
                return log;
            }
            set
            {
                log = value;
            }
        }
    }

    public class ReportRow
    {
        public string fio { get; set; }
        public string gate { get; set; }

        public string direction { get; set; }
        public DateTime passageDate { get; set; }
    }

    public class FilerFormModel
    {
        private ObjectCache cache;
        private Connector_1C conn1c;
        private SKUDDatabaseConnection connDb;

        public FilerFormModel()
        {
            cache = MemoryCache.Default;
            
            try
            {
                conn1c = new Connector_1C(
                    WebConfigurationManager.AppSettings["baseURL1C"],
                    WebConfigurationManager.AppSettings["user1C"],
                    WebConfigurationManager.AppSettings["password1C"]
                    );
            }
            catch(Exception ex)
            {
                Error.Add("Ошибка при подключении к 1С" + ex.Message);
            }
            try
            {
                connDb = new SKUDDatabaseConnection(
                    WebConfigurationManager.AppSettings["mysqlIP"],
                    Convert.ToInt32(WebConfigurationManager.AppSettings["mysqlPort"])
                    );
            }
            catch (Exception ex)
            {
                Error.Add("Ошибка при подключении к БД" + ex.Message);
            }
        }

        private Student[] students;
        public Student[] Students
        {
            get
            {
                students = cache["Students"] as Student[];
                if (students == null)
                {
                    try
                    {
                        students = conn1c.GetStudents().ToArray();
                        cache["Students"] = students;
                    }
                    catch (Exception ex)
                    {
                        Error.Add("Ошибка при подключении к 1С" + ex.Message);
                    }
                }
                    
                return students;
            }
        }

        private Specialty[] specialities;
        public Specialty[] Specialities
        {
            get
            {
                students = cache["Specialities"] as Student[];
                if (specialities == null)
                {
                    try
                    {
                        specialities = conn1c.GetSpecialities().ToArray();
                        cache["Specialities"] = specialities;
                    }
                    catch (Exception ex)
                    {
                        Error.Add("Ошибка при подключении к 1С" + ex.Message);
                    }
                }
                return specialities;
            }
        }

        private Group[] groups;
        public Group[] Groups
        {
            get
            {
                groups = cache["Groups"] as Group[];
                if (groups == null)
                {
                    try
                    {
                        groups = conn1c.GetGroups().ToArray();
                        cache["Groups"] = groups;
                    }
                    catch (Exception ex)
                    {
                        Error.Add("Ошибка при подключении к 1С" + ex.Message);
                    }
                }
                return groups;
            }
            set
            {
                groups = value;
            }
        }

        private HotelGuest[] guests;
        public HotelGuest[] Guests
        {
            get
            {
                if (guests == null)
                {
                    var tmp = connDb.GetHotelGuests();
                    if (tmp == null)
                        return null;
                }
                guests = connDb.GetHotelGuests().ToArray();
                return guests;
            }
            set
            {
                guests = value;
            }
        }

        public Log[] GetLogs(DateTime dateStart, DateTime dateEnd, string[] rfids6)
        {
            var logs = connDb.GetLogs(dateStart, dateEnd, rfids6);
            return logs == null ? null : logs.ToArray();
        }
        
        private ContingentMovement[] contingentMovements;
        public ContingentMovement[] ContingentMovements
        {
            get
            {
                contingentMovements = cache["ContingentMovements"] as ContingentMovement[];
                if(contingentMovements == null)
                {
                    try
                    {
                        contingentMovements = conn1c.GetContingentMovements().ToArray();
                        cache["ContingentMovements"] = contingentMovements;
                    }
                    catch (Exception ex)
                    {
                        Error.Add(ex.Message);
                    }
                }
                return contingentMovements;
            }
            set
            {
                contingentMovements = value;
            }
        }

        private Gate[] gates;
        public Gate[] Gates
        {
            get
            {
                if(gates == null)
                {
                    var tmp_gates = connDb.GetGates();

                    if (tmp_gates == null)
                        return null;

                    gates = tmp_gates.ToArray();
                }

                return gates;
            }
            set
            {
                gates = value;
            }
        }

        public ReportRow[] FilterStudents(DateTime dateStart, DateTime dateEnd, string[] groups, string[] studnets)
        {
            string[] rfids6 = null;
            Log[] logs = null;
            ReportRow[] report = null;

            if (studnets != null) //case when user choose some students
            {
                rfids6 = Students
                    .Where((x) => { return studnets.Contains(x.RefKey); })
                    .Select((y) => { return new RfidLabelZ5R(y.Rfid).Id6String; })
                    .ToArray();
            }
            else if(groups != null) //case when user choose only groups without students 
            {
                rfids6 = GetStudentsByGroups(groups)
                    .Where((x) => { return x.Rfid != ""; })
                    .Select((y) => { return new RfidLabelZ5R(y.Rfid).Id6String; })
                    .ToArray();
            }

            if (rfids6 != null && rfids6.Length != 0)
                logs = GetLogs(dateStart, dateEnd, rfids6);
            else
                return null;

            if (logs == null || logs.Length == 0)
                return null;

            Gate[] gates = Gates ?? new Gate[] { new Gate(0, "", 0) };
            
            report = Students
                .Join(
                    logs,
                    (s) => { return s.Rfid.Length > 4 ? s.Rfid.Remove(0,3) : ""; },
                    (l) => { return l.Rfid6; },
                    (s, l) =>
                    {
                        Gate tmpGate = gates.FirstOrDefault((x) => { return x.ControllerId == l.RfidReaderId; });
                        return new ReportRow
                        {
                            direction = l.Direction,
                            fio = s.Description,
                            passageDate = l.Date,
                            gate = tmpGate != null ? tmpGate.Name : ""
                        };
                    })
                .OrderBy((x) => { return x.fio; })
                .OrderBy((x) => { return x.passageDate; })
                .ToArray();
            return report;
        }

        public Student[] GetStudentsByGroups(string[] RefKeys)
        {
            if (RefKeys == null || RefKeys.Length == 0)
                return null;
            List<Student> students = new List<Student>();
            foreach(var refKey in RefKeys)
            {
                List<Student> tmpStudents = null;
                try
                {
                    tmpStudents = conn1c.GetStudentsByGroup(refKey, ContingentMovements, Students);
                }
                catch (Exception ex)
                {
                    Error.Add("Ошибка при подключении к 1С" + ex.Message);
                }
                
                students.AddRange(tmpStudents);
            }
            return students.ToArray();
        }

        public ReportRow[] FilterGuests(DateTime dateStart, DateTime dateEnd, string[] guests)
        {
            ReportRow[] report = connDb.GetGuestsByIds(dateStart, dateEnd, guests)
                .Select(
                    (x) =>
                    {
                        return new ReportRow
                        {
                            direction = (string)x["DIRECTION"],
                            fio = x["HOTEL_ID"].ToString(),
                            passageDate = (DateTime)x["DATE"]
                        };
                    })
                .ToArray();

            return report;
        }

        private List<string> error = new List<string>();
        public List<string> Error
        {
            get
            {
                return error;
            }
            set
            {
                error = value;
            }
        }
    }
}