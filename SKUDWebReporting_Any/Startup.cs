﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SKUDWebReporting.Startup))]
namespace SKUDWebReporting
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
