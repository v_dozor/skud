﻿using SKUDWebReporting.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Caching;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace SKUDWebReporting.Controllers
{
    public class DemonController : Controller
    {
        private ObjectCache cache;
        private const string demonLastUpdateCache = "demonLastUpdateCache";
        //timeout between demon force run
        private readonly TimeSpan demonUpdateTimeout = new TimeSpan(0, 2, 0);

        private bool checkLogin()
        {
            return Request.Cookies["vas"] != null && Request.Cookies["vas"].Value == "ade39e8ef60d8691a6114ae48aeb5660fc2140ac35858ae";
        }

        public DemonController()
        {
            cache = MemoryCache.Default;
        }

        // GET: Demon
        public ActionResult Index()
        {
            if (!checkLogin())
                return View("~/Views/Home/Login.cshtml");

            return View();
        }

        // POST: Run demon
        [HttpPost]
        public ActionResult Run()
        {
            if (!checkLogin())
                return View("~/Views/Home/Login.cshtml");

            string errStr = string.Empty;

            if (cache[demonLastUpdateCache] != null &&
                (DateTime.Now - (DateTime)cache[demonLastUpdateCache]) < demonUpdateTimeout)
            {
                errStr = "Таймаут " + demonUpdateTimeout.ToString();
            }
            else
            {
                DemonCommander demonCmdr = new DemonCommander();
                byte result = demonCmdr.RunDemonCmd();

                if (demonCmdr.errorList.Count > 0)
                {
                    errStr = demonCmdr.errorList[0];
                }
                else
                {
                    cache[demonLastUpdateCache] = DateTime.Now;
                }
            }

            Response.Clear();
            Response.ContentType = "application/json; charset=utf-8";
            if (errStr == string.Empty)
                Response.Write("{\"result\": \"ok\"}");
            else
                Response.Write("{\"result\": \"" + errStr + "\"}");
            Response.End();
            return null;
        }
    }
}