﻿using Newtonsoft.Json;
using SKUDWebReporting.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SKUDWebReporting.Controllers
{
    public class FormFilterController : Controller
    {
        FilerFormModel model;
        public FormFilterController()
        {
            try
            { 
                model = new FilerFormModel();
            }
            catch(Exception ex)
            {
                model.Error.Add(ex.Message);
            }
        }

         
        private bool checkLogin()
        {
            return Request.Cookies["vas"] != null && Request.Cookies["vas"].Value == "ade39e8ef60d8691a6114ae48aeb5660fc2140ac35858ae";
        }
        // GET: FormFilter
        public ActionResult Index()
        {
            if (!checkLogin())
                return View("~/Views/Home/Login.cshtml");
            return View(model);
        }

        public ActionResult Filter(
            DateTime? dateStart, DateTime? dateEnd, string choice, 
            string[] speciality, string[] groups, string[] studnets,
            string[] guests)
        {
            if (!checkLogin())
                return View("~/Views/Home/Login.cshtml");

            ReportRow[] report = null;
            if (choice != null && choice == "students")
            {
                if (groups != null && groups.Length > 0)
                {
                    report = model.FilterStudents(
                        dateStart == null ? DateTime.MinValue : (DateTime)dateStart,
                        dateEnd == null ? DateTime.MaxValue : (DateTime)dateEnd,
                        groups,
                        studnets);
                }
            }
            if(choice != null && choice == "guests")
            {
                if (guests != null && guests.Length > 0)
                {
                    report = model.FilterGuests(
                        dateStart == null ? DateTime.MinValue : (DateTime)dateStart,
                        dateEnd == null ? DateTime.MaxValue : (DateTime)dateEnd,
                        guests);
                }
            }
            return View(report);
        }

        [HttpGet]
        public ActionResult StudentsBySpeciality(string speciality_ids)
        {
            if (!checkLogin())
                return View("~/Views/Home/Login.cshtml");

            string[] ids = speciality_ids.Split(',');

            var filteredStudetns = model.Students
                .Where((x) => { return ids.Contains(x.SpecialtyKey); })
                .Select((x) => { return new { fio = x.Description, id = x.RefKey }; });
            string str = JsonConvert.SerializeObject(filteredStudetns);
            Response.Clear();
            Response.ContentType = "application/json; charset=utf-8";
            Response.Write(str);
            Response.End();
            return View((object)str);
        }

        [HttpGet]
        public ActionResult StudentsBySpecialization(string specialization_ids)
        {
            if (!checkLogin())
                return View("~/Views/Home/Login.cshtml");

            string[] ids = specialization_ids.Split(',');
            FilerFormModel model = new FilerFormModel();
            
            var filteredStudetns = model.Students
                .Where((x) => { return ids.Contains(x.SpecializationKey); })
                .Select((x) => { return new { fio = x.Description, id = x.RefKey }; });
            string str = JsonConvert.SerializeObject(filteredStudetns);
            return null;
        }

        public ActionResult GroupsBySpecialities(string specialitiesIds)
        {
            if (!checkLogin())
                return View("~/Views/Home/Login.cshtml");

            string[] ids = specialitiesIds.Split(',');
            string str =
                JsonConvert.SerializeObject(
                    model.Groups
                        .Where(
                            (x) =>
                            {
                                return ids.Contains(x.SpecialtyKey);
                            })
                        .Select(
                            (y) =>
                            {
                                return new { id = y.RefKey, name = y.Description };
                            })
                    );

            Response.Clear();
            Response.ContentType = "application/json; charset=utf-8";
            Response.Write(str);
            Response.End();
            return null;
        }

        public ActionResult StudentsByGroups(string groupsIds)
        {
            if (!checkLogin())
                return View("~/Views/Home/Login.cshtml");

            string[] ids = groupsIds.Split(',');
            string str = JsonConvert.SerializeObject(
                model.GetStudentsByGroups(ids).Select(
                    (x) => {
                        return new
                        {
                            id = x.RefKey,
                            name = x.Description
                        };
                    }));
            Response.Clear();
            Response.ContentType = "application/json; charset=utf-8";
            Response.Write(str);
            Response.End();
            return null;
        }

    }
}