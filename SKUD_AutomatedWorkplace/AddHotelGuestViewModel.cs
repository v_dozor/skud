﻿using DevExpress.Mvvm;
using DozorUsbLib;
using SKUDDatabase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Votum.Types;

namespace SKUD_AutomatedWorkplace
{
    public class AddHotelGuestViewModel : ViewModelBase
    {
        private SKUDDatabaseConnection skudDatabaseConnection;
        private RfidReader rfidReader;

        private String rfid;
        public String Rfid
        {
            get { return rfid; }
            set
            {
                if(rfid != value)
                {
                    rfid = value;
                    RaisePropertyChanged("Rfid");
                }
            }
        }

        private String guestHotelId;
        public String GuestHotelId
        {
            get { return guestHotelId; }
            set
            {
                if (guestHotelId != value)
                {
                    guestHotelId = value;
                    RaisePropertyChanged("GuestHotelId");
                }
            }
        }

        public AddHotelGuestViewModel(SKUDDatabaseConnection _skudDatabaseConnection)
        {
            skudDatabaseConnection = _skudDatabaseConnection;

            rfidReader = new RfidReader();
            rfidReader.Rfid_Updated += new EventHandler<RfidReaderEventArgs>(RfidReceived);
        }

        public void Show()
        {
            AddHotelGuestWindow addHotelGuestWindow = new AddHotelGuestWindow() { DataContext = this };
            addHotelGuestWindow.ShowDialog();
        }

        private void RfidReceived(object sender, RfidReaderEventArgs args)
        {       
            Rfid = args.Rfid;
        }

        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            rfidReader.CloseDevices();
        }  

        #region Commands

        private DelegateCommand addGuestCommand;
        /// <summary>
        /// Команда добавления гостя
        /// </summary>
        public ICommand AddGuestCommand
        {
            get
            {
                if (addGuestCommand == null)
                {
                    if (addGuestCommand == null)
                        addGuestCommand = new DelegateCommand(AddGuest);
                }
                return addGuestCommand;
            }
        }

        /// <summary>
        /// Добавление гостя
        /// </summary>
        private void AddGuest()
        {
            try
            {
                long labelId = skudDatabaseConnection.IsLabelExist(Rfid);
                if (labelId == -1)
                {
                    labelId = skudDatabaseConnection.InsertLabelIfNotExists(new RfidLabelZ5R(Rfid));
                }
                if (skudDatabaseConnection.InsertHotelGuest(labelId, GuestHotelId) != -1)
                {
                    MessageBox.Show("Запись добавлена в базу данных", "Добавление гостя", MessageBoxButton.OK, MessageBoxImage.Information);
                    Rfid = "";
                    GuestHotelId = "";
                }
                else
                {
                    MessageBox.Show("Произошла ошибка при добавлении записи в базу данных", "Добавление гостя", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Произошла ошибка при добавлении записи в базу данных: " + ex.Message, "Добавление гостя", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion
    }
}
