﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SKUD_AutomatedWorkplace
{
    /// <summary>
    /// Interaction logic for AddHotelGuestWindow.xaml
    /// </summary>
    public partial class AddHotelGuestWindow : Window
    {
        private AddHotelGuestViewModel viewModel;

        public AddHotelGuestWindow()
        {
            InitializeComponent();
            Loaded += OnAddHotelGuestWindowLoaded;   
        }

        private void OnAddHotelGuestWindowLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            viewModel = DataContext as AddHotelGuestViewModel;
            Closing += viewModel.OnWindowClosing;
        }
    }
}
