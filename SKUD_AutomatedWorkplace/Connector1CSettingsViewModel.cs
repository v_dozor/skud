﻿using DevExpress.Mvvm;
using SKUDDatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace SKUD_AutomatedWorkplace
{
    public class Connector1CSettingsViewModel : ViewModelBase
    {
        private String url;
        public String Url
        {
            get { return url; }
            set
            {
                if(url != value)
                {
                    url = value;
                    RaisePropertyChanged("Url");
                }
            }
        }

        private String username;
        public String Username
        {
            get { return username; }
            set
            {
                if (username != value)
                {
                    username = value;
                    RaisePropertyChanged("Username");
                }
            }
        }

        private String password;
        public String Password
        {
            get { return password; }
            set
            {
                if (password != value)
                {
                    password = value;
                    RaisePropertyChanged("Password");
                }
            }
        }

        public Connector1CSettingsViewModel()
        {
            Settings settings = Settings.Load() ?? new Settings();

            Url = settings.Url;
            Username = settings.Username;
            Password = settings.Password;
        }

        public void Show()
        {
            Connector1CSettingsWindow connector1CSettingsWindow = new Connector1CSettingsWindow() { DataContext = this };
            connector1CSettingsWindow.ShowDialog();
        }

        private DelegateCommand saveCommand;
        /// <summary>
        /// Команда сохранения настроек сервера
        /// </summary>
        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new DelegateCommand(Save);
                }
                return saveCommand;
            }
        }

        /// <summary>
        /// Сохраняет настройки в файле конфигурации
        /// </summary>
        private void Save()
        {
            Settings settings = new Settings();
            settings.Url = Url;
            settings.Username = Username;
            settings.Password = Password;
            settings.Save();
            MessageBox.Show("Настройки сервера успешно сохранены", "Сохранение настроек", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
