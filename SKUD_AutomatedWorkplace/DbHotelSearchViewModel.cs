﻿using DevExpress.Mvvm;
using SKUDDatabase;
using SKUDDatabase.DataModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SKUD_AutomatedWorkplace
{
    public class DbHotelSearchViewModel : ViewModelBase
    {
        private SKUDDatabaseConnection skudDatabaseConnection;

        private ObservableCollection<HotelGuest> hotelGuests;
        public ObservableCollection<HotelGuest> HotelGuests
        {
            get { return hotelGuests; }
            set
            {
                if(hotelGuests != value)
                {
                    hotelGuests = value;
                    RaisePropertyChanged("HotelGuests");
                }
            }
        }

        private HotelGuest selectedHotelGuest;
        public HotelGuest SelectedHotelGuest
        {
            get { return selectedHotelGuest; }
            set
            {
                if(selectedHotelGuest != value)
                {
                    selectedHotelGuest = value;
                    RaisePropertyChanged("SelectedHotelGuest");
                }
            }
        }

        public DbHotelSearchViewModel(SKUDDatabaseConnection _skudDatabaseConnection)
        {
            skudDatabaseConnection = _skudDatabaseConnection;

            HotelGuests = skudDatabaseConnection.GetHotelGuests();
        }

        public void Show()
        {
            DbHotelSearchWindow dbHotelSearchWindow = new DbHotelSearchWindow() { DataContext = this };
            dbHotelSearchWindow.ShowDialog();
        }

        #region Commands

        private DelegateCommand whiteListManagementCommand;
        /// <summary>
        /// Команда открытия окна управления Белым списком
        /// </summary>
        public ICommand WhiteListManagementCommand
        {
            get
            {
                if (whiteListManagementCommand == null)
                {
                    if (whiteListManagementCommand == null)
                        whiteListManagementCommand = new DelegateCommand(WhiteListManagement);
                }
                return whiteListManagementCommand;
            }
        }

        /// <summary>
        /// Открывает окно управления Белым списком
        /// </summary>
        private void WhiteListManagement()
        {
            if (SelectedHotelGuest == null)
            {
                MessageBox.Show("Выберите гостя для совершения этого действия", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            WhiteListManagementViewModel wlmvm = new WhiteListManagementViewModel(skudDatabaseConnection, SelectedHotelGuest);
            wlmvm.Show();
        }

        private DelegateCommand deleteGuestCommand;
        /// <summary>
        /// Команда удаления гостя
        /// </summary>
        public ICommand DeleteGuestCommand
        {
            get
            {
                if (deleteGuestCommand == null)
                {
                    if (deleteGuestCommand == null)
                        deleteGuestCommand = new DelegateCommand(DeleteGuest);
                }
                return deleteGuestCommand;
            }
        }

        /// <summary>
        /// Удалить гостя
        /// </summary>
        private void DeleteGuest()
        {
            try
            {
                skudDatabaseConnection.DeleteById("HOTEL_GUESTS_TABLE", SelectedHotelGuest.Id);
                HotelGuests.Remove(SelectedHotelGuest);
                MessageBox.Show("Запись успешно удалена", "Удаление гостя", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Ошибка удаления гостя: " + ex.Message, "Ошибка удаления", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion
    }
}
