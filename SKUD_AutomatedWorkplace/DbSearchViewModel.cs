﻿using DevExpress.Mvvm;
using SKUDDatabase;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Votum._1C;
using Votum.Types;

namespace SKUD_AutomatedWorkplace
{
    public class DbSearchViewModel : ViewModelBase
    {
        private Connector_1C connector;
        private SKUDDatabaseConnection skudDatabaseConnection;

        // Список всех специальностей
        private ObservableCollection<Specialty> specialities;
        public ObservableCollection<Specialty> Specialities
        {
            get { return specialities; }
            set
            {
                if(specialities != value)
                {
                    specialities = value;
                    RaisePropertyChanged("Specialities");
                }
            }
        }

        // Выбранная специальность
        private Specialty selectedSpecialty;
        public Specialty SelectedSpecialty
        {
            get { return selectedSpecialty; }
            set
            {
                if(selectedSpecialty != value)
                {
                    selectedSpecialty = value;
                    RaisePropertyChanged("SelectedSpecialty");
                    try
                    {
                        var groupsBySpecialty = connector.GetGroupsBySpecialty(selectedSpecialty.RefKey);
                        if (groupsBySpecialty != null)
                        {                            
                            Groups = new ObservableCollection<Group>(groupsBySpecialty);
                            if(Groups != null)
                            {
                                if (Groups.Count > 0)
                                {
                                    SelectedGroup = Groups[0];
                                }
                            }                            
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Ошибка при запросе к серверу 1С: " + ex.Message,
                                        "Ошибка 1С", MessageBoxButton.OK, MessageBoxImage.Error);
                    }                    
                }
            }
        }        

        // Список специализаций в выбранной специальности
        private ObservableCollection<Group> groups;
        public ObservableCollection<Group> Groups
        {
            get { return groups; }
            set
            {
                if (groups != value)
                {
                    groups = value;
                    RaisePropertyChanged("Groups");
                }
            }
        }

        // Выбранная специализация
        private Group selectedGroup;
        public Group SelectedGroup
        {
            get { return selectedGroup; }
            set
            {
                if (selectedGroup != value)
                {
                    selectedGroup = value;
                    RaisePropertyChanged("SelectedGroup");
                    if(selectedGroup != null)
                    {
                        try
                        {
                            var studentsByGroup = connector.GetStudentsByGroup(selectedGroup.RefKey);
                            if (studentsByGroup != null)
                            {
                                Students = new ObservableCollection<Student>(studentsByGroup);
                                if (Students.Count > 0)
                                {
                                    SelectedStudent = Students[0];
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Ошибка при запросе к серверу 1С: " + ex.Message,
                                            "Ошибка 1С", MessageBoxButton.OK, MessageBoxImage.Error);
                        }                 
                    }                         
                }
            }
        }

        // Список студентов выбранной специализации
        private ObservableCollection<Student> students;
        public ObservableCollection<Student> Students
        {
            get { return students; }
            set
            {
                if (students != value)
                {
                    students = value;
                    RaisePropertyChanged("Students");
                }
            }
        }

        // Выбранный студент
        private Student selectedStudent;
        public Student SelectedStudent
        {
            get { return selectedStudent; }
            set
            {
                if (selectedStudent != value)
                {
                    selectedStudent = value;
                    RaisePropertyChanged("SelectedStudent");
                }
            }
        }

        public DbSearchViewModel(SKUDDatabaseConnection _skudDatabaseConnection)
        {
            skudDatabaseConnection = _skudDatabaseConnection;            
            try
            {
                Settings settings = Settings.Load();
                connector = null;
                if (settings != null)
                {
                    String url = settings.Url;
                    String username = settings.Username;
                    String password = settings.Password;
                    connector = new Connector_1C(url, username, password);
                }
                else
                {
                    connector = new Connector_1C();
                }
                //connector.GetClassrooms();
                //connector.GetTimetable();
                Specialities = new ObservableCollection<Specialty>(connector.GetSpecialities());
                if (Specialities.Count > 0)
                {
                    SelectedSpecialty = Specialities[0];
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при запросе к серверу 1С: " + ex.Message,
                                "Ошибка 1С", MessageBoxButton.OK, MessageBoxImage.Error);
            }             
        }

        public void Show()
        {
            DbSearchWindow studentSearchWindow = new DbSearchWindow() { DataContext = this };
            studentSearchWindow.ShowDialog();
        }

        #region Commands

        private DelegateCommand whiteListManagementCommand;
        /// <summary>
        /// Команда открытия окна управления Белым списком
        /// </summary>
        public ICommand WhiteListManagementCommand
        {
            get
            {
                if (whiteListManagementCommand == null)
                {
                    if (whiteListManagementCommand == null)
                        whiteListManagementCommand = new DelegateCommand(WhiteListManagement);
                }
                return whiteListManagementCommand;
            }
        }

        /// <summary>
        /// Открывает окно управления Белым списком
        /// </summary>
        private void WhiteListManagement()
        {
            if(SelectedStudent == null)
            {
                MessageBox.Show("Выберите студента для совершения этого действия", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if(SelectedStudent.Rfid == "")
            {
                MessageBox.Show("Данному студенту не назначена RFID-метка", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            WhiteListManagementViewModel wlmvm = new WhiteListManagementViewModel(skudDatabaseConnection, SelectedStudent);
            wlmvm.Show();
        }

        #endregion
    }
}
