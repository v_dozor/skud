﻿using ControllerConnectors;
using DevExpress.Mvvm;
using SKUDDatabase;
using SKUDDatabase.DataModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SKUD_AutomatedWorkplace
{
    public class GateControlViewModel : ViewModelBase
    {
        private SKUDDatabaseConnection skudDatabaseConnection;

        // Список всех проходных
        private ObservableCollection<Gate> gates;
        public ObservableCollection<Gate> Gates
        {
            get { return gates; }
            set
            {
                if (gates != value)
                {
                    gates = value;
                    RaisePropertyChanged("Gates");
                }
            }
        }

        // Выбранный проход
        private Gate selectedGate;
        public Gate SelectedGate
        {
            get { return selectedGate; }
            set
            {
                if (selectedGate != value)
                {
                    selectedGate = value;
                    RaisePropertiesChanged("SelectedGate");
                }
            }
        }

        public GateControlViewModel(SKUDDatabaseConnection _SKUDDatabaseConnection)
        {
            skudDatabaseConnection = _SKUDDatabaseConnection;
            try
            {
                Gates = skudDatabaseConnection.GetGates();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Не удалось получить список проходных: " + ex.Message, "Ошибка соединения с сервером", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void Show()
        {
            GateControlWindow gateControlWindow = new GateControlWindow() { DataContext = this };
            gateControlWindow.ShowDialog();
        }

        #region commands

        private DelegateCommand openEntranceGateCommand;
        /// <summary>
        /// Команда открытия турникета на вход
        /// </summary>
        public ICommand OpenEntranceGateCommand
        {
            get
            {
                if (openEntranceGateCommand == null)
                {
                    if (openEntranceGateCommand == null)
                        openEntranceGateCommand = new DelegateCommand(OpenEntranceGate);
                }
                return openEntranceGateCommand;
            }
        }

        /// <summary>
        /// Открывает выбранный турникет на вход
        /// </summary>
        private void OpenEntranceGate()
        {
            if(SelectedGate != null)
            {
                RFIDReaderController controller = skudDatabaseConnection.GetControllerById(SelectedGate.ControllerId);
                try
                {
                    Z5rwebConnector connector = new Z5rwebConnector(IPAddress.Parse(controller.Ip), (ushort)Int32.Parse(controller.SerialNumber));
                    connector.Connect();
                    connector.OpenLock(0);
                    connector.Disconnect();
                    MessageBox.Show("Турникет успешно открыт на вход", "Открытие турникета", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Не удалось открыть турникет: " + ex.Message, "Ошибка открытия турникета", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Пожалуйста, сначала выберите проходную", "Открытие турникета", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private DelegateCommand openExitGateCommand;
        /// <summary>
        /// Команда открытия турникета на выход
        /// </summary>
        public ICommand OpenExitGateCommand
        {
            get
            {
                if (openExitGateCommand == null)
                {
                    if (openExitGateCommand == null)
                        openExitGateCommand = new DelegateCommand(OpenExitGate);
                }
                return openExitGateCommand;
            }
        }

        /// <summary>
        /// Открывает выбранный турникет на выход
        /// </summary>
        private void OpenExitGate()
        {
            if (SelectedGate != null)
            {
                RFIDReaderController controller = skudDatabaseConnection.GetControllerById(SelectedGate.ControllerId);
                try
                {
                    Z5rwebConnector connector = new Z5rwebConnector(IPAddress.Parse(controller.Ip), (ushort)Int32.Parse(controller.SerialNumber));
                    connector.Connect();
                    connector.OpenLock(1);
                    connector.Disconnect();
                    MessageBox.Show("Турникет успешно открыт на выход", "Открытие турникета", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Не удалось открыть турникет: " + ex.Message, "Ошибка открытия турникета", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Пожалуйста, сначала выберите проходную", "Открытие турникета", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private DelegateCommand setFreeModeCommand;
        /// <summary>
        /// Команда перевода турникета в свободный режим
        /// </summary>
        public ICommand SetFreeModeCommand
        {
            get
            {
                if (setFreeModeCommand == null)
                {
                    if (setFreeModeCommand == null)
                        setFreeModeCommand = new DelegateCommand(SetFreeMode);
                }
                return setFreeModeCommand;
            }
        }

        /// <summary>
        /// Переводит турникет в свободный режим
        /// </summary>
        private void SetFreeMode()
        {
            if (SelectedGate != null)
            {
                RFIDReaderController controller = skudDatabaseConnection.GetControllerById(SelectedGate.ControllerId);
                try
                {
                    Z5rwebConnector connector = new Z5rwebConnector(IPAddress.Parse(controller.Ip), (ushort)Int32.Parse(controller.SerialNumber));
                    connector.Connect();
                    if(connector.GetControllerMode() == ZGuard.ZG_CTR_MODE.ZG_MODE_FREE)
                    {
                        MessageBox.Show("Данный турникет уже находится в свободном режиме", "Перевод в свободный режим", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        connector.Disconnect();
                        return;
                    }
                    else
                    {
                        connector.SetControllerMode(ZGuard.ZG_CTR_MODE.ZG_MODE_FREE);
                    }
                    connector.Disconnect();
                    MessageBox.Show("Турникет успешно переведен в свободный режим", "Перевод в свободный режим", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Не удалось перевести в свободный режим: " + ex.Message, "Ошибка перевода в свободный режим", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Пожалуйста, сначала выберите проходную", "Перевод в свободный режим", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private DelegateCommand setNormalModeCommand;
        /// <summary>
        /// Команда перевода турникета в нормальный режим
        /// </summary>
        public ICommand SetNormalModeCommand
        {
            get
            {
                if (setNormalModeCommand == null)
                {
                    if (setNormalModeCommand == null)
                        setNormalModeCommand = new DelegateCommand(SetNormalMode);
                }
                return setNormalModeCommand;
            }
        }

        /// <summary>
        /// Переводит турникет в нормальный режим
        /// </summary>
        private void SetNormalMode()
        {
            if (SelectedGate != null)
            {
                RFIDReaderController controller = skudDatabaseConnection.GetControllerById(SelectedGate.ControllerId);
                try
                {
                    Z5rwebConnector connector = new Z5rwebConnector(IPAddress.Parse(controller.Ip), (ushort)Int32.Parse(controller.SerialNumber));
                    connector.Connect();
                    if (connector.GetControllerMode() == ZGuard.ZG_CTR_MODE.ZG_MODE_NORMAL)
                    {
                        MessageBox.Show("Данный турникет уже находится в нормальном режиме", "Перевод в нормальный режим", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        connector.Disconnect();
                        return;
                    }
                    else
                    {
                        connector.SetControllerMode(ZGuard.ZG_CTR_MODE.ZG_MODE_NORMAL);
                    }
                    connector.Disconnect();
                    MessageBox.Show("Турникет успешно переведен в нормальный режим", "Перевод в нормальный режим", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Не удалось перевести в нормальный режим: " + ex.Message, "Ошибка перевода в нормальный режим", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Пожалуйста, сначала выберите проходную", "Перевод в нормальный режим", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        #endregion
    }
}
