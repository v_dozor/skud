﻿using DevExpress.Mvvm;
using SKUDDatabase;
using SKUDDatabase.DataModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SKUD_AutomatedWorkplace
{
    public class GateSelectionViewModel : ViewModelBase
    {
        GateSelectionWindow gateSelectionWindow;
        private WhiteListManagementViewModel wlmvm;
        private SKUDDatabaseConnection skudDatabaseConnection;

        // Список всех проходных
        private ObservableCollection<Gate> gates;
        public ObservableCollection<Gate> Gates
        {
            get { return gates; }
            set
            {
                if (gates != value)
                {
                    gates = value;
                    RaisePropertyChanged("Gates");
                }
            }
        }

        // Выбранный проход
        private Gate selectedGate;
        public Gate SelectedGate
        {
            get { return selectedGate; }
            set
            {
                if (selectedGate != value)
                {
                    selectedGate = value;
                    RaisePropertiesChanged("SelectedGate");
                }
            }
        }

        public GateSelectionViewModel(SKUDDatabaseConnection _skudDatabaseConnection, WhiteListManagementViewModel _wlmvm)
        {
            skudDatabaseConnection = _skudDatabaseConnection;

            Gates = skudDatabaseConnection.GetGates();
            wlmvm = _wlmvm;
        }

        public void Show()
        {
            gateSelectionWindow = new GateSelectionWindow() { DataContext = this };
            gateSelectionWindow.ShowDialog();
        }

        #region Commands

        private DelegateCommand addGateCommand;
        /// <summary>
        /// Команда добавления проходной
        /// </summary>
        public ICommand AddGateCommand
        {
            get
            {
                if (addGateCommand == null)
                {
                    if (addGateCommand == null)
                        addGateCommand = new DelegateCommand(AddGate);
                }
                return addGateCommand;
            }
        }

        /// <summary>
        /// Добавление проходной
        /// </summary>
        private void AddGate()
        {
            if(SelectedGate == null)
            {
                MessageBox.Show("Для добавления выделите нужную проходную в списке", "Ошибка добавления", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if(!wlmvm.Gates.Contains(SelectedGate))
            {
                wlmvm.Gates.Add(SelectedGate);
                gateSelectionWindow.Close();
            }
            else
            {
                MessageBox.Show("Данная проходная уже есть в списке", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        #endregion
    }
}
