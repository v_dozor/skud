﻿using ControllerConnectors;
using DevExpress.Mvvm;
using DozorUsbLib;
using Microsoft.Win32;
using SKUDDatabase;
using SKUDDatabase.DataModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using CsvHelper;


namespace SKUD_AutomatedWorkplace
{
    public class MainViewModel : ViewModelBase
    {
        // Настройки соединения с БД
        private String ip;
        private int port;
        private bool isLocalhost;

        private SKUDDatabaseConnection skudDatabaseConnection;

        private String currentRfid;
        public String CurrentRfid
        {
            get { return currentRfid; }
            set
            {
                if(currentRfid != value)
                {
                    currentRfid = value;
                    RaisePropertyChanged("CurrentRfid");
                }
            }
        }
        
        public MainViewModel()
        {            
            // соединение с БД
            Settings settings = Settings.Load();
            try 
            {
                if (settings == null)
                {
                    skudDatabaseConnection = new SKUDDatabaseConnection(ip, port, true);
                }
                else
                {
                    ip = settings.IP;
                    port = settings.Port;
                    isLocalhost = settings.IsLocalhost;
                    skudDatabaseConnection = new SKUDDatabaseConnection(ip, port, isLocalhost);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Не удалось соединиться с базой данных: " + ex.Message, "Ошибка соединения", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void Show()
        {
            MainWindow mainWindow = new MainWindow() { DataContext = this };
            mainWindow.Show();
        }

        #region Commands        

        private DelegateCommand serverSettingsCommand;
        /// <summary>
        /// Команда открытия окна настроек сервера
        /// </summary>
        public ICommand ServerSettingsCommand
        {
            get
            {
                if (serverSettingsCommand == null)
                {
                    serverSettingsCommand = new DelegateCommand(ServerSettings);
                }
                return serverSettingsCommand;
            }
        }

        /// <summary>
        /// Открывает окно настроек сервера
        /// </summary>
        private void ServerSettings()
        {
            ServerSettingsViewModel ssvm = new ServerSettingsViewModel();
            ssvm.Show();
        }

        private DelegateCommand openGateControlCommand;

        /// <summary>
        /// Команда открытия окна управления турникетами
        /// </summary>
        public ICommand OpenGateControlCommand
        {
            get
            {
                if(openGateControlCommand == null)
                {
                    openGateControlCommand = new DelegateCommand(OpenGateControl);
                }
                return openGateControlCommand;
            }
        }

        /// <summary>
        /// Управление турникетами
        /// </summary>
        private void OpenGateControl()
        {                
            GateControlViewModel gcvm = new GateControlViewModel(skudDatabaseConnection);
            gcvm.Show();
        }           

        private DelegateCommand rfidReadingModeCommand;

        /// <summary>
        /// Запуск режима считывания RFID-меток
        /// </summary>
        public ICommand RfidReadingModeCommand
        {
            get
            {
                if (rfidReadingModeCommand == null)
                {
                    rfidReadingModeCommand = new DelegateCommand(RfidReadingMode);
                }
                return rfidReadingModeCommand;
            }
        }

        /// <summary>
        /// Режим считывания RFID-меток
        /// </summary>
        private void RfidReadingMode()
        {
            ReadRfidModeViewModel rrmvm = new ReadRfidModeViewModel(skudDatabaseConnection);
            rrmvm.Show();
        }

        private DelegateCommand dbSearchModeCommand;

        /// <summary>
        /// Запуск режима поиска по базе 1С
        /// </summary>
        public ICommand DbSearchModeCommand
        {
            get
            {
                if (dbSearchModeCommand == null)
                {
                    dbSearchModeCommand = new DelegateCommand(DbSearchMode);
                }
                return dbSearchModeCommand;
            }
        }

        /// <summary>
        /// Режим поиска по базе 1С
        /// </summary>
        private void DbSearchMode()
        {
            DbSearchViewModel dsvm = new DbSearchViewModel(skudDatabaseConnection);
            dsvm.Show();
        }

        private DelegateCommand addHotelGuestModeCommand;

        /// <summary>
        /// Запуск режима добавления гостя
        /// </summary>
        public ICommand AddHotelGuestModeCommand
        {
            get
            {
                if (addHotelGuestModeCommand == null)
                {
                    addHotelGuestModeCommand = new DelegateCommand(AddHotelGuestMode);
                }
                return addHotelGuestModeCommand;
            }
        }

        /// <summary>
        /// Режим добавления гостя
        /// </summary>
        private void AddHotelGuestMode()
        {
            AddHotelGuestViewModel ahgvm = new AddHotelGuestViewModel(skudDatabaseConnection);
            ahgvm.Show();
        }

        private DelegateCommand dbHotelSearchModeCommand;

        /// <summary>
        /// Запуск режима поиска гостей по номеру в учетной книге
        /// </summary>
        public ICommand DbHotelSearchModeCommand
        {
            get
            {
                if (dbHotelSearchModeCommand == null)
                {
                    dbHotelSearchModeCommand = new DelegateCommand(DbHotelSearchMode);
                }
                return dbHotelSearchModeCommand;
            }
        }

        /// <summary>
        /// Режим поиска гостей по номеру в учетной книге
        /// </summary>
        private void DbHotelSearchMode()
        {
            DbHotelSearchViewModel dhsvm = new DbHotelSearchViewModel(skudDatabaseConnection);
            dhsvm.Show();
        }

        private DelegateCommand connector1CSettingsCommand;

        /// <summary>
        /// Команда настроек соединения с сервером 1С
        /// </summary>
        public ICommand Connector1CSettingsCommand
        {
            get
            {
                if (connector1CSettingsCommand == null)
                {
                    connector1CSettingsCommand = new DelegateCommand(Connector1CSettings);
                }
                return connector1CSettingsCommand;
            }
        }

        /// <summary>
        /// Настройки соединения с сервером 1С
        /// </summary>
        private void Connector1CSettings()
        {
            Connector1CSettingsViewModel c1Csvm = new Connector1CSettingsViewModel();
            c1Csvm.Show();
        }

        #endregion
    }
}
