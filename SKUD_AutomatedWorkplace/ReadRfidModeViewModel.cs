﻿using DevExpress.Mvvm;
using DozorUsbLib;
using SKUDDatabase;
using SKUDDatabase.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Votum._1C;
using Votum.Types;

namespace SKUD_AutomatedWorkplace
{
    public class ReadRfidModeViewModel : ViewModelBase
    {
        private SKUDDatabaseConnection skudDatabaseConnection;
        private RfidReader rfidReader;

        public bool canRead;

        private String currentRfid;
        public String CurrentRfid
        {
            get { return currentRfid; }
            set
            {
                if (currentRfid != value)
                {
                    currentRfid = value;
                    RaisePropertyChanged("CurrentRfid");                    
                }
            }
        }

        public ReadRfidModeViewModel(SKUDDatabaseConnection _skudDatabaseConnection)
        {
            skudDatabaseConnection = _skudDatabaseConnection;
            rfidReader = new RfidReader();
            rfidReader.Rfid_Updated += new EventHandler<RfidReaderEventArgs>(RfidReceived);
            canRead = true;
        }

        public void Show()
        {
            ReadRfidModeWindow readRfidModeWindow = new ReadRfidModeWindow() { DataContext = this };
            readRfidModeWindow.ShowDialog();
        }

        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            rfidReader.CloseDevices();
        }        

        private void RfidReceived(object sender, RfidReaderEventArgs args)
        {
            if(canRead)
            {
                try
                {
                    canRead = false;
                    CurrentRfid = args.Rfid;
                    Thread newWindowThread = new Thread(new ThreadStart(openWhiteListManagementWindow));
                    newWindowThread.SetApartmentState(ApartmentState.STA);
                    newWindowThread.IsBackground = true;
                    newWindowThread.Start();     
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Ошибка считывания метки: " + ex.Message, "Ошибка считывания", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }            
        }

        private void openWhiteListManagementWindow()
        {
            try
            {
                bool is1C = true;
                // АРМ для колледжа
                if(is1C)
                {
                    // Получаем студента по считанной метке
                    Settings settings = Settings.Load();                    
                    try
                    {
                        Connector_1C connector = null;
                        if (settings != null)
                        {
                            String url = settings.Url;
                            String username = settings.Username;
                            String password = settings.Password;
                            connector = new Connector_1C(url, username, password);
                        }
                        else
                        {
                            connector = new Connector_1C();
                        }
                        IEnumerable<Student> students = new Student[0];
                        students = connector.GetStudentsByRFID(CurrentRfid);

                        Student student = null;
                        WhiteListManagementViewModel wlmvm;
                        if (students.Count() > 0)
                        {
                            student = students.ElementAt(0);
                            wlmvm = new WhiteListManagementViewModel(skudDatabaseConnection, student, this);
                        }
                        else
                        {
                            wlmvm = new WhiteListManagementViewModel(skudDatabaseConnection, new RfidLabelZ5R(CurrentRfid), this);
                        }
                        wlmvm.Show();
                        System.Windows.Threading.Dispatcher.Run();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Ошибка при запросе к серверу 1С: " + ex.Message,
                                        "Ошибка 1С", MessageBoxButton.OK, MessageBoxImage.Error);
                    }                    
                }
                // АРМ для гостиницы
                else
                {
                    // Получаем гостя по считанной метке
                    long labelId = skudDatabaseConnection.IsLabelExist(CurrentRfid);
                    HotelGuest hotelGuest = skudDatabaseConnection.GetHotelGuestByRfidId(labelId);
                    WhiteListManagementViewModel wlmvm;
                    if(hotelGuest != null)
                    {
                        wlmvm = new WhiteListManagementViewModel(skudDatabaseConnection, hotelGuest, this);
                    }
                    else
                    {
                        wlmvm = new WhiteListManagementViewModel(skudDatabaseConnection, new RfidLabelZ5R(CurrentRfid), this);
                    }
                    wlmvm.Show();
                    System.Windows.Threading.Dispatcher.Run();
                }                
            }
            catch(Exception ex)
            {
                MessageBox.Show("Произошла ошибка: " + ex.Message + " " + ex.StackTrace, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
