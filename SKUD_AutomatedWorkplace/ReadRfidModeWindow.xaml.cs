﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SKUD_AutomatedWorkplace
{
    /// <summary>
    /// Interaction logic for ReadRfidModeWindow.xaml
    /// </summary>
    public partial class ReadRfidModeWindow : Window
    {
        private ReadRfidModeViewModel viewModel;

        public ReadRfidModeWindow()
        {
            InitializeComponent();
            Loaded += OnReadRfidModeWindowLoaded;            
        }

        private void OnReadRfidModeWindowLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            viewModel = DataContext as ReadRfidModeViewModel;
            Closing += viewModel.OnWindowClosing;
        }
    }
}
