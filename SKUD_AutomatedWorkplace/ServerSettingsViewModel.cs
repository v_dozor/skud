﻿using DevExpress.Mvvm;
using SKUDDatabase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SKUD_AutomatedWorkplace
{
    public class ServerSettingsViewModel : ViewModelBase
    {
        private bool isLocalhost;
        public Boolean IsLocalhost
        {
            get { return isLocalhost; }
            set
            {
                if (isLocalhost != value)
                {
                    isLocalhost = value;
                    IP = "127.0.0.1";
                    Port = "3306";
                    RaisePropertyChanged("IsLocalhost");
                }
            }
        }

        private string ip;
        public String IP
        {
            get { return ip; }
            set
            {
                if(ip != value)
                {
                    ip = value;
                    RaisePropertyChanged("IP");
                }
            }
        }

        private string port;
        public String Port
        {
            get { return port; }
            set
            {
                if (port != value)
                {
                    port = value;
                    RaisePropertyChanged("Port");
                }
            }
        }

        public ServerSettingsViewModel()
        {
            Settings settings = Settings.Load();
            if(settings != null)
            {
                IsLocalhost = settings.IsLocalhost;
                IP = settings.IP;
                Port = Convert.ToString(settings.Port);
            }
            else
            {
                IsLocalhost = true;
                IP = "127.0.0.1";
                Port = Convert.ToString(3306);
            }
        }

        public void Show()
        {
            ServerSettingsWindow serverSettingsWindow = new ServerSettingsWindow() { DataContext = this };
            serverSettingsWindow.Show();
        }

        private DelegateCommand saveCommand;
        /// <summary>
        /// Команда сохранения настроек сервера
        /// </summary>
        public ICommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new DelegateCommand(Save);
                }
                return saveCommand;
            }
        }

        /// <summary>
        /// Сохраняет настройки в файле конфигурации
        /// </summary>
        private void Save()
        {
            Settings settings = new Settings();
            settings.IsLocalhost = IsLocalhost;
            if(!IsLocalhost)
            {
                settings.IP = IP;
                settings.Port = Int32.Parse(Port);
            }
            else
            {
                settings.IP = "127.0.0.1";
                settings.Port = 3306;
            }
            settings.Save();
            MessageBox.Show("Настройки сервера успешно сохранены", "Сохранение настроек", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
