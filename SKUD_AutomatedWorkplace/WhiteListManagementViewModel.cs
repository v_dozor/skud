﻿using ControllerConnectors;
using DevExpress.Mvvm;
using SKUDDatabase;
using SKUDDatabase.DataModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Votum._1C;
using Votum.Types;

namespace SKUD_AutomatedWorkplace
{
    public class WhiteListManagementViewModel : ViewModelBase
    {
        private ReadRfidModeViewModel readRfidModeViewModel;
        private SKUDDatabaseConnection skudDatabaseConnection;
        private RfidLabelZ5R rfidLabelZ5R;

        private String currentRfid;
        public String CurrentRfid
        {
            get { return currentRfid; }
            set
            {
                if (currentRfid != value)
                {
                    currentRfid = value;
                    RaisePropertyChanged("CurrentRfid");
                }
            }
        }

        // список проходных, на которых разрешен доступ по данной метке, до всех изменений
        private List<Gate> initialGates { get; set; }

        // Список всех проходных
        private ObservableCollection<Gate> gates;
        public ObservableCollection<Gate> Gates
        {
            get { return gates; }
            set
            {
                if (gates != value)
                {
                    gates = value;
                    RaisePropertyChanged("Gates");
                }
            }
        }

        // Выбранный проход
        private Gate selectedGate;
        public Gate SelectedGate
        {
            get { return selectedGate; }
            set
            {
                if (selectedGate != value)
                {
                    selectedGate = value;
                    RaisePropertiesChanged("SelectedGate");
                }
            }
        }

        private Student student;
        private HotelGuest hotelGuest;

        // Информация о владельце карты
        private String currentPerson;
        public String CurrentPerson
        {
            get { return currentPerson; }
            set
            {
                if (currentPerson != value)
                {
                    currentPerson = value;
                    RaisePropertyChanged("СurrentPerson");
                }
            }
        }

        public WhiteListManagementViewModel(SKUDDatabaseConnection _skudDatabaseConnection, Student _student, ReadRfidModeViewModel _readRfidModeViewModel = null)
        {
            readRfidModeViewModel = _readRfidModeViewModel;
            skudDatabaseConnection = _skudDatabaseConnection;
            student = _student;
            CurrentPerson = student.Description;
            rfidLabelZ5R = new RfidLabelZ5R(_student.Rfid);
            CurrentRfid = rfidLabelZ5R.IdString;

            gates = skudDatabaseConnection.GetGatesByRfidLabel(rfidLabelZ5R);
            if(gates != null)
            {
                initialGates = gates.ToList();
            }                
        }

        public WhiteListManagementViewModel(SKUDDatabaseConnection _skudDatabaseConnection, RfidLabelZ5R rfid, ReadRfidModeViewModel _readRfidModeViewModel = null)
        {
            readRfidModeViewModel = _readRfidModeViewModel;
            skudDatabaseConnection = _skudDatabaseConnection;
            rfidLabelZ5R = rfid;
            CurrentRfid = rfid.IdString;

            gates = skudDatabaseConnection.GetGatesByRfidLabel(rfid);
            if (gates != null)
            {
                initialGates = gates.ToList();
            }   
        }

        public WhiteListManagementViewModel(SKUDDatabaseConnection _skudDatabaseConnection, HotelGuest _hotelGuest, ReadRfidModeViewModel _readRfidModeViewModel = null)
        {
            readRfidModeViewModel = _readRfidModeViewModel;
            skudDatabaseConnection = _skudDatabaseConnection;
            hotelGuest = _hotelGuest;
            CurrentPerson = "Гость номер " + hotelGuest.HotelId;

            Label label = skudDatabaseConnection.GetRfidById(hotelGuest.RfidLabelId);
            if(label != null)
            {
                rfidLabelZ5R = new RfidLabelZ5R(label.Rfid7);
                CurrentRfid = rfidLabelZ5R.IdString;
            }
            else
            {
                MessageBox.Show("Ошибка - RFID не найден в базе", "Управление доступом", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            gates = skudDatabaseConnection.GetGatesByRfidLabel(rfidLabelZ5R);
            if (gates != null)
            {
                initialGates = gates.ToList();
            }   
        }

        public void Show()
        {
            try
            {
                WhiteListManagementWindow whiteListManagementWindow = new WhiteListManagementWindow() { DataContext = this };
                whiteListManagementWindow.Show();         
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            if(readRfidModeViewModel != null)
            {
                readRfidModeViewModel.canRead = true;
            }
        }

        #region Commands

        private DelegateCommand addGateCommand;

        /// <summary>
        /// Приложение входит в режим обновления белого списка по считыванию RFID-меток
        /// </summary>
        public ICommand AddGateCommand
        {
            get
            {
                if (addGateCommand == null)
                {
                    addGateCommand = new DelegateCommand(AddGate);
                }
                return addGateCommand;
            }
        }

        /// <summary>
        /// Режим считывания меток
        /// </summary>
        private void AddGate()
        {
            GateSelectionViewModel gsvm = new GateSelectionViewModel(skudDatabaseConnection, this);
            gsvm.Show();
        }

        private DelegateCommand deleteGateCommand;

        /// <summary>
        /// Команда удаления проходной из списка
        /// </summary>
        public ICommand DeleteGateCommand
        {
            get
            {
                if (deleteGateCommand == null)
                {
                    deleteGateCommand = new DelegateCommand(DeleteGate);
                }
                return deleteGateCommand;
            }
        }

        /// <summary>
        /// Удаление проходной из списка
        /// </summary>
        private void DeleteGate()
        {
            if(SelectedGate != null)
            {
                Gates.Remove(SelectedGate);
            }
            else
            {
                MessageBox.Show("Для удаления выделите нужную проходную в списке", "Ошибка удаления", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private DelegateCommand saveWhiteListChangesCommand;

        /// <summary>
        /// Команда обновления белого списка на контроллерах
        /// </summary>
        public ICommand SaveWhiteListChangesCommand
        {
            get
            {
                if (saveWhiteListChangesCommand == null)
                {
                    saveWhiteListChangesCommand = new DelegateCommand(SaveWhiteListChanges);
                }
                return saveWhiteListChangesCommand;
            }
        }

        /// <summary>
        /// Обновляет белый список на контроллерах
        /// </summary>
        private void SaveWhiteListChanges()
        {
            // удаляем из Белого списка
            List<RFIDReaderController> controllers = new List<RFIDReaderController>();   
            try
            {
                foreach (Gate initialGate in initialGates)
                {
                    bool delete = true;
                    // если проходная отсутствует в окончательном списке, добавляем ее в список на удаление
                    foreach (Gate gate in Gates)
                    {
                        if (gate == initialGate)
                        {
                            delete = false;
                            break;
                        }
                    }
                    if (delete)
                    {
                        RFIDReaderController controller = skudDatabaseConnection.GetControllerById(initialGate.ControllerId);
                        if (!controllers.Contains(controller))
                        {
                            controllers.Add(controller);
                        }
                    }
                }
                foreach (RFIDReaderController controller in controllers)
                {
                    Z5rwebConnector connector = new Z5rwebConnector(IPAddress.Parse(controller.Ip), (ushort)Convert.ToInt32(controller.SerialNumber));
                    try
                    {
                        connector.Connect();
                        connector.ClearKey(rfidLabelZ5R.IdZ5R);
                        long labelId = skudDatabaseConnection.IsLabelExist(rfidLabelZ5R.IdString);
                        skudDatabaseConnection.DeleteLabelReader(labelId, controller.Id);
                        MessageBox.Show("Метка успешно удалена из белого списка контроллера " + controller.SerialNumber, "Операция завершена успешно", MessageBoxButton.OK, MessageBoxImage.Information);
                        connector.Disconnect();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Контроллер " + controller.SerialNumber + ": " + ex.Message, "Ошибка удаления метки из белого списка", MessageBoxButton.OK, MessageBoxImage.Error);
                        connector.Disconnect();
                    }
                }

                // добавляем в Белый список
                controllers.Clear();
                foreach (Gate gate in Gates)
                {
                    bool add = true;
                    // если в окончательном списке появилась новая проходная, добавляем ее в список на добавление
                    foreach (Gate initialGate in initialGates)
                    {
                        if (gate == initialGate)
                        {
                            add = false;
                            break;
                        }
                    }
                    if (add)
                    {
                        RFIDReaderController controller = skudDatabaseConnection.GetControllerById(gate.ControllerId);
                        if (!controllers.Contains(controller))
                        {
                            controllers.Add(controller);
                        }
                    }
                }
                foreach (RFIDReaderController controller in controllers)
                {
                    Z5rwebConnector connector = new Z5rwebConnector(IPAddress.Parse(controller.Ip), (ushort)Convert.ToInt32(controller.SerialNumber));
                    try
                    {
                        connector.Connect();
                        connector.WriteKey(new byte[][] { rfidLabelZ5R.IdZ5R });
                        long labelId = skudDatabaseConnection.IsLabelExist(rfidLabelZ5R.IdString);
                        if (labelId != -1)
                        {
                            skudDatabaseConnection.InsertLabelReader(labelId, controller.Id);
                        }
                        else
                        {
                            labelId = skudDatabaseConnection.InsertLabelIfNotExists(rfidLabelZ5R);
                            skudDatabaseConnection.InsertLabelReader(labelId, controller.Id);
                        }
                        MessageBox.Show("Метка успешно записана в белый список контроллера " + controller.SerialNumber, "Операция завершена успешно", MessageBoxButton.OK, MessageBoxImage.Information);
                        connector.Disconnect();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Контроллер " + controller.SerialNumber + ": " + ex.Message, "Ошибка добавления метки в Белый список", MessageBoxButton.OK, MessageBoxImage.Error);
                        connector.Disconnect();
                    }
                }

                if(Gates != null)
                    initialGates = Gates.ToList();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Произошла ошибка: " + ex.Message + " " + ex.StackTrace, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }            
        }     

        #endregion
    }
}
