﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SKUD_AutomatedWorkplace
{
    /// <summary>
    /// Interaction logic for WhiteListManagementWindow.xaml
    /// </summary>
    public partial class WhiteListManagementWindow : Window
    {
        private WhiteListManagementViewModel viewModel;

        public WhiteListManagementWindow()
        {
            InitializeComponent();
            Loaded += OnWhiteListManagementWindowLoaded; 
        }

        private void OnWhiteListManagementWindowLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            viewModel = DataContext as WhiteListManagementViewModel;
            Closing += viewModel.OnWindowClosing;
        }
    }
}
