﻿using DevExpress.Mvvm;
using SKUDDatabase;
using SKUDDatabase.DataModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SKUD_Configurator
{
    public class ControllerAddingViewModel : ViewModelBase
    {
        private ControllerAddingWindow caw;

        private SKUDDatabaseConnection skudDatabaseConnection;

        private string serialNumber;
        public String SerialNumber
        {
            get { return serialNumber; }
            set
            {
                if(serialNumber != value)
                {
                    serialNumber = value;
                }
            }
        }

        private string ip;
        public String Ip
        {
            get { return ip; }
            set
            {
                if (ip != value)
                {
                    ip = value;
                }
            }
        }

        private string port;
        public String Port
        {
            get { return port; }
            set
            {
                if (port != value)
                {
                    port = value;
                }
            }
        }        

        public ControllerAddingViewModel(SKUDDatabaseConnection _skudDatabaseConnection)
        {
            caw = new ControllerAddingWindow() { DataContext = this };

            skudDatabaseConnection = _skudDatabaseConnection;
        }

        public void Show()
        {            
            caw.ShowDialog();
        }

        #region commands

        private DelegateCommand addControllerCommand;
        /// <summary>
        /// Команда добавления данных о контроллере в БД
        /// </summary>
        public ICommand AddControllerCommand
        {
            get
            {
                if (addControllerCommand == null)
                {
                    addControllerCommand = new DelegateCommand(AddController);
                }
                return addControllerCommand;
            }
        }

        /// <summary>
        /// Добавляет контроллер в БД
        /// </summary>
        private void AddController()
        {
            if(SerialNumber != null && Ip != null && Port != null)
            {
                int portValue;
                if(Int32.TryParse(Port, out portValue))
                {
                    String query = "INSERT INTO RFID_READERS_TABLE (SERIAL_NUMBER, IP, PORT) VALUES('" +
                                SerialNumber + "', '" + Ip + "', '" + Port + "')";
                    skudDatabaseConnection.Insert(query);
                    MessageBox.Show("Запись успешно добавлена в базу данных", "Добавление записи", MessageBoxButton.OK, MessageBoxImage.Information);
                    caw.Close();
                }
                else
                {
                    MessageBox.Show("Поле \"Порт\" может принимать только числовые значения", "Ошибка ввода данных", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Поля не должны быть пустыми", "Ошибка ввода данных", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion
    }
}
