﻿using DevExpress.Mvvm;
using SKUDDatabase;
using SKUDDatabase.DataModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SKUD_Configurator
{
    public class GateAddingViewModel : ViewModelBase
    {
        #region Properties

        private GateAddingWindow gaw;

        private SKUDDatabaseConnection skudDatabaseConnection;

        // название прохода
        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                if(name != value)
                {
                    name = value;
                }
            }
        }

        // коллекция всех контроллеров
        private ObservableCollection<RFIDReaderController> controllers;
        public ObservableCollection<RFIDReaderController> Controllers
        {
            get { return controllers; }
            set
            {
                if(controllers != value)
                {
                    controllers = value;
                    RaisePropertyChanged("Controllers");
                }
            }
        }

        // Выбранный контроллер
        private RFIDReaderController selectedController;
        public RFIDReaderController SelectedController
        {
            get { return selectedController; }
            set
            {
                if(selectedController != value)
                {
                    selectedController = value;
                    RaisePropertyChanged("SelectedController");
                }
            }
        }

        #endregion

        public GateAddingViewModel(SKUDDatabaseConnection _skudDatabaseConnection)
        {
            skudDatabaseConnection = _skudDatabaseConnection;

            Controllers = skudDatabaseConnection.GetRfidReaderControllers();
            if(Controllers != null)
            {
                if(Controllers.Count > 0)
                {
                    SelectedController = Controllers.ElementAt(0);
                }
            }
        }

        public void Show()
        {
            gaw = new GateAddingWindow() { DataContext = this };
            gaw.ShowDialog();
        }

        #region commands

        private DelegateCommand addGateCommand;
        /// <summary>
        /// Команда добавления данных о проходе в БД
        /// </summary>
        public ICommand AddGateCommand
        {
            get
            {
                if (addGateCommand == null)
                {
                    addGateCommand = new DelegateCommand(AddGate);
                }
                return addGateCommand;
            }
        }

        /// <summary>
        /// Добавляет проход в БД
        /// </summary>
        private void AddGate()
        {
            if (Name != null)
            {
                String query = "INSERT INTO GATES_TABLE (NAME, RFID_READER_ID) VALUES('" +
                    Name + "', " +
                    SelectedController.Id +
                    ")";
                    skudDatabaseConnection.Insert(query);
                    MessageBox.Show("Запись успешно добавлена в базу данных", "Добавление записи", MessageBoxButton.OK, MessageBoxImage.Information);
                    gaw.Close();
            }
            else
            {
                MessageBox.Show("Поле \"Название прохода\" не должно быть пустым", "Ошибка ввода данных", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion
    }
}
