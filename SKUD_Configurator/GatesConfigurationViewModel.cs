﻿using DevExpress.Mvvm;
using SKUDDatabase;
using SKUDDatabase.DataModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SKUD_Configurator
{
    public class GatesConfigurationViewModel : ViewModelBase
    {
        private SKUDDatabaseConnection skudDatabaseConnection;

        // Список всех проходных
        private ObservableCollection<Gate> gates;
        public ObservableCollection<Gate> Gates
        {
            get { return gates; }
            set
            {
                if (gates != value)
                {
                    gates = value;
                    RaisePropertyChanged("Gates");
                }
            }
        }

        // Выбранный проход
        private Gate selectedGate;
        public Gate SelectedGate
        {
            get { return selectedGate; }
            set
            {
                if (selectedGate != value)
                {
                    selectedGate = value;
                    RaisePropertiesChanged("SelectedGate");
                    foreach(RFIDReaderController controller in controllers)
                    {
                        if(controller.Id == selectedGate.ControllerId)
                        {
                            SelectedController = controller;
                        }
                    }
                }
            }
        }

        // коллекция всех контроллеров
        private ObservableCollection<RFIDReaderController> controllers;
        public ObservableCollection<RFIDReaderController> Controllers
        {
            get { return controllers; }
            set
            {
                if (controllers != value)
                {
                    controllers = value;
                    RaisePropertyChanged("Controllers");
                }
            }
        }

        // Выбранный контроллер
        private RFIDReaderController selectedController;
        public RFIDReaderController SelectedController
        {
            get { return selectedController; }
            set
            {
                if (selectedController != value)
                {
                    selectedController = value;
                    RaisePropertyChanged("SelectedController");                    
                }
            }
        }

        public GatesConfigurationViewModel(SKUDDatabaseConnection _skudDatabaseConnection)
        {
            skudDatabaseConnection = _skudDatabaseConnection;

            // получаем список всех проходных            
            Gates = skudDatabaseConnection.GetGates();
            if (Gates == null)
            {
                Gates = new ObservableCollection<Gate>();
            }

            // получаем список всех контроллеров
            Controllers = skudDatabaseConnection.GetRfidReaderControllers();
            if (Controllers != null)
            {
                if (Controllers.Count > 0)
                {
                    SelectedController = Controllers.ElementAt(0);
                }
            }
        }

        public void Show()
        {
            GatesWindow gw = new GatesWindow() { DataContext = this };
            gw.Show();
        }

        #region commands

        private DelegateCommand gateAddingCommand;
        /// <summary>
        /// Команда открытия окна добавления прохода
        /// </summary>
        public ICommand GateAddingCommand
        {
            get
            {
                if (gateAddingCommand == null)
                {
                    gateAddingCommand = new DelegateCommand(GatesAdding);
                }
                return gateAddingCommand;
            }
        }

        /// <summary>
        /// Открывает окно добавления прохода
        /// </summary>
        private void GatesAdding()
        {
            GateAddingViewModel gavm = new GateAddingViewModel(skudDatabaseConnection);
            gavm.Show();
            // получаем список всех проходных            
            Gates = skudDatabaseConnection.GetGates();
            if (Gates == null)
            {
                Gates = new ObservableCollection<Gate>();
            }
        }

        private DelegateCommand updateGateCommand;
        /// <summary>
        /// Команда редактирования проходной
        /// </summary>
        public ICommand UpdateGateCommand
        {
            get
            {
                if (updateGateCommand == null)
                {
                    updateGateCommand = new DelegateCommand(UpdateGate);
                }
                return updateGateCommand;
            }
        }

        /// <summary>
        /// Обновляет запись проходной в БД 
        /// </summary>
        private void UpdateGate()
        {
            if (SelectedGate != null && SelectedGate.Name != null)
            {
                skudDatabaseConnection.UpdateGateById(SelectedGate);
                MessageBox.Show("Запись успешно отредактирована", "Редактирование записи", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("Поля не должны быть пустыми", "Ошибка ввода данных", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private DelegateCommand deleteGateCommand;
        /// <summary>
        /// Команда удаления проходной
        /// </summary>
        public ICommand DeleteGateCommand
        {
            get
            {
                if (deleteGateCommand == null)
                {
                    deleteGateCommand = new DelegateCommand(DeleteGate);
                }
                return deleteGateCommand;
            }
        }

        /// <summary>
        /// Удаляет проходную из БД
        /// </summary>
        private void DeleteGate()
        {
            if (SelectedGate != null)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Вы уверены, что хотите удалить данные?", "Удаление проходной", System.Windows.MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {                    
                    skudDatabaseConnection.DeleteById(DbConstants.GatesTableName, SelectedGate.Id);
                    Gates.Remove(SelectedGate);
                    MessageBox.Show("Запись успешно удалена", "Удаление записи", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }

        #endregion
    }
}
