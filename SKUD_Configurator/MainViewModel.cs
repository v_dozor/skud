﻿using DevExpress.Mvvm;
using SKUDDatabase;
using SKUDDatabase.DataModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SKUD_Configurator
{
    public class MainViewModel : ViewModelBase
    {
        #region properties

        private SKUDDatabaseConnection skudDatabaseConnection;

        // Настройки соединения с БД
        private String ip;
        private int port;
        private bool isLocalhost;

        // Список всех контроллеров
        private ObservableCollection<RFIDReaderController> controllers;
        public ObservableCollection<RFIDReaderController> Controllers
        {
            get { return controllers; }
            set
            {
                if(controllers != value)
                {
                    controllers = value;
                    RaisePropertyChanged("Controllers");
                }
            }
        }

        // Выбранный контроллер
        private RFIDReaderController selectedController;
        public RFIDReaderController SelectedController
        {
            get { return selectedController; }
            set
            {
                if(selectedController != value)
                {
                    selectedController = value;
                    RaisePropertyChanged("SelectedController");
                }
            }
        }        

        #endregion

        public MainViewModel()
        {
            // соединение с БД
            try
            {
                Settings settings = Settings.Load();
                if (settings == null)
                {
                    skudDatabaseConnection = new SKUDDatabaseConnection("127.0.0.1", 3306, true);
                }
                else
                {
                    ip = settings.IP;
                    port = settings.Port;
                    isLocalhost = settings.IsLocalhost;
                    skudDatabaseConnection = new SKUDDatabaseConnection(ip, port, isLocalhost);
                }

                // получаем список контроллеров                     
                Controllers = skudDatabaseConnection.GetRfidReaderControllers();
                if (Controllers == null)
                {
                    Controllers = new ObservableCollection<RFIDReaderController>();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Не удалось соединиться с сервером БД: " + ex.Message, "Ошибка соединения", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #region methods

        public void Show()
        {
            MainWindow mw = new MainWindow() { DataContext = this };
            mw.Show();
        }        

        #endregion

        #region commands

        private DelegateCommand gatesConfigurationCommand;
        /// <summary>
        /// Команда открытия окна конфигурации проходов
        /// </summary>
        public ICommand GatesConfigurationCommand
        {
            get
            {
                if (gatesConfigurationCommand == null)
                {
                    gatesConfigurationCommand = new DelegateCommand(GatesConfiguration);
                }
                return gatesConfigurationCommand;
            }
        }

        /// <summary>
        /// Открывает окно конфигурации проходов
        /// </summary>
        private void GatesConfiguration()
        {
            GatesConfigurationViewModel gcvm = new GatesConfigurationViewModel(skudDatabaseConnection);
            gcvm.Show();
        }

        private DelegateCommand controllerAddingCommand;
        /// <summary>
        /// Команда открытия окна добавления контроллеров для работы с метками
        /// </summary>
        public ICommand ControllerAddingCommand
        {
            get
            {
                if (controllerAddingCommand == null)
                {
                    controllerAddingCommand = new DelegateCommand(ControllerAdding);
                }
                return controllerAddingCommand;
            }
        }

        /// <summary>
        /// Открывает окно добавления контроллеров по работе с метками
        /// </summary>
        private void ControllerAdding()
        {
            ControllerAddingViewModel cavm = new ControllerAddingViewModel(skudDatabaseConnection);
            cavm.Show();
            // получаем список контроллеров                     
            Controllers = skudDatabaseConnection.GetRfidReaderControllers();
            if (Controllers == null)
            {
                Controllers = new ObservableCollection<RFIDReaderController>();
            }
        }

        private DelegateCommand serverSettingsCommand;
        /// <summary>
        /// Команда открытия окна настроек сервера
        /// </summary>
        public ICommand ServerSettingsCommand
        {
            get
            {
                if (serverSettingsCommand == null)
                {
                    serverSettingsCommand = new DelegateCommand(ServerSettings);
                }
                return serverSettingsCommand;
            }
        }

        /// <summary>
        /// Открывает окно настроек сервера
        /// </summary>
        private void ServerSettings()
        {
            ServerSettingsViewModel ssvm = new ServerSettingsViewModel();
            ssvm.Show();
        }

        private DelegateCommand updateControllerCommand;
        /// <summary>
        /// Команда редактирования параметров контроллера
        /// </summary>
        public ICommand UpdateControllerCommand
        {
            get
            {
                if (updateControllerCommand == null)
                {
                    updateControllerCommand = new DelegateCommand(UpdateController);
                }
                return updateControllerCommand;
            }
        }

        /// <summary>
        /// Обновляет запись контроллера в БД 
        /// </summary>
        private void UpdateController()
        {            
            if (SelectedController != null && SelectedController.SerialNumber != null && SelectedController.Ip != null)
            {
                skudDatabaseConnection.UpdateControllerById(SelectedController);
                MessageBox.Show("Запись успешно отредактирована", "Редактирование записи", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("Поля не должны быть пустыми", "Ошибка ввода данных", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private DelegateCommand deleteControllerCommand;
        /// <summary>
        /// Команда удаления контроллера
        /// </summary>
        public ICommand DeleteControllerCommand
        {
            get
            {
                if (deleteControllerCommand == null)
                {
                    deleteControllerCommand = new DelegateCommand(DeleteController);
                }
                return deleteControllerCommand;
            }
        }

        /// <summary>
        /// Удаляет запись контроллера в БД 
        /// </summary>
        private void DeleteController()
        {
            if(SelectedController != null)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Вы уверены, что хотите удалить данные?", "Удаление контроллера", System.Windows.MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    if(skudDatabaseConnection.IsControllerInstalledOnGate(SelectedController.Id))
                    {
                        MessageBox.Show("Нельзя удалить контроллер, так он установлен на проходной. Сначала уберите контроллер с проходной.", "Удаление записи", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    skudDatabaseConnection.DeleteById(DbConstants.RfidReadersTable, SelectedController.Id);
                    Controllers.Remove(SelectedController);
                    MessageBox.Show("Запись успешно удалена", "Удаление записи", MessageBoxButton.OK, MessageBoxImage.Information);
                }                
            }
        }

        #endregion
    }
}
