﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace VSKUD
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        Mutex m;
        MainViewModel mvm;
        DemonViewModel dvm;
        WinnerViewModel wvm;
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            //DispatcherUnhandledException += UnhandledDispatcherException;
            //TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;
            //AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            

            if (e.Args.Count() > 0)
            {
                if(e.Args.Contains("-run"))
                {
                    if (!checkSecondRun())
                        return;
                    dvm = new DemonViewModel();
                    dvm.Show();
                }
                else if(e.Args.Contains("-cfg"))
                {
                    if (!checkSecondRun())
                        return;
                    mvm = new MainViewModel();
                    mvm.Show();
                }
                else if(e.Args.Contains("-win"))
                {
                    wvm = new WinnerViewModel();
                    wvm.Show();
                }
            }
            if (mvm == null && dvm == null && wvm == null)
            {
                if (!checkSecondRun())
                    return;
                mvm = new MainViewModel();
                mvm.Show();
            }
        }

        private void UnhandledDispatcherException(Object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            // do your stuff here!
            e.Handled = true; // Ex is now handled and will not crash your app
        }


        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            throw new NotImplementedException();
        }


        //проверка на запуск второй копии приложения
        private bool checkSecondRun()
        {
            bool createdNew;
            m = new Mutex(true, "VSKUD_MUTEX", out createdNew);
            if (!createdNew)
            {
                MessageBox.Show("Один экземпляр приложения уже запущен, приложение закроется");
                Current.Shutdown();
            }
            return createdNew;
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            mvm?.Close();
            dvm?.Close();
        }
    }
}
