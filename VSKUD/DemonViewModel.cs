﻿using DevExpress.Mvvm;
using DozorUsbLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Votum.Types;

namespace VSKUD
{
    public class DemonViewModel : ViewModelBase
    {
        private MySqlDb db = new MySqlDb();
        private RfidReader reader;
        private IList<Gate> gates;
        HashSet<Passage> passages = new HashSet<Passage>();

        public DemonViewModel()
        {
            reader = new RfidReader();
            gates = db.Session.QueryOver<Gate>().List();
            reader.Rfid_Updated += Reader_Rfid_Updated;
        }

        private void Reader_Rfid_Updated(object sender, RfidReaderEventArgs e)
        {
            string readerId = e.ReceiverId.ToString() + e.SerialId.ToString();

            //получает проходную
            Gate gate = gates.FirstOrDefault(
                x =>
                {
                    return x.InnerReaderId == readerId ||
                           x.OuterReaderId == readerId;
                });

            if (gate == null)
                return;

            DateTime? tagLastDate = null;
            Passage lastTagPassage;

            if (passages.Count() > 0)
            {
                var tags = passages.Where(x => { return x.Tag == e.Rfid && x.Gate == gate; });

                if (tags.Count() > 0)
                {
                    tagLastDate = tags.Max(x => { return x.Date; });
                    lastTagPassage = tags.FirstOrDefault(x => { return x.Date == tagLastDate; });
                    
                    if(!(lastTagPassage == null ||
                        DateTime.Now > lastTagPassage.Date + TimeSpan.FromMinutes(1)))
                    {
                        return;
                    }
                }
            }
            
            Passage passage = new Passage()
            {
                Date = DateTime.Now,
                Gate = gate,
                Tag = e.Rfid,
                Inside = readerId == gate.InnerReaderId ? true : false
            };

            passages.Add(passage);

            db.Session.Save(passage);
        }


        #region view
        private NotifyIcon notifyIcon;

        public void Show()
        {
            if (notifyIcon != null)
                return;
            notifyIcon = new NotifyIcon();
            notifyIcon.Icon = new System.Drawing.Icon(
                        System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "icon.ico"));
            notifyIcon.Click += NotifyIcon_Click;
            notifyIcon.Visible = true;

            ContextMenu cm = new ContextMenu();

            MenuItem exit_item = new MenuItem();
            exit_item.Text = "Выход";
            exit_item.Click += Exit_Click;

            cm.MenuItems.Add(exit_item);

            notifyIcon.ContextMenu = cm;

        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Close();
            App.Current.Shutdown();
        }

        private void NotifyIcon_Click(object sender, EventArgs e)
        {
            
        }

        public void Close()
        {
            notifyIcon.Visible = false;
            notifyIcon.Dispose();
            reader?.CloseDevices();
        }
        #endregion

    }
}
