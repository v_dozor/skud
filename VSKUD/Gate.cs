﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSKUD
{
    public class Gate
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string InnerReaderId { get; set; }
        public virtual string OuterReaderId { get; set; }

        public override string ToString()
        {
            return Name;
        }

    }
}
