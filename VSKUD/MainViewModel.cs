﻿using System.Linq;
using DevExpress.Mvvm;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Collections.Generic;
using System;
using System.Collections;
using DozorUsbLib;
using System.Windows;

namespace VSKUD
{
    public class MainViewModel : ViewModelBase
    {
        MySqlDb db = new MySqlDb();
        RfidReader reader;
        string[] initedReaders = new string[2] { string.Empty, string.Empty };

        public MainViewModel()
        {
            reader = new RfidReader();
            reader.Rfid_Updated += Reader_Rfid_Updated;

            var gates = db.Session.QueryOver<Gate>().List();
            Gates = new ObservableCollection<Gate>(gates);
        }

        MainWindow wnd;
        public void Show()
        {
            if (wnd != null)
                return;

            wnd = new MainWindow() { DataContext = this };
            wnd.Show();
        }

        private void Reader_Rfid_Updated(object sender, RfidReaderEventArgs e)
        {
            string readerId = e.ReceiverId.ToString() + e.SerialId.ToString();

            //проверка на повторную инициализацю
            if (gates.Count() > 0)
            {
                var gate = gates.FirstOrDefault(
                    x => {
                        return x.InnerReaderId == readerId ||
                               x.OuterReaderId == readerId;
                    });
                if(gate != null)
                {
                    MessageBox.Show("Этот считыатель уже проинициализирован");
                    return;
                }
            }

            if(initedReaders[0] == string.Empty)
            {
                initedReaders[0] = readerId;
                LabelInside = "Инициализирован";
            }
            else if(initedReaders[1] == string.Empty &&
                initedReaders[0] != readerId)
            {
                initedReaders[1] = readerId;
                LabelOut = "Инициализирован";
            }
            
        }

        public void Close()
        {
            reader?.CloseDevices();
            wnd?.Close();
        }

        #region bindings
        private ObservableCollection<Gate> gates;
        public ObservableCollection<Gate> Gates
        {
            get
            {
                return gates;
            }
            set
            {
                gates = value;
                RaisePropertiesChanged("Gates");
            }
        }

        private string name = "";
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                RaisePropertiesChanged("Name");
            }
        }

        private string labelInside = "Поднесте метку к считывателю";
        public string LabelInside
        {
            get
            {
                return labelInside;
            }
            set
            {
                labelInside = value;
                RaisePropertiesChanged("LabelInside");
            }
        }

        private string labelOut = "Поднесте метку к считывателю";
        public string LabelOut
        {
            get
            {
                return labelOut;
            }
            set
            {
                labelOut = value;
                RaisePropertiesChanged("LabelOut");
            }
        }

        private DelegateCommand addCommand;
        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand = new DelegateCommand(Add);
                }
                return addCommand;
            }
        }
        private void Add()
        {
            if(Name.Length <= 0)
            {
                MessageBox.Show("Введите название проходной");
                return;
            }
            if(initedReaders[0] == string.Empty)
            {
                MessageBox.Show("Входной счиыватель не проининциализирован");
                return;
            }
            if (initedReaders[1] == string.Empty)
            {
                MessageBox.Show("Выходной счиыватель не проининциализирован");
                return;
            }

            Gate gate = new Gate();
            gate.Name = Name;
            gate.InnerReaderId = initedReaders[0];
            gate.OuterReaderId = initedReaders[1];

            db.Session.Save(gate);

            Gates.Add(gate);
            Clear();
        }

        private DelegateCommand clearCommand;
        public ICommand ClearCommand
        {
            get
            {
                if (clearCommand == null)
                {
                    clearCommand = new DelegateCommand(Clear);
                }
                return clearCommand;
            }
        }
        private void Clear()
        {
            Name = "";
            LabelInside = "Поднесте метку к считывателю";
            LabelOut = "Поднесте метку к считывателю";
            initedReaders[0] = string.Empty;
            initedReaders[1] = string.Empty;
        }
        #endregion
    }
}
