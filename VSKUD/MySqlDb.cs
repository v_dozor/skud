﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;

namespace VSKUD
{
    public class MySqlDb
    {
        public MySqlDb()
        {
            try
            {
                factory =
                    new NHibernate.
                    Cfg.
                    Configuration().
                    Configure(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "nhibernate.cfg.xml")).
                    AddFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Gate.hbm.xml")).
                    AddFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Passage.hbm.xml")).
                    BuildSessionFactory();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            
            OpenSession();
        }

        private ISessionFactory factory;
        public ISessionFactory Factory
        {
            get
            {
                return factory;
            }
        }

        private ISession session;
        public ISession Session
        {
            get
            {
                return session;
            }
        }

        /// <summary>
        /// Создает новую ссесию
        /// </summary>
        public ISession OpenSession()
        {
            
            session = factory.OpenSession();
            return session;
        }

        public void ExecuteSQLFile(string sqlFilePath)
        {
            string sql = string.Empty;

            using (FileStream strm = File.OpenRead(sqlFilePath))
            {
                var reader = new StreamReader(strm);
                sql = reader.ReadToEnd();
            }

            var regex = new Regex("^GO", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            string[] lines = regex.Split(sql);

            foreach (string line in lines)
            {
                IQuery query = session.CreateSQLQuery(line);
                query.ExecuteUpdate();
            }
        }
    }
}
