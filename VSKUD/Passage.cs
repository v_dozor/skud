﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSKUD
{
    public class Passage
    {
        private int id;
        public virtual int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        private bool inside;
        public virtual bool Inside
        {
            get
            {
                return inside;
            }
            set
            {
                inside = value;
            }
        }

        private string tag;
        public virtual string Tag
        {
            get
            {
                return tag;
            }
            set
            {
                tag = value;
            }
        }

        private DateTime date;
        public virtual DateTime Date
        {
            get
            {
                return date;
            }
            set
            {
                date = value;
            }
        }

        private Gate gate;
        public virtual Gate Gate
        {
            get
            {
                return gate;
            }
            set
            {
                gate = value;
            }
        }
    }
}