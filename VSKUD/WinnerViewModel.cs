﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using eRatingAPI;
using Eval.Framework.Abstracts;
using Eval.Authorization;

namespace VSKUD
{
    public class WinnerViewModel
    {

        MySqlDb db = new MySqlDb();

        public WinnerViewModel()
        {

        }

        public void Show()
        {
            var tagTime = new Dictionary<string, TimeSpan>();

            var tags = db.Session.QueryOver<Passage>()
            .Select(x => x.Tag)
            .SelectList(list => list.SelectGroup(x => x.Tag))
            .List<string>();

            if(tags.Count == 0)
            {
                MessageBox.Show("Нет ни одного прохода с меткой");
                App.Current.Shutdown();
                return;
            }

            //проходит по меткам
            foreach(var tag in tags)
            {
                TimeSpan userTimeInverval = TimeSpan.Zero;
                var tagPassages = db.Session.QueryOver<Passage>()
                    .Where(x => x.Tag == tag)
                    .OrderBy(x => x.Date).Asc
                    .List();
                var daysPassages = tagPassages.GroupBy(x => x.Date.ToString("yyyy.MM.dd"));
                
                //проходит по всем дням этой метки
                foreach(var dayPassages in daysPassages)
                {
                    var passages = dayPassages.Where(x => { return true; }).ToList();
                    int i = 0;
                    while (passages.Count > i)
                    {
                        if (passages[i].Inside == true)
                        {
                            Passage inside = passages[i];
                            //если подряд идут записи со входом, то пропускаем их
                            while (passages.Count > i && passages[i].Inside == true)
                            {
                                inside = passages[i];
                                ++i;
                            }
                            if(passages.Count > i)
                            {
                                Passage outside = passages[i];
                                userTimeInverval += outside.Date - inside.Date;
                            }
                        }
                        ++i;
                    }
                }

                tagTime.Add(tag, userTimeInverval);
            }

            if(tagTime.Count == 0)
            {
                MessageBox.Show("Нет ни одного прохода");
                App.Current.Shutdown();
                return;
            }

            List<int> winnersArray = new List<int>();

            foreach (var item in tagTime)
            {
                int[] arr = Enumerable.Repeat(tags.IndexOf(item.Key), (int)item.Value.TotalMinutes).ToArray();
                winnersArray.AddRange(arr);
            }

            if (winnersArray.Count == 0)
            {
                MessageBox.Show("Нет ни одного прохода");
                App.Current.Shutdown();
                return;
            }

            Random rnd = new Random(DateTime.Now.Millisecond);
            winnersArray = winnersArray.OrderBy(x => { return rnd.Next(); }).ToList();
            
            int index = rnd.Next(0, winnersArray.Count() - 1);

            var winnerTag = tags[winnersArray[index]];

            List<Member> members = new List<Member>();
            ERDatabase database = new ERDatabase();
            EvalObservableCollection<UserGroup> groups = database.GetGroups();
            foreach (UserGroup group in groups)
            {
                members.AddRange(database.GetGroupMembers(group));
            }
            var winner = members.FirstOrDefault(x => { return x.RFID == winnerTag; });

            if(winner == null)
            {
                MessageBox.Show(string.Format("Человек с меткой [{0}] не найден в e-Raiting", winnerTag));
                App.Current.Shutdown();
                return;
            }

            MessageBox.Show("Победитель " + winner.Name); 

        }
    }
}
