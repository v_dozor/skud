﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Votum._1C
{
    /// <summary>
    /// Класс сущности "Аудитории" в БД 1С
    /// </summary>
    [DataContract]
    public class Classroom
    {
        /// <summary>
        /// Идентификатор сущности
        /// </summary>
        [DataMember(Name = "Ref_Key")]
        public String RefKey { get; set; }

        /// <summary>
        /// Название аудитории
        /// </summary>
        [DataMember(Name = "Description")]
        public String Description { get; set; }
    }
}
