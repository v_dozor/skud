﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Votum._1C;

namespace Votum._1C
{
    public class Connector_1C : IDisposable
    {
        // параметры соединения
        private String baseUrl;
        private String username;
        private String password;

        private String CATALOG = "/odata/standard.odata/Catalog_";
        private String INFORMATION_REGISTER = "/odata/standard.odata/InformationRegister_";

        private HttpClient httpClient;

        public Connector_1C(String _baseUrl = "http://88.87.88.93:3280/college_test", 
                            String _username = "exchange_votum",
                            String _password = "eVNjvE")
        {
            baseUrl = _baseUrl;
            username = _username;
            password = _password;

            httpClient = new HttpClient();

            // устанавливаем параметры аутентификации
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                "Basic",
                Convert.ToBase64String(
                System.Text.ASCIIEncoding.ASCII.GetBytes(
                string.Format("{0}:{1}", username, password))));
            // тип контента - JSON
            httpClient.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        #region Students

        /// <summary>
        /// Возвращает список всех студентов из БД 1С
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Student> GetStudents()
        {
            String select = "?$select=Ref_Key,Description,Специальность_Key,Специализация_Key,RFID";
            var response = httpClient.GetAsync(baseUrl + CATALOG + "Студенты" + select).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;

                string responseString = @responseContent.ReadAsStringAsync().Result;

                JObject jsonResponse = JObject.Parse(responseString);
                var objResponse = jsonResponse["value"];

                var students = JsonConvert.DeserializeObject<IEnumerable<Student>>(objResponse.ToString());                

                return students;
            }
            return null;
        }

        /// <summary>
        /// Возвращает массив студентов, относящихся к заданной специализации
        /// </summary>
        /// <param name="specialtyRefKey">Идентификатор специализации</param>
        /// <returns></returns>
        public IEnumerable<Student> GetStudentsBySpecialization(String specializationRefKey)
        {
            String filter = "?$filter=Специализация_Key eq guid'" + specializationRefKey + "'";
            String select = "&$select=Ref_Key,Description,Специальность_Key,Специализация_Key,RFID";
            var response = httpClient.GetAsync(baseUrl + CATALOG + "Студенты" + filter + select).Result;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;

                string responseString = @responseContent.ReadAsStringAsync().Result;

                JObject jsonResponse = JObject.Parse(responseString);
                var objResponse = jsonResponse["value"];

                var students = JsonConvert.DeserializeObject<IEnumerable<Student>>(objResponse.ToString());

                return students;
            }
            return null;
        }

        /// <summary>
        /// Возвращает студента с заданным идентификатором
        /// </summary>
        /// <param name="refKey">Идентификатор студента</param>
        /// <returns></returns>
        public Student GetStudentByRefKey(String refKey)
        {
            String filter = "?$filter=Ref_Key eq guid'" + refKey + "'";
            String select = "&$select=Ref_Key,Description,Специальность_Key,Специализация_Key,RFID";
            var response = httpClient.GetAsync(baseUrl + CATALOG + "Студенты" + filter + select).Result;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;

                string responseString = @responseContent.ReadAsStringAsync().Result;

                JObject jsonResponse = JObject.Parse(responseString);
                var objResponse = jsonResponse["value"];

                var students = JsonConvert.DeserializeObject<IEnumerable<Student>>(objResponse.ToString());

                if(students.Count() > 0)
                {
                    return students.ElementAt(0);   
                }
            }
            return null;
        }

        /// <summary>
        /// Возвращает студента с заданным идентификатором
        /// </summary>
        /// <param name="students">Массив студентов</param>
        /// <param name="refKey">Идентификатор студента</param>
        /// <returns></returns>
        public Student GetStudentByRefKey(IEnumerable<Student> students, String refKey)
        {
            foreach(Student student in students)
            {
                if(student.RefKey == refKey)
                {
                    return student;
                }
            }
            return null;
        }

        /// <summary>
        /// Возвращает список студентов с заданным RFID
        /// </summary>
        /// <param name="rfid">Значение поля RFID</param>
        /// <returns></returns>
        public IEnumerable<Student> GetStudentsByRFID(String rfid)
        {
            String filter = "?$filter=RFID eq '" + rfid + "'";
            String select = "&$select=Ref_Key,Description,Специальность_Key,Специализация_Key,RFID";
            var response = httpClient.GetAsync(baseUrl + CATALOG + "Студенты" + filter + select).Result;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;

                string responseString = @responseContent.ReadAsStringAsync().Result;

                JObject jsonResponse = JObject.Parse(responseString);
                var objResponse = jsonResponse["value"];

                var students = JsonConvert.DeserializeObject<Student[]>(objResponse.ToString());

                return students;
            }
            return null;
        }

        /// <summary>
        /// Возвращает массив студентов, обучающихся в заданной группе
        /// </summary>
        /// <param name="groupRefKey">Идентификатор группы</param>
        /// <returns></returns>
        public List<Student> GetStudentsByGroup(String groupRefKey, 
                                                IEnumerable<ContingentMovement> contingentMovements = null,
                                                IEnumerable<Student> students = null)
        {
            if(contingentMovements == null)
            {
                contingentMovements = GetContingentMovements();
                students = GetStudents();
            }            

            List<String> groupStudents = new List<String>();

            foreach(ContingentMovement contingentMovement in contingentMovements)
            {
                if(contingentMovement.GroupKey == groupRefKey)
                {
                    groupStudents.Add(contingentMovement.StudentKey);
                }
            }

            List<Student> currentGroupStudents = new List<Student>();            

            foreach(String studentRefKey in groupStudents)
            {
                DateTime lastDateTime = new DateTime(1992, 11, 28);
                String currentGroupRefKey = groupRefKey;
                foreach (ContingentMovement contingentMovement in contingentMovements)
                {
                    if(contingentMovement.StudentKey == studentRefKey)
                    {
                        DateTime dateTime = DateTime.ParseExact(contingentMovement.Period,
                                            "yyyy-MM-ddTHH:mm:ss",
                                            CultureInfo.InvariantCulture);
                        if(dateTime > lastDateTime)
                        {
                            lastDateTime = dateTime;
                            currentGroupRefKey = contingentMovement.GroupKey;
                        }
                    }                    
                }
                if (currentGroupRefKey == groupRefKey)
                {
                    currentGroupStudents.Add(GetStudentByRefKey(students, studentRefKey));
                }
            }
            return currentGroupStudents;
        }        

        #endregion

        #region Специальности

        /// <summary>
        /// Возвращает список всех специальностей из БД 1С
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Specialty> GetSpecialities()
        {
            var response = httpClient.GetAsync(baseUrl + CATALOG + "Специальности").Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;

                string responseString = @responseContent.ReadAsStringAsync().Result;

                JObject jsonResponse = JObject.Parse(responseString);
                var objResponse = jsonResponse["value"];

                var specialities = JsonConvert.DeserializeObject<IEnumerable<Specialty>>(objResponse.ToString());

                return specialities;
            }
            return null;
        }

        #endregion

        #region Специализации

        /// <summary>
        /// Возвращает список всех специализаций из БД 1С
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Specialization> GetSpecializations()
        {
            var response = httpClient.GetAsync(baseUrl + CATALOG + "Специализации").Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;

                string responseString = @responseContent.ReadAsStringAsync().Result;

                JObject jsonResponse = JObject.Parse(responseString);
                var objResponse = jsonResponse["value"];

                var specializations = JsonConvert.DeserializeObject<IEnumerable<Specialization>>(objResponse.ToString());

                return specializations;
            }
            return null;
        }

        /// <summary>
        /// Возвращает массив специализаций, относящихся к заданной специальности
        /// </summary>
        /// <param name="specialtyRefKey">Идентификатор специальности</param>
        /// <returns></returns>
        public IEnumerable<Specialization> GetSpecializationsBySpecialty(String specialtyRefKey)
        {            
            String filter = "?$filter=Owner_Key eq guid'" + specialtyRefKey + "'";
            var response = httpClient.GetAsync(baseUrl + CATALOG + "Специализации" + filter).Result;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;

                string responseString = @responseContent.ReadAsStringAsync().Result;

                JObject jsonResponse = JObject.Parse(responseString);
                var objResponse = jsonResponse["value"];

                var specializations = JsonConvert.DeserializeObject<IEnumerable<Specialization>>(objResponse.ToString());

                return specializations;
            }
            return null;
        }

        #endregion

        #region Группы

        /// <summary>
        /// Возвращает массив учебных групп
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Group> GetGroups()
        {
            String select = "?$select=Ref_Key,Description,Специальность_Key,Специализация_Key";
            var response = httpClient.GetAsync(baseUrl + CATALOG + "УчебныеГруппы" + select).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;

                string responseString = @responseContent.ReadAsStringAsync().Result;

                JObject jsonResponse = JObject.Parse(responseString);
                var objResponse = jsonResponse["value"];

                var groups = JsonConvert.DeserializeObject<IEnumerable<Group>>(objResponse.ToString());

                return groups;
            }
            return null;
        }

        /// <summary>
        /// Возвращает массив учебных групп, относящихся к заданной специальности
        /// </summary>
        /// <param name="specializationRefKey"></param>
        /// <returns></returns>
        public IEnumerable<Group> GetGroupsBySpecialty(String specialtyRefKey)
        {
            String filter = "?$filter=Специальность_Key eq guid'" + specialtyRefKey + "'";
            String select = "&$select=Ref_Key,Description,Специальность_Key,Специализация_Key";
            var response = httpClient.GetAsync(baseUrl + CATALOG + "УчебныеГруппы" + filter + select).Result;

            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;

                string responseString = @responseContent.ReadAsStringAsync().Result;

                JObject jsonResponse = JObject.Parse(responseString);
                var objResponse = jsonResponse["value"];

                var groups = JsonConvert.DeserializeObject<IEnumerable<Group>>(objResponse.ToString());

                return groups;
            }
            return null;
        }

        /// <summary>
        /// Возвращает группу, в которой обучается заданный студент
        /// </summary>
        /// <param name="groupRefKey">Идентификатор группы</param>
        /// <returns></returns>
        public Group GetGroupByStudent(String studentRefKey,
                                       IEnumerable<ContingentMovement> contingentMovements = null,
                                       IEnumerable<Group> groups = null)
        {
            if (contingentMovements == null)
            {
                contingentMovements = GetContingentMovements();
                groups = GetGroups();
            }

            DateTime lastDateTime = new DateTime(1992, 11, 28);
            Group currentGroup = null;
            foreach (ContingentMovement contingentMovement in contingentMovements)
            {
                if (contingentMovement.StudentKey == studentRefKey)
                {
                    DateTime dateTime = DateTime.ParseExact(contingentMovement.Period,
                                            "yyyy-MM-ddTHH:mm:ss",
                                            CultureInfo.InvariantCulture);
                    if (dateTime > lastDateTime)
                    {
                        lastDateTime = dateTime;
                        foreach (Group group in groups)
                        {
                            if (group.RefKey == contingentMovement.GroupKey)
                            {
                                currentGroup = group;
                                break;
                            }
                        }
                    }
                }
            }
            return currentGroup;
        }

        #endregion

        #region Движения контингента

        /// <summary>
        /// Возвращает массив записей движения контингента
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ContingentMovement> GetContingentMovements()
        {
            String select = "?$select=RecordSet";
            var response = httpClient.GetAsync(baseUrl + INFORMATION_REGISTER + "ДвижениеКонтингента" + select).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;

                string responseString = @responseContent.ReadAsStringAsync().Result;

                JObject jsonResponse = JObject.Parse(responseString);
                JArray values = (JArray)jsonResponse["value"];

                ContingentMovement[] contingentMovements = new ContingentMovement[0];
                foreach(JObject obj in values)
                {
                    var recordSets = obj["RecordSet"];
                    var newContingentMovements = JsonConvert.DeserializeObject<ContingentMovement[]>(recordSets.ToString());

                    var tmp = contingentMovements;
                    contingentMovements = new ContingentMovement[tmp.Count() + newContingentMovements.Count()];
                    tmp.CopyTo(contingentMovements, 0);
                    newContingentMovements.CopyTo(contingentMovements, tmp.Count());
                }                             

                return contingentMovements;
            }
            return null;
        }

        #endregion       

        #region Расписание на дату

        /// <summary>
        /// Возвращает массив расписаний (только на текущий период обучения)
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Timetable> GetTimetable()
        {
            var response = httpClient.GetAsync(baseUrl + INFORMATION_REGISTER + "РасписаниеНаДату").Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;

                string responseString = @responseContent.ReadAsStringAsync().Result;

                JObject jsonResponse = JObject.Parse(responseString);
                JArray values = (JArray)jsonResponse["value"];

                Timetable[] timetables = new Timetable[0];

                // получаем периоды обучения, которые охватывают текущую дату
                IEnumerable<PeriodOfStudy> periods = GetPeriodsOfStudy();

                foreach (JObject obj in values)
                {
                    var recordSets = obj["RecordSet"];
                    var newTimetables = JsonConvert.DeserializeObject<List<Timetable>>(recordSets.ToString());

                    var fixed_newTimetables = newTimetables.Where(x => { return true; }).ToList();
                    // удаляем записи о расписании, не относящиеся к текущей дате
                    foreach (Timetable timetable in newTimetables)
                    {
                        bool delete = true;
                        foreach(PeriodOfStudy period in periods)
                        {
                            if(period.RefKey == timetable.PeriodOfStudyKey)
                            {
                                delete = false;
                                break;
                            }
                        }
                        if(delete)
                        {
                            fixed_newTimetables.Remove(timetable);
                        }
                    }

                    var tmp = timetables;
                    timetables = new Timetable[tmp.Count() + fixed_newTimetables.Count()];
                    tmp.CopyTo(timetables, 0);
                    fixed_newTimetables.CopyTo(timetables, tmp.Count());
                }

                return timetables;
            }
            return null;
        }

        /// <summary>
        /// Получаем периоды обучения, которые охватывают текущую дату
        /// </summary>
        private IEnumerable<PeriodOfStudy> GetPeriodsOfStudy()
        {
            var response = httpClient.GetAsync(baseUrl + CATALOG + "ПериодыОбучения").Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;

                string responseString = @responseContent.ReadAsStringAsync().Result;

                JObject jsonResponse = JObject.Parse(responseString);
                var objResponse = jsonResponse["value"];

                var periods = JsonConvert.DeserializeObject<List<PeriodOfStudy>>(objResponse.ToString());

                // удаляем периоды обучения, не охватывающие текущую дату
                var fixed_periods = periods.Where(x => { return true; }).ToList();
                foreach (PeriodOfStudy period in periods)
                {
                    try
                    {
                        DateTime dateBegin = DateTime.ParseExact(period.DateBegin, "yyyy-MM-ddTHH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
                        DateTime dateEnd = DateTime.ParseExact(period.DateEnd, "yyyy-MM-ddTHH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);

                        if (DateTime.Now < dateBegin || DateTime.Now > dateEnd)
                        {
                            fixed_periods.Remove(period);
                        }
                        if (fixed_periods.Count == 0)
                            break;
                    }
                    catch (Exception ex)
                    {

                    }
                }

                return fixed_periods;
            }

            return null;
        }

        /// <summary>
        /// Возвращает расписание заданной группы
        /// </summary>
        /// <param name="groupRefKey">ID группы</param>
        /// <param name="timetables">Записи из регистра "РасписаниеНаДату"(может быть пустым)</param>
        /// <returns></returns>
        public IEnumerable<Timetable> GetGroupTimetable(String groupRefKey, IEnumerable<Timetable> timetables = null)
        {
            if(timetables == null)
            {
                timetables = GetTimetable();
            }

            List<Timetable> groupTimetable = new List<Timetable>();
            foreach(Timetable timetable in timetables)
            {
                // выбираем только активные расписания
                if(timetable.GroupKey == groupRefKey && timetable.Active)
                {
                    groupTimetable.Add(timetable);
                }
            }
            return groupTimetable;
        }

        /// <summary>
        /// Возвращает список всех дисциплин
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Lesson> GetLessons()
        {
            var response = httpClient.GetAsync(baseUrl + CATALOG + "Дисциплины").Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;

                string responseString = @responseContent.ReadAsStringAsync().Result;

                JObject jsonResponse = JObject.Parse(responseString);
                var objResponse = jsonResponse["value"];

                var lessons = JsonConvert.DeserializeObject<IEnumerable<Lesson>>(objResponse.ToString());

                return lessons;
            }
            return null;
        }

        /// <summary>
        /// Возвращает список всех аудиторий
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Classroom> GetClassrooms()
        {
            var response = httpClient.GetAsync(baseUrl + CATALOG + "Аудитории").Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent = response.Content;

                string responseString = @responseContent.ReadAsStringAsync().Result;

                JObject jsonResponse = JObject.Parse(responseString);
                var objResponse = jsonResponse["value"];

                var lessons = JsonConvert.DeserializeObject<IEnumerable<Classroom>>(objResponse.ToString());

                return lessons;
            }
            return null;
        }

        #endregion

        public void Dispose()
        {
            Dispose(true);

            // Use SupressFinalize in case a subclass 
            // of this type implements a finalizer.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                httpClient.Dispose();
            }
        }
    } 
}
