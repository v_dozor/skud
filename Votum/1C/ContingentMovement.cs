﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Votum._1C
{
    /// <summary>
    /// Класс сущности "Движение контингента" в БД 1С
    /// (содержит историю переводов студентов в группы)
    /// </summary>
    [DataContract]
    public class ContingentMovement
    {
        /// <summary>
        /// Дата перевода в группу
        /// </summary>
        [DataMember(Name = "Period")]
        public String Period;

        /// <summary>
        /// "Номер строки" - по всей видимости, номер перевода по порядку
        /// </summary>
        [DataMember(Name = "LineNumber")]
        public int LineNumber;

        /// <summary>
        /// Идентификатор студента
        /// </summary>
        [DataMember(Name = "Студент_Key")]
        public String StudentKey;

        /// <summary>
        /// Идентификатор группы
        /// </summary>
        [DataMember(Name = "Группа_Key")]
        public String GroupKey;
    }
}
