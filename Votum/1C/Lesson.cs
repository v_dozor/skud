﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Votum._1C
{
    /// <summary>
    /// Класс сущности "Дисциплины" в БД 1С
    /// </summary>
    [DataContract]
    public class Lesson
    {
        /// <summary>
        /// Идентификатор сущности
        /// </summary>
        [DataMember(Name = "Ref_Key")]
        public String RefKey { get; set; }

        /// <summary>
        /// Название дисциплины
        /// </summary>
        [DataMember(Name = "Description")]
        public String Description { get; set; }
    }
}
