﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Votum._1C
{
    /// <summary>
    /// Класс сущности "Периоды обучения" в БД 1С
    /// </summary>
    [DataContract]
    public class PeriodOfStudy
    {
        /// <summary>
        /// Идентификатор сущности
        /// </summary>
        [DataMember(Name = "Ref_Key")]
        public String RefKey { get; set; }

        /// <summary>
        /// Название группы
        /// </summary>
        [DataMember(Name = "Description")]
        public String Description { get; set; }

        /// <summary>
        /// Дата начала периода
        /// </summary>
        [DataMember(Name = "ДатаНачалаПериода")]
        public String DateBegin { get; set; }

        // Дата конца периода
        [DataMember(Name = "ДатаОкончанияПериода")]
        public String DateEnd { get; set; }
    }
}
