﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Votum._1C
{
    /// <summary>
    /// Класс сущности "Специализация" в БД 1С
    /// </summary>
    [DataContract]
    public class Specialization
    {
        /// <summary>
        /// Идентификатор сущности
        /// </summary>
        [DataMember(Name = "Ref_Key")]
        public String RefKey { get; set; }
        
        /// <summary>
        /// Название специализации
        /// </summary>
        [DataMember(Name = "Description")]
        public String Description { get; set; }

        /// <summary>
        /// Идентификатор специальности, которой принадлежит специализация
        /// </summary>
        [DataMember(Name = "Owner_Key")]
        public String OwnerKey { get; set; }
    }
}
