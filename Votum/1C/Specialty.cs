﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Votum._1C
{
    /// <summary>
    /// Класс сущности "Специальность" в БД 1С
    /// </summary>
    [DataContract]
    public class Specialty
    {
        /// <summary>
        /// Идентификатор сущности
        /// </summary>
        [DataMember(Name = "Ref_Key")]
        public String RefKey { get;set; }

        /// <summary>
        /// Название специальности
        /// </summary>
        [DataMember(Name = "Description")]
        public String Description { get; set; }
    }
}
