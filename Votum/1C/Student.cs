﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Votum._1C
{
    /// <summary>
    /// Класс сущности "Студент" в БД 1С
    /// </summary>
    [DataContract]
    public class Student
    {
        /// <summary>
        /// Идентификатор сущности
        /// </summary>
        [DataMember(Name = "Ref_Key")]
        public String RefKey { get;set; }

        /// <summary>
        /// ФИО студента
        /// </summary>
        [DataMember(Name = "Description")]
        public String Description { get; set; }

        /// <summary>
        /// Идентификатор специальности
        /// </summary>
        [DataMember(Name = "Специальность_Key")]
        public String SpecialtyKey { get; set; }

        /// <summary>
        /// Идентификатор специализации
        /// </summary>
        [DataMember(Name = "Специализация_Key")]
        public String SpecializationKey { get; set; }

        /// <summary>
        /// Rfid студента
        /// </summary>
        [DataMember(Name = "RFID")]
        public String Rfid { get; set; }
    }
}
