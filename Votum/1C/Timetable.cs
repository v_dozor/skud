﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Votum._1C
{
    /// <summary>
    /// Класс сущности "Расписание" в БД 1С
    /// </summary>
    [DataContract]
    public class Timetable
    {
        /// <summary>
        /// ID группы
        /// </summary>
        [DataMember(Name = "УчебнаяГруппа_Key")]
        public String GroupKey;

        /// <summary>
        /// День недели
        /// </summary>
        [DataMember(Name = "ДеньНедели")]
        public String DayOfWeek;

        /// <summary>
        /// Вид недели(четная/нечетная)
        /// </summary>
        [DataMember(Name = "ВидНедели")]
        public String WeekType;

        /// <summary>
        /// Номер пары
        /// </summary>
        [DataMember(Name = "НомерПары")]
        public int LessonNumber;

        /// <summary>
        /// ID предмета
        /// </summary>
        [DataMember(Name = "Дисциплина")]
        public String LessonKey;

        /// <summary>
        /// ID аудитории
        /// </summary>
        [DataMember(Name = "Аудитория_Key")]
        public String ClassroomKey;

        /// <summary>
        /// Флаг, устанавливающий, является ли расписание активным
        /// </summary>
        [DataMember(Name = "Active")]
        public Boolean Active;

        /// <summary>
        /// Идентификатор периода обучения
        /// </summary>
        [DataMember(Name = "ПериодОбучения_Key")]
        public String PeriodOfStudyKey;
    }
}
