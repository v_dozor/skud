﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Votum.Types
{
    public interface IRfidLabel
    {
        byte[] Id { get; }
        string IdString { get; }
        string Separator { get; }
    }
}
