﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Votum.Types
{
    public interface IRfidTag : IEquatable<IRfidTag>, ICloneable
    {
        byte[] Id { get; }
        string ToString(string separetor = "-");
        int Length { get; }
    }
}
