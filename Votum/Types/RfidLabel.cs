﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Votum.Types
{
    public class RfidLabel
    {
        private byte[] id { get; set; }
        public byte[] Id
        {
            get
            {
                return id;
            }
        }

        private string idString;
        public string IdString {
            get
            {
                return idString;
            }
        }

        public int Lenght
        {
            get
            {
                return id.Length;
            }
        }

        private string separator = "-";
        public string Separator
        {
            get
            {
                return separator;
            }
        }

        /// <summary>
        /// Creates new rfid label
        /// </summary>
        /// <param name="id">Key identifier without any addtitonal data</param>
        /// <param name="separator">Separator of string rfid label</param>
        public RfidLabel(byte[] id, string separator = "-")
        {
            this.separator = separator;

                this.id = new byte[7];
                Array.Copy(id, 0, this.id, 1, 6);
                this.id[0] = 6;
            
            this.idString = RfidLabel.IdToString(this.id, this.separator);
        }

        public RfidLabel(string idString, string separator = "-")
        {
            this.separator = separator;
            this.idString = idString;
            this.id = RfidLabel.StringIdToBytes(idString, separator);
        }

        public static string IdToString(byte[] id, string separator)
        {
            if (id == null)
                return "";
            string str = String.Concat(Array.ConvertAll(id, x => x.ToString("X2") + "-"));
            if (str.Length > 1)
            {
                str = str.Remove(str.Length - 1, 1);
            }
            return str;
        }

        public static byte[] StringIdToBytes(string id, string separator)
        {
            if(id == null || id == "")
            {
                return null;
            }
            byte[] Id = id.Trim().Split(separator.ToCharArray()).
                Select(v => byte.Parse(v, NumberStyles.HexNumber)).ToArray<byte>();

            return Id;
        }

    }
}
