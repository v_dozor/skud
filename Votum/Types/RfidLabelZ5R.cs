﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Votum.Types
{
    public enum LabelTypeZ5R { bytes7 = 0, bytes6, SpecificZ5R };

    public class RfidLabelZ5R : IRfidLabel
    {
        private LabelTypeZ5R type = LabelTypeZ5R.bytes7;
        public LabelTypeZ5R Type
        {
            get
            {
                return type;
            }
        }

        private byte[] id;
        public byte[] Id
        {
            get
            {
                return id;
            }
        }
        public string IdString
        {
            get { return RfidLabel.IdToString(id, this.separator); }
        }

        private byte[] id6;
        public byte[] Id6
        {
            get { return id6; }
        }
        public string Id6String
        {
            get { return RfidLabel.IdToString(id6, this.separator); }
        }

        private byte[] idZ5R;
        public byte[] IdZ5R
        {
            get { return idZ5R; }
        }
        public string IdZ5RString
        {
            get { return RfidLabel.IdToString(idZ5R, this.separator); }
        }

        private string separator = "-";
        public string Separator
        {
            get
            {
                return separator;
            }
        }

        /// <summary>
        /// Creates new rfid label
        /// </summary>
        /// <param name="id">rfid id</param>
        /// <param name="clearId">If id contains only data from rfid-label (7 bytes) and nothing else,
        /// if at start of id you have specific for Z5RWEB "6" use false</param>
        /// <param name="separator">String separator</param>
        public RfidLabelZ5R(byte[] id, LabelTypeZ5R type = LabelTypeZ5R.bytes7, string separator = "-")
        {
            this.separator = separator;
            this.type = type;
            handleLabel(id);
        }

        /// <summary>
        /// Creates new rfid label
        /// </summary>
        /// <param name="id">rfid id</param>
        /// <param name="clearId">If id contains only data from rfid-label and nothing else,
        /// if at start of id you have specific for Z5RWEB "6" use false</param>
        /// <param name="separator">String separator</param>
        public RfidLabelZ5R(string idString, LabelTypeZ5R type = LabelTypeZ5R.bytes7, string separator = "-")
        {
            this.separator = separator;
            this.type = type;
            byte[] id = RfidLabel.StringIdToBytes(idString, separator);
            handleLabel(id);
        }

        private void handleLabel(byte[] id)
        {
            if (type == LabelTypeZ5R.bytes7)
            {
                if (id == null || id.Length < 7)
                    return;// throw new Exception("Некорректные данные при создании rfid-метки");

                this.id = new byte[7];
                Array.Copy(id, this.id, 7);

                this.id6 = new byte[6];
                Array.Copy(id, 1, id6, 0, 6);

                idZ5R = new byte[7];
                Array.Copy(id, idZ5R, 7);
                idZ5R[0] = 6;
            }
            else if (type == LabelTypeZ5R.bytes6)
            {
                Array.Copy(id, 1, this.id, 1, 6);
                this.id[0] = 6;
                if (id == null || id.Length < 6)
                    throw new Exception("Некорректные данные при создании rfid-метки");

                id6 = new byte[6];
                Array.Copy(id, id6, 6);

                idZ5R = new byte[7];
                Array.Copy(id, 0, idZ5R, 1, 6);
                idZ5R[0] = 6;
            }
            else if (type == LabelTypeZ5R.SpecificZ5R)
            {
                if (id == null || id.Length < 7)
                    throw new Exception("Некорректные данные при создании rfid-метки");

                id6 = new byte[6];
                Array.Copy(id, 1, id6, 0, 6);

                idZ5R = new byte[7];
                Array.Copy(id, idZ5R, 7);
            }
        }
    }
}
