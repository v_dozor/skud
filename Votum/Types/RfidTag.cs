﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Votum.Types
{
    /// <summary>
    /// Представляет RFID метку
    /// </summary>
    public class RfidTag : IRfidTag
    {
        protected byte[] id = null;

        public byte[] Id
        {
            get
            {
                return id;
            }
        }

        protected string idString = string.Empty;

        public int Length
        {
            get
            {
                return id != null ? id.Length : 0;
            }
        }

        public RfidTag(byte[] id, string separator = "-")
        {
            if (id == null || id.Count() == 0)
                throw new ArgumentException("Попытка создать RFID метку с пустыми параметрами");

            this.id = new byte[id.Length];

            //Debug.Print("Массив создался корректно {0}", this.id != null && this.id.Length == id.Length);
            
            Array.Copy(id, 0, this.id, 0, id.Length);
            
            idString = RfidTag.BytesToString(this.id, separator);
        }

        public RfidTag(string id, string separator = "-")
        {
            this.idString = id;
            this.id = RfidTag.StringToBytes(id, separator);
        }

        public string ToString(string separator = "-")
        {
            if (separator == "-")
                return idString;
            return BytesToString(this.id, separator);
        }

        public static string BytesToString(byte[] id, string separator = "-")
        {
            if (id == null)
                return "";
            string str = String.Concat(Array.ConvertAll(id, x => x.ToString("X2") + "-"));
            if (str.Length > 1)
            {
                str = str.Remove(str.Length - 1, 1);
            }
            return str;
        }

        public static byte[] StringToBytes(string id, string separator = "-")
        {
            if (id == null || id == "")
            {
                return null;
            }
            byte[] Id = id.Trim().Split(separator.ToCharArray()).
                Select(v => byte.Parse(v, NumberStyles.HexNumber)).ToArray<byte>();

            return Id;
        }

        public bool Equals(IRfidTag other)
        {
            if(this.Id.SequenceEqual(other.Id))
            {
                return true;
            }
            return false;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public static bool operator == (RfidTag left, RfidTag right)
        {
            if (ReferenceEquals(left, null) && ReferenceEquals(right, null))
                return true;
            if (ReferenceEquals(left, null) || ReferenceEquals(right, null))
                return false;
            return left.Equals(right);
        }

        public static bool operator !=(RfidTag left, RfidTag right)
        {
            if (ReferenceEquals(left, null) && ReferenceEquals(right, null))
                return false;
            if (ReferenceEquals(left, null) || ReferenceEquals(right, null))
                return true;
            return !left.Equals(right);
        }
    }
}
