﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;

namespace VotumInfo
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static Mutex m_instance;
        private const string m_appName = "VotumInfoApplicationMutex";

        protected override void OnStartup(StartupEventArgs e)
        {
            bool tryCreateNewApp;
            m_instance = new Mutex(true, m_appName, out tryCreateNewApp);
            if (tryCreateNewApp)
            {
                base.OnStartup(e);
                DevExpress.Xpf.Core.ApplicationThemeHelper.UpdateApplicationThemeName();
            }
            else
            {
                MessageBox.Show("Попытка запустить приложение дважды, приложение закрывается ... ");
                Application.Current.Shutdown();
            }
        }
        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            DevExpress.Xpf.Core.ApplicationThemeHelper.SaveApplicationThemeName();
        }

        private void OnAppStartup_UpdateThemeName(object sender, StartupEventArgs e)
        {
            Dispatcher.UnhandledException += OnDispatcherUnhandledException;
            DevExpress.Xpf.Core.ApplicationThemeHelper.UpdateApplicationThemeName();
            MainViewModel mvm = new MainViewModel();
        }

        void OnDispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            DateTime eventTime = DateTime.Now;
            File.AppendAllText("errors.txt", "\r\n" + eventTime.ToString() + " " + e.Exception.Message + " " + e.Exception.StackTrace + "\r\n");
            e.Handled = true;
        }
    }
}
