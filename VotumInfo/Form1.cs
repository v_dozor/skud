﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VotumInfo
{
    public partial class Splash : Form
    {
        public Splash()
        {
            InitializeComponent();
            this.Refresh();
            label1.Refresh();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            this.TopMost = true;
            this.BringToFront();
        }
    }
}
