﻿using DevExpress.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VotumInfo
{
    public class LessonModel : ViewModelBase
    {
        /// <summary>
        /// Название пары
        /// </summary>
        public String LessonName { get; set; }

        /// <summary>
        /// Название аудитории
        /// </summary>
        public String Classroom { get; set; }

        /// <summary>
        /// Номер пары
        /// </summary>
        public int LessonNumber { get; set; }

        /// <summary>
        /// Время начала пары
        /// </summary>
        public String LessonBeginTime { get; set; }

        /// <summary>
        /// Время конца пары
        /// </summary>
        public String LessonEndTime { get; set; }

        /// <summary>
        /// Время пары
        /// </summary>
        public String LessonTime { get; set; }
    }
}
