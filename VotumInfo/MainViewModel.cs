﻿using DevExpress.Mvvm;
using DozorUsbLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Votum._1C;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Votum.Types;

namespace VotumInfo
{
    /// <summary>
    /// Тип недели(четная/нечетная)
    /// </summary>
    public enum WeekType
    {
        Even,   // четная
        Odd     // нечетная
    }

    public class MainViewModel : ViewModelBase
    {
        private MainWindow mainWindow;
        private Reader1Window reader1Window;
        private Reader2Window reader2Window;
        private Reader3Window reader3Window;
        private Reader4Window reader4Window;

        #region Переменные 1С

        private Connector_1C connector;
        private IEnumerable<Student> students;
        private IEnumerable<Lesson> lessons;
        private IEnumerable<Group> groups;
        private IEnumerable<ContingentMovement> contingentMovements;
        private IEnumerable<Timetable> timetables;
        private IEnumerable<Classroom> classrooms;

        #endregion

        #region Настройки

        private WeekType currentWeekType;        
        private Settings settings;
        private List<Tuple<string, string>> lessonsTime;
        public int screenWidth;
        public int screenHeight;

        #endregion

        #region Дни

        private ObservableCollection<ObservableCollection<LessonModel>> readersDayLessons;
        public ObservableCollection<ObservableCollection<LessonModel>> ReadersDayLessons
        {
            get { return readersDayLessons; }
            set
            {
                if (readersDayLessons != value)
                {
                    readersDayLessons = value;
                    RaisePropertyChanged("ReadersDayLessons");
                }
            }
        }

        private ObservableCollection<LessonModel> selectedReadersLessons;
        public ObservableCollection<LessonModel> SelectedReadersLessons
        {
            get { return selectedReadersLessons; }
            set
            {
                if (selectedReadersLessons != value)
                {
                    selectedReadersLessons = value;
                    RaisePropertyChanged("SelectedReadersLessons");
                }
            }
        }

        #endregion

        #region Недели

        private ObservableCollection<WeekTimetable> readersWeeks;
        public ObservableCollection<WeekTimetable> ReadersWeeks
        {
            get { return readersWeeks; }
            set
            {
                if(readersWeeks != value)
                {
                    readersWeeks = value;
                    RaisePropertyChanged("ReadersWeeks");
                }
            }
        }

        #endregion

        #region Переключатели день/неделя

        private ObservableCollection<bool> isReadersWeek;
        public ObservableCollection<bool> IsReadersWeek
        {
            get { return isReadersWeek; }
            set
            {
                if (isReadersWeek != value)
                {
                    isReadersWeek = value;
                    RaisePropertyChanged("IsReadersWeek");
                }
            }
        }       

        #endregion

        #region Параметры считывателей

        private RfidReader rfidReader;
        private long[] readersLastEvents;

        private String[] readersLastRfid;

        private bool isInit;
        public bool IsInit
        {
            get { return isInit; }
            set
            {
                if(isInit != value)
                {
                    isInit = value;
                    RaisePropertyChanged("IsInit");
                }
            }
        }

        // словарь считывателей(ключ - путь к устройству, значение - номер считывателя(0-3)
        private Dictionary<string, int> readers;

        #endregion

        #region Состояние экрана

        private WindowState currentWindowState;
        public WindowState CurrentWindowState
        {
            get { return currentWindowState; }
            set
            {
                if (currentWindowState != value)
                {
                    currentWindowState = value;
                    RaisePropertyChanged("CurrentWindowState");
                }
            }
        }

        private ObservableCollection<bool> isReaderScreenVisible;
        public ObservableCollection<bool> IsReaderScreenVisible
        {
            get { return isReaderScreenVisible; }
            set
            {
                if (isReaderScreenVisible != value)
                {
                    isReaderScreenVisible = value;
                    RaisePropertyChanged("IsReaderScreenVisible");
                }
            }
        }

        private ObservableCollection<string> readersInitializationState;
        public ObservableCollection<string> ReadersInitializationState
        {
            get { return readersInitializationState; }
            set
            {
                if (readersInitializationState != value)
                {
                    readersInitializationState = value;
                    RaisePropertyChanged("ReadersInitializationState");
                }
            }
        }

        #endregion

        // флаг, указывающий на необходимость обновить данные 1С
        private bool needToUpdate;

        // текущие имена файлов с логами программы
        private string logFilename;                 // log_<date>
        private string mainThreadErrorsLogFilename; // errors_main_thread_<date>
        private string controlWindowsStateThreadErrorsLogFilename;  // errors_second_thread_<date>

        Splash splash = null;

        public MainViewModel()
        {
            try
            {
                logFilename = "";
                mainThreadErrorsLogFilename = "";
                controlWindowsStateThreadErrorsLogFilename = "";

                // загружаем настройки или создаем файл по умолчанию
                settings = Settings.Load() ?? new Settings();
                settings.Save();

                screenWidth = settings.screenWidth == "" ?
                    System.Windows.Forms.SystemInformation.VirtualScreen.Width :
                    Convert.ToInt32(settings.screenWidth);

                screenHeight = settings.screenHeight == "" ?
                    System.Windows.Forms.SystemInformation.VirtualScreen.Height : 
                    Convert.ToInt32(settings.screenHeight);

                settings.updateDate = DateTime.Now.ToShortDateString();
                settings.Save();

                needToUpdate = false;

                // загружаем пути к считывателям
                readers = new Dictionary<string, int>();

                string reader1PortName = settings.reader1PortName;
                string reader2PortName = settings.reader2PortName;
                string reader3PortName = settings.reader3PortName;
                string reader4PortName = settings.reader4PortName;

                if (reader1PortName != "" &&
                    reader2PortName != "" &&
                    reader3PortName != "" &&
                    reader4PortName != "")
                {
                    IsInit = true;
                    readers.Add(reader1PortName, 0);
                    readers.Add(reader2PortName, 1);
                    readers.Add(reader3PortName, 2);
                    readers.Add(reader4PortName, 3);
                }

                if (IsInit)
                    CurrentWindowState = WindowState.Minimized;
                else
                    CurrentWindowState = WindowState.Maximized;

                //отображает окна с расписаниями
                Show();

                IsReaderScreenVisible = new ObservableCollection<bool> { false, false, false, false };
                ReadersInitializationState = new ObservableCollection<string> { IsInit ? "" : VotumInfo.Properties.Resources.ReaderInitWait, "", "", "" };

                IsReadersWeek = new ObservableCollection<bool> { false, false, false, false };
                ReadersDayLessons = new ObservableCollection<ObservableCollection<LessonModel>> { new ObservableCollection<LessonModel>(),
                                                                                              new ObservableCollection<LessonModel>(),
                                                                                              new ObservableCollection<LessonModel>(),
                                                                                              new ObservableCollection<LessonModel>() };
                ReadersWeeks = new ObservableCollection<WeekTimetable> { new WeekTimetable(),
                                                                     new WeekTimetable(),
                                                                     new WeekTimetable(),
                                                                     new WeekTimetable() };

                SelectedReadersLessons = new ObservableCollection<LessonModel> { new LessonModel(),
                                                                             new LessonModel(), 
                                                                             new LessonModel(), 
                                                                             new LessonModel() };

                bool timetableOk = false;

                try
                {
                    // загружаем параметры соединения с 1С
                    String url = settings.Url;
                    String username = settings.Username;
                    String password = settings.Password;

                    // загружаем расписание звонков
                    lessonsTime = new List<Tuple<string, string>>();
                    lessonsTime.Add(new Tuple<string, string>(settings.lesson1Begin, settings.lesson1End));
                    lessonsTime.Add(new Tuple<string, string>(settings.lesson2Begin, settings.lesson2End));
                    lessonsTime.Add(new Tuple<string, string>(settings.lesson3Begin, settings.lesson3End));
                    lessonsTime.Add(new Tuple<string, string>(settings.lesson4Begin, settings.lesson4End));

                    if (settings.showWindows == true)
                    {
                        splash = new Splash();
                        splash.Show();
                    }

                    // загружаем данные из 1С
                    connector = new Connector_1C(url, username, password);
                    timetableOk = Get1CData();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка при запросе к серверу 1С: " + ex.Message,
                                    "Ошибка 1С", MessageBoxButton.OK, MessageBoxImage.Error);
                    MessageBox.Show("Не удалось получить расписание от сервера 1С. Проверьте соединение с сервером и убедитесь, что расписание внесено корректно.",
                                    "Ошибка 1С", MessageBoxButton.OK, MessageBoxImage.Information);
                    DateTime eventTime = DateTime.Now;
                    File.AppendAllText(mainThreadErrorsLogFilename, "\r\n" + eventTime.ToString() + " " + ex.Message + " " + ex.StackTrace + "\r\n");
                    Application.Current.Shutdown();
                    return;
                }

                if(!timetableOk)
                {
                    MessageBox.Show("Не удалось получить расписание от сервера 1С. Проверьте соединение с сервером и убедитесь, что расписание внесено корректно.",
                                    "Ошибка 1С", MessageBoxButton.OK, MessageBoxImage.Information);
                    Application.Current.Shutdown();
                    return;
                }

                // Инициализация RFID считывателей
                rfidReader = new RfidReader();
                rfidReader.DiveceAttached += RfidReader_DiveceAttached;
                long curMs = Stopwatch.GetTimestamp() / Stopwatch.Frequency * 1000;
                readersLastEvents = new long[4] { curMs,
                                                  curMs,
                                                  curMs,
                                                  curMs};
                readersLastRfid = new String[4] { "", "", "", "" };
                rfidReader.Rfid_Updated += new EventHandler<RfidReaderEventArgs>(RfidReceived);                
            }
            catch(Exception ex)
            {
                try
                {
                    DateTime eventTime = DateTime.Now;
                    File.AppendAllText(mainThreadErrorsLogFilename, "\r\n" + eventTime.ToString() + " " + ex.Message + " " + ex.StackTrace + "\r\n");
                }
                catch(Exception ex1)
                {

                }                
            }
            if(splash != null)
            {
                if(settings.showWindows == true)
                {
                    splash.Close();
                }
                splash.Dispose();
            }

            ControlWindowState();
        }

        private object diveceAttachedLocker = new object();
        private void RfidReader_DiveceAttached(object sender, EventArgs e)
        {
            lock(diveceAttachedLocker)
            {
                Thread.Sleep(500);
                rfidReader.SearchDevices();
            }

        }

        /// <summary>
        /// Асинхронный метод для сворачивания окон программы при отсутствии активности
        /// </summary>
        private async void ControlWindowState()
        {
            var result = await Task<string>.Factory.StartNew(() =>
            {
                while(true)
                {
                    try
                    {
                        UpdateLogFiles();
                        Thread.Sleep(1000);
                        long curMs = Stopwatch.GetTimestamp() / Stopwatch.Frequency * 1000;
                        for (int i = 0; i < 4; i++)
                        {
                            if (curMs - 15000 > readersLastEvents[i])
                            {
                                lock (IsReaderScreenVisible)
                                {
                                    IsReaderScreenVisible[i] = false;
                                }                                
                                lock(IsReadersWeek)
                                {
                                    IsReadersWeek[i] = false;
                                }                                
                            }                 
                        }

                        // немного магии
                        Random rnd = new Random();
                        if(rnd.Next(1, 1000) % 13 == 0)
                        {
                            // суточное обновление расписания
                            string updateDate = "";
                            lock(settings)
                            {
                                updateDate = settings.updateDate;
                            }                            

                            if (updateDate != DateTime.Now.ToShortDateString())
                            {
                                needToUpdate = true;
                            }
                        }                        
                    }
                    catch(Exception ex)
                    {
                        try
                        {
                            DateTime eventTime = DateTime.Now;
                            File.AppendAllText(controlWindowsStateThreadErrorsLogFilename, "\r\n" + eventTime.ToString() + " " + ex.Message + " " + ex.StackTrace + "\r\n");
                        }
                        catch (Exception ex1)
                        {

                        }   
                    }
                }
            });
        }

        /// <summary>
        /// Берет все необходимые данные от 1С
        /// </summary>
        private bool Get1CData()
        {
            try
            {
                File.AppendAllText("log.txt", "Обновление расписания 1С " + DateTime.Now.ToShortDateString() + "\r\n");
            }
            catch (Exception ex1)
            {

            }

            IEnumerable<Student> newStudents = null;
            IEnumerable<Lesson> newLessons = null;
            IEnumerable<Group> newGroups = null;
            IEnumerable<ContingentMovement> newContingentMovements = null;
            IEnumerable<Timetable> newTimetables = null;
            IEnumerable<Classroom> newClassrooms = null;

            if (settings.demo == true)
            {
                string demoFilesPath = AppDomain.CurrentDomain.BaseDirectory + @"demo\";

                newStudents = JsonConvert.DeserializeObject<IEnumerable<Student>>(
                    JObject.Parse(
                        File.ReadAllText(demoFilesPath + "students.json"))["value"].ToString()
                );

                newLessons = JsonConvert.DeserializeObject<IEnumerable<Lesson>>(
                    JObject.Parse(
                        File.ReadAllText(demoFilesPath + "lessons.json"))["value"].ToString()
                );

                newGroups = JsonConvert.DeserializeObject<IEnumerable<Group>>(
                    JObject.Parse(
                        File.ReadAllText(demoFilesPath + "groups.json"))["value"].ToString()
                );

                /* ------------ создание таблиц расписания  ------------ */

                JArray values = (JArray)JObject.Parse(
                        File.ReadAllText(demoFilesPath + "contingent_movements.json"))["value"];

                ContingentMovement[] contingentMovements = new ContingentMovement[0];
                foreach (JObject obj in values)
                {
                    var recordSets = obj["RecordSet"];
                    var newContingentMovementsDemo = JsonConvert.DeserializeObject<ContingentMovement[]>(recordSets.ToString());

                    var tmp = contingentMovements;
                    contingentMovements = new ContingentMovement[tmp.Count() + newContingentMovementsDemo.Count()];
                    tmp.CopyTo(contingentMovements, 0);
                    newContingentMovementsDemo.CopyTo(contingentMovements, tmp.Count());
                }
                newContingentMovements = contingentMovements;

                /* ------------ создание таблиц расписания  ------------ */
                values = (JArray)JObject.Parse(
                    File.ReadAllText(demoFilesPath + "timetable.json"))["value"];

                Timetable[] timetables = new Timetable[0];
                foreach (JObject obj in values)
                {
                    var recordSets = obj["RecordSet"];
                    var newTimetablesDemo = JsonConvert.DeserializeObject<Timetable[]>(recordSets.ToString());

                    var tmp = timetables;
                    timetables = new Timetable[tmp.Count() + newTimetablesDemo.Count()];
                    tmp.CopyTo(timetables, 0);
                    newTimetablesDemo.CopyTo(timetables, tmp.Count());
                }
                newTimetables = timetables;


                newClassrooms = JsonConvert.DeserializeObject<IEnumerable<Classroom>>(
                    JObject.Parse(
                        File.ReadAllText(demoFilesPath + "classrooms.json"))["value"].ToString()
                );
            }
            else
            {
                newStudents = connector.GetStudents();
                newLessons = connector.GetLessons();
                newGroups = connector.GetGroups();
                newContingentMovements = connector.GetContingentMovements();
                newTimetables = connector.GetTimetable();
                newClassrooms = connector.GetClassrooms();
            }

            students = newStudents;
            lessons = newLessons;
            groups = newGroups;
            contingentMovements = newContingentMovements;
            timetables = newTimetables;
            classrooms = newClassrooms;

            return true;
        }

        public void Show()
        {
            if (settings.showWindows == false)
                return;
            try
            {
                Debug.Print("Создание окна считывателя");
                mainWindow = new MainWindow() { DataContext = this };
                mainWindow.Show();

                reader1Window = new Reader1Window() { DataContext = this };
                reader1Window.Show();

                reader2Window = new Reader2Window() { DataContext = this };
                reader2Window.Show();

                reader3Window = new Reader3Window() { DataContext = this };
                reader3Window.Show();

                reader4Window = new Reader4Window() { DataContext = this };
                reader4Window.Show();

                //reader1Window.InvalidateVisual();
                //reader2Window.InvalidateVisual();
                //reader3Window.InvalidateVisual();
                //reader4Window.InvalidateVisual();

            }
            catch (Exception ex)
            {
                try
                {
                    DateTime eventTime = DateTime.Now;
                    File.AppendAllText(mainThreadErrorsLogFilename, "\r\n" + eventTime.ToString() + " " + ex.Message + " " + ex.StackTrace + "\r\n");
                }
                catch (Exception ex1)
                {

                }   
            }            
        }

        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            rfidReader.CloseDevices();
            rfidReader.Rfid_Updated -= new EventHandler<RfidReaderEventArgs>(RfidReceived);
            if (reader1Window != null)
                reader1Window.Close();
            if (reader2Window != null)
                reader2Window.Close();
            if (reader3Window != null)
                reader3Window.Close();
            if (reader4Window != null)
                reader4Window.Close();
        }

        private void RfidReceived(object sender, RfidReaderEventArgs args)
        {
            try
            {
                // суточное обновление расписания
                if (needToUpdate)
                {
                    if(Get1CData())
                    {
                        settings.updateDate = DateTime.Now.ToShortDateString();
                        needToUpdate = false;
                    }
                    else
                    {
                        try
                        {
                            needToUpdate = false;
                            DateTime eventTime = DateTime.Now;
                            File.AppendAllText(logFilename, eventTime.ToString() + " Не удалось обновить расписание\r\n");
                        }
                        catch (Exception ex1)
                        {

                        }
                    }
                }

                // Получаем номер считывателя и путь к устройству
                int receiverId = args.ReceiverId;
                string portName = args.ReceiverId.ToString() + args.SerialId.ToString();

                //DateTime eventTime = DateTime.Now;
                //File.AppendAllText(logFilename, eventTime.ToString() + " " + portName + " " + args.Rfid + "\r\n");

                // инициализация считывателей при первом запуске
                if (!IsInit)
                {
                    long curMs = Stopwatch.GetTimestamp() / Stopwatch.Frequency * 1000;

                    // тайм-аут
                    for (int i = 0; i < 4; i++)
                    {                      
                        if (curMs - readersLastEvents[i] < 1000)
                        {
                            return;
                        }
                    }

                    // инициализируем текущий считыватель
                    int currentReader = readers.Count;
                    if (readers.ContainsKey(portName))
                        return;
                    readers.Add(portName, currentReader);
                    if (currentReader == 0)
                        settings.reader1PortName = portName;
                    else if (currentReader == 1)
                        settings.reader2PortName = portName;
                    else if (currentReader == 2)
                        settings.reader3PortName = portName;
                    else if (currentReader == 3)
                        settings.reader4PortName = portName;

                    lock (settings)
                    {
                        settings.Save();
                    }

                    readersLastEvents[currentReader] = curMs;
                    ReadersInitializationState[currentReader] = VotumInfo.Properties.Resources.ReaderInitOk;

                    if (currentReader < 3)
                        ReadersInitializationState[currentReader + 1] = VotumInfo.Properties.Resources.ReaderInitWait;
                    if (currentReader == 3)
                    {
                        // все считыватели инициализированы
                        IsInit = true;
                        CurrentWindowState = WindowState.Minimized;
                    }
                }
                else
                {
                    // если таймаут по считыванию истек, обрабатываем событие
                    long curMs = Stopwatch.GetTimestamp() / Stopwatch.Frequency * 1000;

                    // если пакет пришел от неизвестного устройства
                    if (!readers.ContainsKey(portName))
                        return;

                    int readerNumber = readers[portName];

                    // таймаут - секунда
                    if (curMs - readersLastEvents[readerNumber] > 1000)
                    {
                        readersLastEvents[readerNumber] = curMs;

                        // получаем ID студента по его метке
                        String rfid = args.Rfid;

                        // получаем расписание на неделю и день
                        WeekTimetable weekTimetable = GetStudentWeekTimetable(rfid);
                        if (weekTimetable != null)
                        {
                            IsReaderScreenVisible[readerNumber] = true;                            
                            SetCurrentReaderTimetable(readerNumber, weekTimetable);
                            UpdateCurrentLessonSelection();
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                try
                {
                    DateTime eventTime = DateTime.Now;
                    File.AppendAllText(mainThreadErrorsLogFilename, "\r\n" + eventTime.ToString() + " " + ex.Message + " " + ex.StackTrace + "\r\n");
                }
                catch (Exception ex1)
                {

                }   
            }                  
        }

        /// <summary>
        /// Возвращает расписание группы на неделю по RFID студента
        /// </summary>
        /// <param name="rfid">RFID студента, который поднес метку</param>
        /// <returns></returns>
        private WeekTimetable GetStudentWeekTimetable(String rfid)
        {
            String studentRefKey = "";
            if (students == null)
                return null;

            var student = students.FirstOrDefault(x => { return x.Rfid == rfid; });

            //если включен режим демо расписания, то для любой катры получаю студента
            if(settings.demo == true && settings.demoSchedule == true && student == null)
            {
                RfidTag tag = new RfidTag(rfid);

                Random rnd = new Random(21121991 + tag.Id[tag.Id.Length - 3] + tag.Id[tag.Id.Length - 2]);
                var next = rnd.Next(0, students.Count() - 1);
                student = students.ElementAt(next);
            }

            if (student == null)
                return null;

            studentRefKey = student.RefKey;
            if (studentRefKey == "")
                return null;

            // получаем группу обучения студента
            Group currentGroup = connector.GetGroupByStudent(studentRefKey, contingentMovements, groups);

            // получаем расписание группы
            List<Timetable> groupTimetable = connector.GetGroupTimetable(currentGroup.RefKey,
                                                               timetables).ToList();

            // оставляем только записи по текущему типу недели
            /*foreach (Timetable timetable in groupTimetable)
            {
                if ((timetable.WeekType == "Четная" && currentWeekType != WeekType.Even) ||
                   (timetable.WeekType == "Нечетная" && currentWeekType != WeekType.Odd))
                {
                    groupTimetable.Remove(timetable);
                }
            }*/

            if (currentGroup == null || groupTimetable == null)
                return null;

            // формируем расписание
            WeekTimetable weekTimetable = new WeekTimetable();
            foreach (Timetable timetable in groupTimetable)
            {
                if (timetable == null)
                    continue;
                // формируем занятие
                LessonModel lessonModel = new LessonModel();

                // устанавливаем время пары
                int lessonNumber = timetable.LessonNumber;
                lessonModel.LessonNumber = lessonNumber;
                if(lessonsTime.Count < lessonNumber)
                    continue;

                if (lessonNumber == 0)
                    lessonNumber++;
                lessonModel.LessonBeginTime = lessonsTime.ElementAt(lessonNumber - 1).Item1;
                lessonModel.LessonEndTime = lessonsTime.ElementAt(lessonNumber - 1).Item2;
                lessonModel.LessonTime = lessonModel.LessonBeginTime + " - " + lessonModel.LessonEndTime;

                // получаем название занятия
                foreach (Lesson lesson in lessons)
                {
                    if (lesson.RefKey == timetable.LessonKey)
                    {
                        lessonModel.LessonName = lesson.Description;
                        break;
                    }
                }

                // получаем аудиторию
                foreach (Classroom classroom in classrooms)
                {
                    if (timetable.ClassroomKey == classroom.RefKey)
                    {
                        lessonModel.Classroom = classroom.Description;
                        break;
                    }
                }

                // добавляем в расписание на неделю
                DayOfWeek dayOfWeek = DayOfWeek.Sunday;
                if (timetable.DayOfWeek == "Понедельник")
                    dayOfWeek = DayOfWeek.Monday;
                else if (timetable.DayOfWeek == "Вторник")
                    dayOfWeek = DayOfWeek.Tuesday;
                else if (timetable.DayOfWeek == "Среда")
                    dayOfWeek = DayOfWeek.Wednesday;
                else if (timetable.DayOfWeek == "Четверг")
                    dayOfWeek = DayOfWeek.Thursday;
                else if (timetable.DayOfWeek == "Пятница")
                    dayOfWeek = DayOfWeek.Friday;
                else if (timetable.DayOfWeek == "Суббота")
                    dayOfWeek = DayOfWeek.Saturday;

                if (dayOfWeek != DayOfWeek.Sunday)
                    weekTimetable.SetLesson(dayOfWeek, lessonModel.LessonNumber, lessonModel);
                
            }

            return weekTimetable;
        }

        /// <summary>
        /// Устанавливает расписание на текущий день и неделю
        /// </summary>
        /// <param name="weekTimetable"></param>
        private void SetCurrentReaderTimetable(int readerNumber, WeekTimetable weekTimetable)
        {
            if (weekTimetable == null)
                return;
            DateTime now = DateTime.Now;
            if (now.DayOfWeek == DayOfWeek.Sunday)
                return;
            ReadersDayLessons[readerNumber] = new ObservableCollection<LessonModel>(weekTimetable.GetLessonsByDayOfWeek(now.DayOfWeek));
            ReadersWeeks[readerNumber] = weekTimetable;
            IsReadersWeek[readerNumber] = !IsReadersWeek[readerNumber];
        }

        /// <summary>
        /// Обновляет текущее занятие на экране
        /// </summary>
        private void UpdateCurrentLessonSelection()
        {
            TimeSpan now = DateTime.Now.TimeOfDay;
            int currentLesson = -1;

            //now = new TimeSpan(10, 50, 30);

            // определяем номер текущего занятия по времени
            for (int i = 0; i < lessonsTime.Count; i++)
            {
                if (i == 0)
                {
                    if (now >= TimeSpan.Parse(lessonsTime.ElementAt(i).Item1) && now <= TimeSpan.Parse(lessonsTime.ElementAt(i).Item2))
                    {
                        currentLesson = i;
                    }
                }
                else
                {
                    if (now > TimeSpan.Parse(lessonsTime.ElementAt(i - 1).Item2) && now <= TimeSpan.Parse(lessonsTime.ElementAt(i).Item2))
                    {
                        currentLesson = i;
                    }
                }
            }

            for(int i = 0; i < 4; i++)
            {
                if(ReadersDayLessons[i].Count > currentLesson)
                {
                    if (currentLesson == -1)
                        SelectedReadersLessons[i] = null;
                    else
                        SelectedReadersLessons[i] = ReadersDayLessons[i].ElementAt(currentLesson);
                }
            }
        }

        private DelegateCommand updateTimetableCommand;

        /// <summary>
        /// Команда обновления данных 1С
        /// </summary>
        public ICommand UpdateTimetableCommand
        {
            get
            {
                if (updateTimetableCommand == null)
                {
                    updateTimetableCommand = new DelegateCommand(UpdateTimetable);
                }
                return updateTimetableCommand;
            }
        }

        private void UpdateTimetable()
        {
            try
            {
                Get1CData();
            }
            catch(Exception ex)
            {
                try
                {
                    DateTime eventTime = DateTime.Now;
                    File.AppendAllText(mainThreadErrorsLogFilename, "\r\n" + eventTime.ToString() + " " + ex.Message + " " + ex.StackTrace + "\r\n");
                }
                catch (Exception ex1)
                {

                }   
            }
        }

        private DelegateCommand exitCommand;

        /// <summary>
        /// Команда выхода
        /// </summary>
        public ICommand ExitCommand
        {
            get
            {
                if (exitCommand == null)
                {
                    exitCommand = new DelegateCommand(Exit);
                }
                return exitCommand;
            }
        }

        private void Exit()
        {
            Application.Current.Shutdown();
            mainWindow.Close();
        }

        /// <summary>
        /// Обновляет файлы с логами(создает новые/удаляет старые при необходимости)
        /// </summary>
        private void UpdateLogFiles()
        {
            string today = DateTime.Now.ToShortDateString();
            string yesterday = DateTime.Now.AddDays(-1).ToShortDateString();
            string dayBeforeYesterday = DateTime.Now.AddDays(-2).ToShortDateString();

            try
            {
                bool sendEmail = false;
                 // создаем новые файлы при необходимости
                if (logFilename != settings.logFilenamePrefix + "log_" + today + ".txt")
                {
                    lock (logFilename)
                    {
                        var tmp = settings.logFilenamePrefix + "log_" + today + ".txt";
                        if (!File.Exists(tmp))
                        {
                            File.Create(tmp);                            
                            sendEmail = true;
                        }
                        logFilename = tmp;
                    }                                                      
                }

                if (mainThreadErrorsLogFilename != settings.logFilenamePrefix + "errors_main_thread_" + today + ".txt")
                {
                    lock (mainThreadErrorsLogFilename)
                    {
                        var tmp = settings.logFilenamePrefix + "errors_main_thread_" + today + ".txt";
                        if (!File.Exists(tmp))
                        {
                            File.Create(tmp);                            
                            sendEmail = true;
                        }
                        mainThreadErrorsLogFilename = tmp;
                    }  
                                                     
                }

                if (controlWindowsStateThreadErrorsLogFilename != settings.logFilenamePrefix + "errors_second_thread_" + today + ".txt")
                {
                    lock (controlWindowsStateThreadErrorsLogFilename)
                    {
                        var tmp = settings.logFilenamePrefix + "errors_second_thread_" + today + ".txt";
                        if (!File.Exists(tmp))
                        {
                            File.Create(tmp);                            
                            sendEmail = true;
                        }
                        controlWindowsStateThreadErrorsLogFilename = tmp;
                    }                                                     
                }

                if(sendEmail)
                {
                    MailMessage mail = new MailMessage("votum.info.logs@gmail.com", "schedule@satcon.ru");
                    var client = new SmtpClient("smtp.gmail.com", 587)
                    {
                        Credentials = new NetworkCredential("votum.info.logs@gmail.com", "votumforever"),
                        EnableSsl = true
                    };
                    client.Port = 25;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    mail.Subject = "VInfo Logs " + DateTime.Now.ToShortDateString();
                    mail.Body = "Созданы новые файлы с логами. К письму будут прикреплены логи за вчерашний день(при их наличии)";

                    // добавляем файлы логов
                    /*if(File.Exists("log_" + yesterday + ".txt"))
                    {
                        string attachmentFilename = "log_" + yesterday + ".txt";
                        Attachment attachment = new Attachment(attachmentFilename, MediaTypeNames.Application.Octet);
                        ContentDisposition disposition = attachment.ContentDisposition;
                        disposition.CreationDate = File.GetCreationTime(attachmentFilename);
                        disposition.ModificationDate = File.GetLastWriteTime(attachmentFilename);
                        disposition.ReadDate = File.GetLastAccessTime(attachmentFilename);
                        disposition.FileName = Path.GetFileName(attachmentFilename);
                        disposition.Size = new FileInfo(attachmentFilename).Length;
                        disposition.DispositionType = DispositionTypeNames.Attachment;
                        mail.Attachments.Add(attachment);        
                    }*/
                    if(File.Exists("errors_main_thread_" + yesterday + ".txt"))
                    {
                        string attachmentFilename = "errors_main_thread_" + yesterday + ".txt";
                        Attachment attachment = new Attachment(attachmentFilename, MediaTypeNames.Application.Octet);
                        ContentDisposition disposition = attachment.ContentDisposition;
                        disposition.CreationDate = File.GetCreationTime(attachmentFilename);
                        disposition.ModificationDate = File.GetLastWriteTime(attachmentFilename);
                        disposition.ReadDate = File.GetLastAccessTime(attachmentFilename);
                        disposition.FileName = Path.GetFileName(attachmentFilename);
                        disposition.Size = new FileInfo(attachmentFilename).Length;
                        disposition.DispositionType = DispositionTypeNames.Attachment;
                        mail.Attachments.Add(attachment);
                    }
                    if(File.Exists("errors_second_thread_" + yesterday + ".txt"))
                    {
                        string attachmentFilename = "errors_second_thread_" + yesterday + ".txt";
                        Attachment attachment = new Attachment(attachmentFilename, MediaTypeNames.Application.Octet);
                        ContentDisposition disposition = attachment.ContentDisposition;
                        disposition.CreationDate = File.GetCreationTime(attachmentFilename);
                        disposition.ModificationDate = File.GetLastWriteTime(attachmentFilename);
                        disposition.ReadDate = File.GetLastAccessTime(attachmentFilename);
                        disposition.FileName = Path.GetFileName(attachmentFilename);
                        disposition.Size = new FileInfo(attachmentFilename).Length;
                        disposition.DispositionType = DispositionTypeNames.Attachment;
                        mail.Attachments.Add(attachment);
                    }

                    client.Send(mail);
                }
            }
            catch(Exception ex)
            {
               
            }

            try
            {
                // удаляем старые
                string [] textFiles = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.txt");
                foreach(string file in textFiles)
                {
                    if(!file.Contains(today) && !file.Contains(yesterday) && !file.Contains(dayBeforeYesterday))
                    {
                        File.Delete(file);
                    }
                }
            }
            catch(Exception ex)
            {

            }
        }
    }
}
