﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;
using System.Globalization;
using System.Resources;

namespace VotumInfo
{
    public partial class MainWindow : Window
    {
        private MainViewModel viewModel;
        private System.Windows.Forms.NotifyIcon ni;
        public MainWindow()
        {
            InitializeComponent();
            Loaded += OnMainWindowLoaded;

            ni = new System.Windows.Forms.NotifyIcon();
            ni.Icon = Properties.Resources.vinfo_icon;

            ni.Visible = true;
            ni.DoubleClick +=
                delegate(object sender, EventArgs args)
                {
                    this.Show();
                    this.WindowState = WindowState.Normal;
                };
        }

        private void OnMainWindowLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            viewModel = DataContext as MainViewModel;
            Closing += viewModel.OnWindowClosing;

            if (viewModel.IsInit)
            {
                var initGrid = (Grid)this.FindName("InitGrid");
                initGrid.Visibility = Visibility.Hidden;
            }
        }

        protected override void OnStateChanged(EventArgs e)
        {
            if (WindowState == WindowState.Minimized)
                this.Hide();

            base.OnStateChanged(e);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ni.Visible = false;
            ni.Dispose();
        }
    }

    [ValueConversion(typeof(bool), typeof(bool))]
    public class InvertBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool booleanValue = (bool)value;
            return !booleanValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool booleanValue = (bool)value;
            return !booleanValue;
        }
    }
}
