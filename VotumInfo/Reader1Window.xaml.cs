﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VotumInfo
{
    /// <summary>
    /// Interaction logic for Reader1Window.xaml
    /// </summary>
    public partial class Reader1Window : Window
    {
        private MainViewModel viewModel;

        public Reader1Window()
        {
            InitializeComponent();            
            Loaded += OnReader1WindowLoaded;     
            StateChanged += Reader1Window_StateChanged;
        }

        private void OnReader1WindowLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            viewModel = DataContext as MainViewModel;
            Resize();
        }

        private void Resize()
        {
            this.Width = viewModel.screenWidth / 2;
            this.Height = viewModel.screenHeight / 2;
            this.Left = 0;
            this.Top = 0;
        }

        void Reader1Window_StateChanged(object sender, EventArgs e)
        {
            if (this.WindowState == WindowState.Minimized)
            {
                this.WindowState = WindowState.Normal;
            }
        }
    }
}
