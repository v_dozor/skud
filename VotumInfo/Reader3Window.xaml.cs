﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VotumInfo
{
    /// <summary>
    /// Interaction logic for Reader3Window.xaml
    /// </summary>
    public partial class Reader3Window : Window
    {
        private MainViewModel viewModel;

        public Reader3Window()
        {
            InitializeComponent();
            Loaded += OnReader3WindowLoaded;
            StateChanged += Reader3Window_StateChanged;
        }

        private void OnReader3WindowLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            viewModel = DataContext as MainViewModel;
            Resize();
        }

        private void Resize()
        {
            this.Width = viewModel.screenWidth / 2;
            this.Height = viewModel.screenHeight / 2;
            this.Left = 0;
            this.Top = viewModel.screenHeight / 2;
        }

        void Reader3Window_StateChanged(object sender, EventArgs e)
        {
            if (this.WindowState == WindowState.Minimized)
            {
                this.WindowState = WindowState.Normal;
            }
        }
    }
}
