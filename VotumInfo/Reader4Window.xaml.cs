﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VotumInfo
{
    /// <summary>
    /// Interaction logic for Reader4Window.xaml
    /// </summary>
    public partial class Reader4Window : Window
    {
        private MainViewModel viewModel;

        public Reader4Window()
        {
            InitializeComponent();
            Loaded += OnReader4WindowLoaded;
            StateChanged += Reader4Window_StateChanged;
        }

        private void OnReader4WindowLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            viewModel = DataContext as MainViewModel;
            Resize();
        }

        private void Resize()
        {
            this.Width = viewModel.screenWidth / 2;
            this.Height = viewModel.screenHeight / 2;
            this.Left = viewModel.screenWidth / 2;
            this.Top = viewModel.screenHeight / 2;
        }

        void Reader4Window_StateChanged(object sender, EventArgs e)
        {
            if (this.WindowState == WindowState.Minimized)
            {
                this.WindowState = WindowState.Normal;
            }
        }
    }
}
