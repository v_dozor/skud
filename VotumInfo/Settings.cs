﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace VotumInfo
{
    public class Settings : IDisposable
    {
        public static readonly string FileName = "settings.xml";

        // 1C
        public string Url = "http://88.87.88.93:3280/college_test";
        public string Username = "exchange_votum";
        public string Password = "eVNjvE";

        // расписание звонков
        public string lesson1Begin = "09:00";
        public string lesson1End = "10:35";
        public string lesson2Begin = "10:45";
        public string lesson2End = "12:20";
        public string lesson3Begin = "12:40";
        public string lesson3End = "14:15";
        public string lesson4Begin = "14:25";
        public string lesson4End = "16:00";  

        // настройки считывателей
        public string reader1PortName = "";
        public string reader2PortName = "";
        public string reader3PortName = "";
        public string reader4PortName = "";

        // разрешение экрана 
        public string screenWidth = "1366";
        public string screenHeight = "768";

        // дата последнего обновления расписания
        public string updateDate = "";

        // начало имени лог файла
        public string logFilenamePrefix = "";
        public bool demo = false;
        public bool demoSchedule = true;
        public bool showWindows = true;

        //сохранение настроек
        public void Save()
        {
            using (Stream writer = new FileStream(AppDomain.CurrentDomain.BaseDirectory + FileName, FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                serializer.Serialize(writer, this);
            }
        }

        //загрузка настроек
        public static Settings Load()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Settings));

            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + FileName))
            {
                using (Stream stream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + FileName, FileMode.Open))
                {
                    return (Settings)serializer.Deserialize(stream);
                }
            }
            return null;
        }

        public void Dispose()
        {
            
        }
    }
}
