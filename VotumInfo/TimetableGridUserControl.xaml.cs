﻿using DevExpress.Xpf.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VotumInfo
{
    /// <summary>
    /// Interaction logic for TimetableGrid.xaml
    /// </summary>
    public partial class TimetableGridUserControl : UserControl
    {
        public object ItemsSource
        {
            get { return (object)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(object), typeof(TimetableGridUserControl), new PropertyMetadata(null));

        public object SelectedItem
        {
            get { return (object)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(object), typeof(TimetableGridUserControl), new PropertyMetadata(null));

        public TimetableGridUserControl()
        {
            InitializeComponent();        
            //IsVisibleChanged += TimetableGridUserControl_IsVisibleChanged;
            //Loaded += TimetableGridUserControl_Loaded;    

            //gridControl.CurrentItem = null;

            //(gridControl.View as TableView).FocusedRowHandle = 3;
            //(gridControl.View as TableView).Focus();
        }

        /*private void TimetableGridUserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //var desktopWorkingArea = System.Windows.SystemParameters.WorkArea;
            //this.FontSize = (int) (10 * desktopWorkingArea.Height / 728);
               
        }

        private void TimetableGridUserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            (gridControl.View as TableView).FocusedRowHandle = 3;
            (gridControl.View as TableView).Focus();
        }*/
    }
}
