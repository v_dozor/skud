﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VotumInfo
{
    public class WeekTimetable
    {
        /// <summary>
        /// Тип недели(четная/нечетная)
        /// </summary>
        public String WeekType;

        /// <summary>
        /// Список пар в понедельник
        /// </summary>
        public LessonModel[] MondayLessons { get; set; }

        /// <summary>
        /// Список пар во вторник
        /// </summary>
        public LessonModel[] TuesdayLessons { get; set; }

        /// <summary>
        /// Список пар в среду
        /// </summary>
        public LessonModel[] WednesdayLessons { get; set; }

        /// <summary>
        /// Список пар в четверг
        /// </summary>
        public LessonModel[] ThursdayLessons { get; set; }

        /// <summary>
        /// Список пар в пятницу
        /// </summary>
        public LessonModel[] FridayLessons { get; set; }

        /// <summary>
        /// Список пар в субботу
        /// </summary>
        public LessonModel[] SaturdayLessons { get; set; }

        public WeekTimetable()
        {
            MondayLessons = new LessonModel[4];
            TuesdayLessons = new LessonModel[4];
            WednesdayLessons = new LessonModel[4];
            ThursdayLessons = new LessonModel[4];
            FridayLessons = new LessonModel[4];
            SaturdayLessons = new LessonModel[4];
        }

        /// <summary>
        /// Возвращает список пар по дню недели
        /// </summary>
        /// <param name="dayOfWeek">День недели</param>
        /// <returns></returns>
        public LessonModel[] GetLessonsByDayOfWeek(DayOfWeek dayOfWeek)
        {
            if (dayOfWeek == DayOfWeek.Monday)
                return MondayLessons;
            if (dayOfWeek == DayOfWeek.Tuesday)
                return TuesdayLessons;
            if (dayOfWeek == DayOfWeek.Wednesday)
                return WednesdayLessons;
            if (dayOfWeek == DayOfWeek.Thursday)
                return ThursdayLessons;
            if (dayOfWeek == DayOfWeek.Friday)
                return FridayLessons;
            if (dayOfWeek == DayOfWeek.Saturday)
                return SaturdayLessons;
            return null;
        }

        /// <summary>
        /// Задает список пар на день недели
        /// </summary>
        /// <param name="dayOfWeek">День недели</param>
        /// <param name="lessons">Список пар</param>
        public void SetLessonsByDayOfWeek(DayOfWeek dayOfWeek, LessonModel[] lessons)
        {
            if (dayOfWeek == DayOfWeek.Monday)
                MondayLessons = lessons;
            if (dayOfWeek == DayOfWeek.Tuesday)
                TuesdayLessons = lessons;
            if (dayOfWeek == DayOfWeek.Wednesday)
                WednesdayLessons = lessons;
            if (dayOfWeek == DayOfWeek.Thursday)
                ThursdayLessons = lessons;
            if (dayOfWeek == DayOfWeek.Friday)
                FridayLessons = lessons;
            if (dayOfWeek == DayOfWeek.Saturday)
                SaturdayLessons = lessons;
        }

        /// <summary>
        /// Устанавливает пару в определенное место в расписании
        /// </summary>
        /// <param name="dayOfWeek">День недели</param>
        /// <param name="lessonNumber">Номер пары</param>
        /// <param name="lesson">Пара</param>
        public void SetLesson(DayOfWeek dayOfWeek, int lessonNumber, LessonModel lesson)
        {
            if (dayOfWeek == DayOfWeek.Monday && MondayLessons.Count() >= lessonNumber)
                MondayLessons[lessonNumber - 1] = lesson;
            if (dayOfWeek == DayOfWeek.Tuesday && TuesdayLessons.Count() >= lessonNumber)
                TuesdayLessons[lessonNumber - 1] = lesson;
            if (dayOfWeek == DayOfWeek.Wednesday && WednesdayLessons.Count() >= lessonNumber)
                WednesdayLessons[lessonNumber - 1] = lesson;
            if (dayOfWeek == DayOfWeek.Thursday && ThursdayLessons.Count() >= lessonNumber)
                ThursdayLessons[lessonNumber - 1] = lesson;
            if (dayOfWeek == DayOfWeek.Friday && FridayLessons.Count() >= lessonNumber)
                FridayLessons[lessonNumber - 1] = lesson;
            if (dayOfWeek == DayOfWeek.Saturday && SaturdayLessons.Count() >= lessonNumber)
                SaturdayLessons[lessonNumber - 1] = lesson;
        }
    }
}
