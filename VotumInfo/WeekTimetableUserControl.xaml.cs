﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VotumInfo
{
    /// <summary>
    /// Interaction logic for WeekTimetable.xaml
    /// </summary>
    public partial class WeekTimetableUserControl : UserControl
    {
        public object WeekItemsSource
        {
            get { return (object)GetValue(WeekItemsSourceProperty); }
            set { SetValue(WeekItemsSourceProperty, value); }
        }

        public static readonly DependencyProperty WeekItemsSourceProperty =
            DependencyProperty.Register("WeekItemsSource", typeof(object), typeof(WeekTimetableUserControl), new PropertyMetadata(null));

        public object SelectedItem
        {
            get { return (object)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(object), typeof(WeekTimetableUserControl), new PropertyMetadata(null));

        public WeekTimetableUserControl()
        {
            InitializeComponent();
            Loaded += WeekTimetableUserControl_Loaded;            

            IsVisibleChanged += WeekTimetableUserControl_IsVisibleChanged;
        }

        private void WeekTimetableUserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Settings settings = Settings.Load();
            this.FontSize = (int)(10 * Convert.ToInt32(settings.screenHeight) / 768);
        }

        private void WeekTimetableUserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var mondayGrid = (Grid)this.FindName("MondayGrid");
            var tuesdayGrid = (Grid)this.FindName("TuesdayGrid");
            var wednesdayGrid = (Grid)this.FindName("WednesdayGrid");
            var thursdayGrid = (Grid)this.FindName("ThursdayGrid");
            var fridayGrid = (Grid)this.FindName("FridayGrid");
            var saturdayGrid = (Grid)this.FindName("SaturdayGrid");             

            if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
                mondayGrid.Background = Brushes.LightSalmon;
            if (DateTime.Now.DayOfWeek == DayOfWeek.Tuesday)
                tuesdayGrid.Background = Brushes.LightSalmon;
            if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
                wednesdayGrid.Background = Brushes.LightSalmon;
            if (DateTime.Now.DayOfWeek == DayOfWeek.Thursday)
            {
                thursdayGrid.Background = Brushes.LightSalmon;
            }
                
            if (DateTime.Now.DayOfWeek == DayOfWeek.Friday)
                fridayGrid.Background = Brushes.LightSalmon;
            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
                saturdayGrid.Background = Brushes.LightSalmon;
        }
    }
}
