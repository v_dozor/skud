﻿using System;

namespace ControllerConnectors
{

    public class ConnectorErrorEventArgs : EventArgs
    {
        public ConnectorErrorEventArgs(uint errorCode, string message)
        {
            ErrorCode = errorCode;
            Message = message;
        }
        uint ErrorCode;
        string Message;
    }

    public partial class Z5rwebConnector
    {
        public event EventHandler<ConnectorErrorEventArgs> OnError;

        /// <summary>
        /// Use this in this class to provide errors
        /// </summary>
        /// <param name="code">Error code</param>
        private void HandleError(int code = 0, string additionalInf = null)
        {
            if (code == 0)
                return;
            if(OnError != null)
            {
                OnError(this, new ConnectorErrorEventArgs(Convert.ToUInt32(code), ErrString(code)));
            }
            else
            {
                string pattern = "Код ошибки: {0} - {1}. {2}";
                string errStr = string.Format(
                    pattern,
                    ((uint)code).ToString("x4"), 
                    ErrString(code), 
                    additionalInf != null ? additionalInf : "");
                throw new Exception(errStr);
            }
        }

        public static string ErrString(int hr)
        {
            uint uhr = (uint)hr;
            string msg = string.Empty;
            switch(uhr)
            {
                case 0x80004005:
                    msg = "Незвестная ошибка"; 
                    break;
                case 0x80070057:
                    msg = "Неправильные параметры";
                    break;
                case 0x80004002:
                    msg = "Функция не поддерживается";
                    break;
                case 0x8007000E:
                    msg = "Недостаточно памяти для обработки команды";
                    break;
                case 0x80000006:
                    msg = "Неправильный дескриптор";
                    break;
                case 0x80000007:
                    msg = "Функция прервана ";
                    break;
                case 0x80000009:
                    msg = "Отказано в доступе";
                    break;
                case 0x00040201:
                    msg = "Отменено пользователем";
                    break;
                case 0x00040202:
                    msg = "Не найден (для функций поиска)";
                    break;
                case 0x00040203:
                    msg = "Операция завершилась по тайм-ауту";
                    break;
                case 0x80040203:
                    msg = "Порт не существует";
                    break;
                case 0x80040205:
                    msg = "Другая ошибка открытия порта";
                    break;
                case 0x80040206:
                    msg = "Ошибка порта (Конвертор отключен от USB?)";
                    break;
                case 0x80040207:
                    msg = "Ошибка настройки порта";
                    break;
                case 0x80040208:
                    msg = "Неудалось загрузить Ftd2xx.dll";
                    break;
                case 0x80040209:
                    msg = "Не удалось инициализировать сокеты";
                    break;
                case 0x8004020a:
                    msg = "Дескриптор закрыт со стороны сервера";
                    break;
                case 0x8004020B:
                    msg = "Не проинициализировано с помощью ZG_Initialize";
                    break;
                case 0x8004020C:
                    msg = "Размер буфера слишком мал";
                    break;
                case 0x8004020D:
                    msg = "Нет связи с устройством";
                    break;
                case 0x80040301:
                    msg = "Слишком большое сообщение для отправки";
                    break;
                case 0x80040303:
                    msg = "Нет ответа";
                    break;
                case 0x80040304:
                    msg = "Нераспознанный ответ";
                    break;
                case 0x80040305:
                    msg = "Не правильная версия ZPort.dll";
                    break;
                case 0x80040306:
                    msg = "Конвертер занят (при открытии конвертера в режиме (Proxy)";
                    break;
                case 0x80040307:
                    msg = "Другая ошибка конвертера";
                    break;
                case 0x80040308:
                    msg = "Ошибка конвертера: Нет такой лицензии";
                    break;
                case 0x80040309:
                    msg = "Текущая лицензия истекла";
                    break;
                case 0x8004030A:
                    msg = "Ошибка конвертера: ограничение лицензии на число";
                    break;
                case 0x8004030B:
                    msg = "Ограничение лицензии на число ключей при чтении";
                    break;
                case 0x8004030C:
                    msg = "Ограничение лицензии на число ключей при записи";
                    break;
                case 0x8004030D:
                    msg = "Срок лицензии истек (определено при установке даты в контроллере)";
                    break;
                case 0x8004030E:
                    msg = "Неверный адрес конвертера (конвертер не найден)";
                    break;
                case 0x8004030F:
                    msg = "Неверный адрес контроллера (контроллер не найден)";
                    break;
                case 0x80040310:
                    msg = "Контроллер отказал в выполнении команды";
                    break;
                case 0x80040311:
                    msg = "Загрузчик не запустился (при прошивке)";
                    break;
                case 0x80040312:
                    msg = "Некорректный размер файла (при прошивке)";
                    break;
                case 0x80040313:
                    msg = "Не обнаружен старт прошивки. Попробуйте перезагрузить устройство (при прошивке)";
                    break;
                case 0x80040314:
                    msg = "Не подходит для этого устройства (при прошивке)";
                    break;
                case 0x80040315:
                    msg = "Не подходит для этого номера устройства (при прошивке)";
                    break;
                case 0x80040316:
                    msg = "Слишком большой размер данных прошивки (при прошивке)";
                    break;
                case 0x80040317:
                    msg = "Некорректная последовательность данных (при прошивке)";
                    break;
                case 0x80040318:
                    msg = "Целостность данных нарушена (при прошивке)";
                    break;
                default:
                    msg = "Ошибка";
                    break;
            }
            return msg;


        }
    }
}
