﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZGuard;

namespace ControllerConnectors
{
    public class ElectroEvent : IControllerEvent
    {
        public ElectroEvent(byte[] rawData, ZG_EC_SUB_EV callCondition, uint powerStateMask, ZG_CTR_EV_TYPE type = 0, DateTime date = new DateTime())
        {
            Array.Copy(rawData, this.rawData, rawData.Length);
            eventType = type;
            this.date = date;
            this.callCondition = callCondition;
            this.powerStateMask = powerStateMask;
        }

        private DateTime date;
        public DateTime Date
        {
            get
            {
                return date;
            }
        }

        public EventFuncType EventFuncType
        {
            get
            {
                return EventFuncType.Electro;
            }
        }

        private ZG_CTR_EV_TYPE eventType;
        public ZG_CTR_EV_TYPE EventType
        {
            get
            {
                return eventType;
            }
        }

        byte[] rawData = new byte[8];
        public byte[] RawData
        {
            get
            {
                return rawData;
            }
        }

        private ZG_EC_SUB_EV callCondition;
        public ZG_EC_SUB_EV CallCondition
        {
            get
            {
                return callCondition;
            }
        }

        private uint powerStateMask = uint.MaxValue;
        public string PowerState
        {
            get { return PowerStateMaskToString(powerStateMask); }
        }

        public string CallConditionString
        {
            get
            {
                return EventContitonToString(callCondition);
            }
        }

        public static string PowerStateMaskToString(uint powerStateMask)
        {
            string msg = "Состояние электропитания:";
            if (powerStateMask == uint.MaxValue)
            {
                return msg + "Данных нет";
            }

            msg += Environment.NewLine;
            msg += "Состояние питания: " +
                ((powerStateMask & ZGIntf.ZG_EC_SF_ENABLED) != 0 ? "Включено" : "Выключено") +
                Environment.NewLine;
            msg += "Активно включение по временной зоне: " +
                ((powerStateMask & ZGIntf.ZG_EC_SF_SCHEDULE) != 0 ? "Да" : "Нет") +
                Environment.NewLine;
            msg += "Включено по команде по сети: " +
                ((powerStateMask & ZGIntf.ZG_EC_SF_REMOTE) != 0 ? "Да" : "Нет") +
                Environment.NewLine;
            msg += "Идет отработка задержки: " +
                ((powerStateMask & ZGIntf.ZG_EC_SF_DELAY) != 0 ? "Да" : "Нет") +
                Environment.NewLine;
            msg += "Карта в поле контрольного считывателя: " +
                ((powerStateMask & ZGIntf.ZG_EC_SF_CARD) != 0 ? "Да" : "Нет") +
                Environment.NewLine;

            return msg;
        }

        public static string EventContitonToString(ZG_EC_SUB_EV cond)
        {
            string msg = "";
            switch (cond)
            {
                case ZG_EC_SUB_EV.ZG_EC_EV_UNDEF:
                    msg = "Не известно";
                    break;
                case ZG_EC_SUB_EV.ZG_EC_EV_CARD_DELAY:
                    msg = "Поднесена валидная карта с другой стороны (для входа) запущена задержка";
                    break;
                case ZG_EC_SUB_EV.ZG_EC_EV_RESERVED1:
                    msg = "(зарезервировано)";
                    break;
                case ZG_EC_SUB_EV.ZG_EC_EV_ON_NET:
                    msg = "Включено командой по сети";
                    break;
                case ZG_EC_SUB_EV.ZG_EC_EV_ON_SCHED:
                    msg = "Включено по временной зоне";
                    break;
                case ZG_EC_SUB_EV.ZG_EC_EV_OFF_SHED:
                    msg = "Выключено по временной зоне";
                    break;
                case ZG_EC_SUB_EV.ZG_EC_EV_CARD:
                    msg = "Поднесена валидная карта к контрольному устройству";
                    break;
                case ZG_EC_SUB_EV.ZG_EC_EV_RESERVED2:
                    msg = "(зарезервировано)";
                    break;
                case ZG_EC_SUB_EV.ZG_EC_EV_OFF_TIMEOUT:
                    msg = "Выключено после отработки таймаута";
                    break;
                case ZG_EC_SUB_EV.ZG_EC_EV_OFF_EXIT:
                    msg = "Выключено по срабатыванию датчика выхода";
                    break;
            }
            return msg;
        }

        public override string ToString()
        {
            return PowerState + CallConditionString;
        }
    }
}
