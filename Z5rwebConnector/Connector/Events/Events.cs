﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZGuard;
using ZPort;

namespace ControllerConnectors
{
    public partial class Z5rwebConnector
    {
        public Dictionary<int, IControllerEvent> EventsBuffer = new Dictionary<int, IControllerEvent>();

        /// <summary>
        /// Index of controller pointer, it points on read position
        /// </summary>
        private int readEventIndex;

        /// <summary>
        /// Index of controller pointer, it points on write position
        /// </summary>
        private int writeEventIndex;

        /// <summary>
        /// Get events indexes from controller and set to writeEventIndex, readEventIndex fields
        /// </summary>
        private void GetEventIndexes()
        {
            ZGIntf.ZG_Ctr_ReadEventIdxs(ControllerHandle, ref writeEventIndex, ref readEventIndex);
        }

        /// <summary>
        /// Set read\write indexes to controller
        /// </summary>
        /// <param name="readEventIndex">Read index, set null if you do not wont to use it</param>
        /// <param name="writeEventIndex">Write index, set null if you do not wont to use it</param>
        private void SetEventIndexes(int? readEventIndex = null, int? writeEventIndex = null)
        {
            uint mask = 0;
            if (writeEventIndex != null)
                mask |= 1;
            if (readEventIndex != null)
                mask |= 2;

            if (mask == 0)
                return;

            HandleError(ZGIntf.ZG_Ctr_WriteEventIdxs(
                ControllerHandle,
                mask,
                writeEventIndex != null ? (int)writeEventIndex : -1,
                readEventIndex != null ? (int)readEventIndex : -1
                ));

            if (writeEventIndex != null)
                this.writeEventIndex = (int)writeEventIndex;

            if (readEventIndex != null)
                this.readEventIndex = (int)readEventIndex;
        }

        /// <summary>
        /// Filter events from buffer
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<IControllerEvent> GetEventsByType(ZG_CTR_EV_TYPE type)
        {
            return EventsBuffer.Values.Where((x) => { return x.EventType == type; });
        }

        /// <summary>
        /// Method-iterator that makes possible iterate all events in controller
        /// </summary>
        /// <returns>Event</returns>
        public IEnumerable<ZG_CTR_EVENT> GetEventsFromCotroller(int blockCount = 0)
        {
            int newEventsCount = 0;
            
            int j = 0;
            int readRest = 0;

            //if you don't need separate to blocks
            if (blockCount == 0)
                blockCount = 6;
            
            //refresh read/write indexes
            GetEventIndexes();

            //this two formulas below were taken from documentation
            if (readEventIndex < writeEventIndex)
                newEventsCount = writeEventIndex - readEventIndex;
            else
                newEventsCount = maxEvents - readEventIndex + writeEventIndex;
            
            ZG_CTR_EVENT[] events = new ZG_CTR_EVENT[blockCount];

            for (j = 0; j < writeEventIndex / blockCount + 1; j++)
            {
                if (j * blockCount + blockCount < writeEventIndex)
                {
                    ZGIntf.ZG_Ctr_ReadEvents(ControllerHandle, j * blockCount, events, blockCount, null);
                }
                else
                {
                    readRest = writeEventIndex - j * blockCount;
                    if (readRest == 0)
                        yield break;
                    events = new ZG_CTR_EVENT[readRest];
                    ZGIntf.ZG_Ctr_ReadEvents(ControllerHandle, j * blockCount, events, readRest, null);
                }
                //iterate block of events
                foreach (var item in events)
                {
                    yield return item;
                }
            }
            yield break;
        }

        /// <summary>
        /// Refresh events buffers from controller
        /// </summary>
        public void RefreshEventBuffer()
        {
            int i = 0;
            EventsBuffer.Clear();
            foreach (var p in GetEventsFromCotroller())
            {
                switch(p.nType)
                {
                    case ZG_CTR_EV_TYPE.ZG_EV_BUT_OPEN:
                    case ZG_CTR_EV_TYPE.ZG_EV_KEY_NOT_FOUND:
                    case ZG_CTR_EV_TYPE.ZG_EV_KEY_OPEN:
                    case ZG_CTR_EV_TYPE.ZG_EV_KEY_ACCESS:
                    case ZG_CTR_EV_TYPE.ZG_EV_REMOTE_OPEN:
                    case ZG_CTR_EV_TYPE.ZG_EV_KEY_DOOR_BLOCK:
                    case ZG_CTR_EV_TYPE.ZG_EV_BUT_DOOR_BLOCK:
                    case ZG_CTR_EV_TYPE.ZG_EV_NO_OPEN:
                    case ZG_CTR_EV_TYPE.ZG_EV_NO_CLOSE:
                    case ZG_CTR_EV_TYPE.ZG_EV_PASSAGE:
                    case ZG_CTR_EV_TYPE.ZG_EV_SENSOR1:
                    case ZG_CTR_EV_TYPE.ZG_EV_SENSOR2:
                    case ZG_CTR_EV_TYPE.ZG_EV_REBOOT:
                    case ZG_CTR_EV_TYPE.ZG_EV_BUT_BLOCK:
                    case ZG_CTR_EV_TYPE.ZG_EV_DBL_PASSAGE:
                    case ZG_CTR_EV_TYPE.ZG_EV_OPEN:
                    case ZG_CTR_EV_TYPE.ZG_EV_CLOSE:
                    case ZG_CTR_EV_TYPE.ZG_EV_POWEROFF:
                    case ZG_CTR_EV_TYPE.ZG_EV_LOCK_CONNECT:
                    case ZG_CTR_EV_TYPE.ZG_EV_LOCK_DISCONNECT:
                    case ZG_CTR_EV_TYPE.ZG_EV_GATEWAY_PASS:
                    case ZG_CTR_EV_TYPE.ZG_EV_GATEWAY_BLOCK:
                    case ZG_CTR_EV_TYPE.ZG_EV_GATEWAY_ALLOWED:
                    case ZG_CTR_EV_TYPE.ZG_EV_ANTIPASSBACK:
                        PassEvent passEv = DecodePassEvent(p);
                        EventsBuffer.Add(i, passEv);
                        break;
                    case ZG_CTR_EV_TYPE.ZG_EV_ELECTRO_ON:
                    case ZG_CTR_EV_TYPE.ZG_EV_ELECTRO_OFF:
                        //ElectroEvent elEvent = DecodeElectrolEvent(p);
                        //EventsBuffer.Add(i, elEvent);
                        break;
                    case ZG_CTR_EV_TYPE.ZG_EV_MODE_STATE:
                        break;
                    case ZG_CTR_EV_TYPE.ZG_EV_FIRE_STATE:
                        break;
                    case ZG_CTR_EV_TYPE.ZG_EV_SECUR_STATE:
                        break;
                    case ZG_CTR_EV_TYPE.ZG_EV_UNKNOWN_KEY:
                        break;
                    case ZG_CTR_EV_TYPE.ZG_EV_HOTEL40:
                    case ZG_CTR_EV_TYPE.ZG_EV_HOTEL41:
                        //HotelEvent hev = DecodeHotelEvent(p);
                        //EventsBuffer.Add(i, hev);
                        break;
                }
                ++i;
            }
        }

        /// <summary>
        /// Trying to convert ZG_CTR_EVENT to PassEvent
        /// Inner buffer (BankBuffer0, BankBuffer1) must have actual data
        /// Actual data you can get by using RefreshBuffers method
        /// </summary>
        /// <returns>ZG_CTR_EVENT event</returns>
        private PassEvent DecodePassEvent(ZG_CTR_EVENT ev)
        {
            ZG_EV_TIME cdate = new ZG_EV_TIME();
            byte[] rawData = new byte[8];
            ZG_CTR_DIRECT direct = new ZG_CTR_DIRECT();
            int keyIndex = 0;
            int keyBank = 0;

            ZGIntf.ZG_Ctr_DecodePassEvent(
                ControllerHandle,
                ev.aData,
                ref cdate,
                ref direct,
                ref keyIndex,
                ref keyBank);
            DateTime date = Z5rwebConnector.FormatDate(cdate);
            string msg = Z5rwebConnector.EventString(ev.nType);

            byte[] rfid = new byte[16];
            KeyValuePair<int, ZG_CTR_KEY>? kv = null;

            if (keyBank == 0)
            {
                kv = BankBuffer0.Values.FirstOrDefault((x) => { if (x.Key == keyIndex) return true; else return false; });
            }
            else if(keyBank == 1)
            {
                kv = BankBuffer1.Values.FirstOrDefault((x) => { if (x.Key == keyIndex) return true; else return false; });
            }
            
            if(kv != null && kv.Value.Value.rNum != null && kv.Value.Value.rNum.Length != 0)
            {
                Array.Copy(kv.Value.Value.rNum, rfid, rfid.Length);
            }

            PassEvent passEv = new PassEvent(
                ev.aData,
                ev.nType,
                date,
                msg,
                rfid,
                direct,
                keyBank == 0 ? BankUsage.FirstBank : BankUsage.SecondBank,
                keyIndex
                );
            return passEv;
        }

        /// <summary>
        /// Trying to convert ZG_CTR_EVENT to PassEvent
        /// Inner buffer (BankBuffer0, BankBuffer1) must have actual data
        /// Actual data you can get by using RefreshBuffers method
        /// </summary>
        /// <param name="ev">Raw event data</param>
        /// <returns>ZG_CTR_EVENT event</returns>
        private ElectroEvent DecodeElectrolEvent(ZG_CTR_EVENT ev)
        {
            ZG_EV_TIME cdate = new ZG_EV_TIME();
            ZG_EC_SUB_EV subEv = ZG_EC_SUB_EV.ZG_EC_EV_UNDEF;
            uint mask = 0;

            ZGIntf.ZG_Ctr_DecodeEcEvent(
                ControllerHandle,
                ev.aData,
                ref cdate,
                ref subEv,
                ref mask
                );

            DateTime date = Z5rwebConnector.FormatDate(cdate);

            ElectroEvent elEv = new ElectroEvent(
                ev.aData,
                subEv,
                mask,
                ev.nType,
                date
                );
            return elEv;
        }

        /// <summary>
        /// Trying to convert ZG_CTR_EVENT to PassEvent
        /// Inner buffer (BankBuffer0, BankBuffer1) must have actual data
        /// Actual data you can get by using RefreshBuffers method
        /// </summary>
        /// <param name="ev">Raw event data</param>
        /// <returns>ZG_CTR_EVENT event</returns>
        private HotelEvent DecodeHotelEvent(ZG_CTR_EVENT ev)
        {
            ZG_EV_TIME cdate = new ZG_EV_TIME();
            ZG_HOTEL_SUB_EV subEv = ZG_HOTEL_SUB_EV.ZG_H_EV_UNDEF;
            ZG_HOTEL_MODE mode = ZG_HOTEL_MODE.ZG_HMODE_UNDEF;
            uint mask = 0;

            ZGIntf.ZG_DecodeHotelEvent(
                ev.aData,
                ref cdate,
                ref mode,
                ref subEv,
                ref mask
                );

            DateTime date = Z5rwebConnector.FormatDate(cdate);
            HotelEvent hev = new HotelEvent(
                ev.aData,
                ev.nType,
                date,
                mode,
                subEv,
                mask);
            return hev;
        }

        /// <summary>
        /// Set controller read/write indexes to 0 and clear EventBuffer
        /// </summary>
        public void ClearControllerEventBuffer()
        {
            SetEventIndexes(0, 0);
        }
        
        /// <summary>
        /// Helps to convert event code to string text
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static string EventString(ZG_CTR_EV_TYPE code)
        {
            string msg = string.Empty;   
            switch(code)
            {
                case ZG_CTR_EV_TYPE.ZG_EV_UNKNOWN:
                    msg = "Не известное";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_BUT_OPEN:
                    msg = "Открыто кнопкой изнутри";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_KEY_NOT_FOUND:
                    msg = "Ключ не найден в банке ключей";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_KEY_OPEN:
                    msg = "Ключ найден, дверь открыта";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_KEY_ACCESS:
                    msg = "Ключ найден, доступ не разрешен";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_REMOTE_OPEN:
                    msg = "Открыто оператором по сети";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_KEY_DOOR_BLOCK:
                    msg = "Ключ найден, дверь заблокирована";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_BUT_DOOR_BLOCK:
                    msg = "Попытка открыть заблокированную дверь кнопкой";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_NO_OPEN:
                    msg = "Дверь взломана";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_NO_CLOSE:
                    msg = "Дверь оставлена открытой (timeout)";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_PASSAGE:
                    msg = "Проход состоялся";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_SENSOR1:
                    msg = "Сработал датчик 1";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_SENSOR2:
                    msg = "Сработал датчик 2";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_REBOOT:
                    msg = "Перезагрузка контроллера";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_BUT_BLOCK:
                    msg = "Заблокирована кнопка открывания";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_DBL_PASSAGE:
                    msg = "Попытка двойного прохода";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_OPEN:
                    msg = "Дверь открыта штатно"; 
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_CLOSE:
                    msg = "Дверь закрыта";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_POWEROFF:
                    msg = "Пропало питание";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_ELECTRO_ON:
                    msg = "Включение электропитания";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_ELECTRO_OFF:
                    msg = "Выключение электропитания";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_LOCK_CONNECT:
                    msg = "Включение замка (триггер)";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_LOCK_DISCONNECT:
                    msg = "Отключение замка (триггер)";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_MODE_STATE:
                    msg = "Переключение режимов работы (cм Режим)";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_FIRE_STATE:
                    msg = "Изменение состояния Пожара";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_SECUR_STATE:
                    msg = "Изменение состояния Охраны";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_UNKNOWN_KEY:
                    msg = "Неизвестный ключ";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_GATEWAY_PASS:
                    msg = "Совершен вход в шлюз";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_GATEWAY_BLOCK:
                    msg = "Заблокирован вход в шлюз (занят)";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_GATEWAY_ALLOWED:
                    msg = "Разрешен вход в шлюз";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_ANTIPASSBACK:
                    msg = "Заблокирован проход (Антипассбек)";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_HOTEL40:
                    msg = "Изменилось состояние Hotel";
                    break;
                case ZG_CTR_EV_TYPE.ZG_EV_HOTEL41:
                    msg = "Изменилось состояние Hotel";
                    break;
            }
            
            return msg;
        }

        /// <summary>
        /// Helps to convert ZG_EV_TIME to DateTime
        /// </summary>
        /// <param name="cdate"></param>
        /// <returns></returns>
        public static DateTime FormatDate(ZG_EV_TIME cdate)
        {
            if (cdate.nMonth == 0 || cdate.nDay == 0)
                return DateTime.MinValue;
            DateTime date = new DateTime(
                DateTime.Now.Year,
                cdate.nMonth,
                cdate.nDay,
                cdate.nHour,
                cdate.nMinute,
                cdate.nSecond,
                0,
                DateTimeKind.Utc
                );
            return date;
        }
    }
}
