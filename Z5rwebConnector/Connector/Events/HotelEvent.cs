﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZGuard;

namespace ControllerConnectors
{
    public class HotelEvent : IControllerEvent
    {
        public HotelEvent(byte[] rawData, ZG_CTR_EV_TYPE type, DateTime date, ZG_HOTEL_MODE mode, ZG_HOTEL_SUB_EV callCondition, uint stateMask)
        {
            Array.Copy(rawData, this.rawData, rawData.Length);
            this.eventType = type;
            this.date = date;
            this.mode = mode;
            this.callCondition = callCondition;
            this.stateMask = stateMask;
        }

        private DateTime date;
        public DateTime Date
        {
            get
            {
                return date;
            }
        }

        public EventFuncType EventFuncType
        {
            get
            {
                return EventFuncType.Hotel;
            }
        }

        private ZG_CTR_EV_TYPE eventType;
        public ZG_CTR_EV_TYPE EventType
        {
            get
            {
                return eventType;
            }
        }

        byte[] rawData = new byte[8];
        public byte[] RawData
        {
            get
            {
                return rawData;
            }
        }

        private ZG_HOTEL_MODE mode;
        public string ModeString
        {
            get
            {
                return HotelModeToString(mode);
            }
        }
        public ZG_HOTEL_MODE Mode
        {
            get { return mode; }
        }

        private ZG_HOTEL_SUB_EV callCondition;
        public string CallConditionString
        {
            get { return CallConditionToString(callCondition); }
        }
        public ZG_HOTEL_SUB_EV CallCondition
        {
            get { return callCondition; }
        }

        private uint stateMask;
        public uint StateMask
        {
            get { return stateMask; }
        }
        public string StateMaskString
        {
            get { return StateMaskToString(stateMask); }
        }

        public static string HotelModeToString(ZG_HOTEL_MODE mode)
        {
            string msg = "";
            switch(mode)
            {
                case ZG_HOTEL_MODE.ZG_HMODE_UNDEF:
                    msg = "Неизвестно";
                    break;
                case ZG_HOTEL_MODE.ZG_HMODE_NORMAL:
                    msg = "Обычный режим работы";
                    break;
                case ZG_HOTEL_MODE.ZG_HMODE_BLOCK:
                    msg = "Блокировка. Проходить могут только блокирующие карты.";
                    break;
                case ZG_HOTEL_MODE.ZG_HMODE_FREE:
                    msg = "Свободный. Замок обесточен, при поднесении карты регистрируются";
                    break;
                case ZG_HOTEL_MODE.ZG_HMODE_RESERVED:
                    msg = "(Reserved) Ожидание. Обычный режим работы, при поднесении допустимой карты переход в режим Свободный (Free)";
                    break;
            }
            return msg;
        }

        public static string CallConditionToString(ZG_HOTEL_SUB_EV callCondition)
        {
            string msg = "";
            switch(callCondition)
            {
                case ZG_HOTEL_SUB_EV.ZG_H_EV_UNDEF:
                    msg = "Не определено";
                    break;
                case ZG_HOTEL_SUB_EV.ZG_H_EV_FREECARD:
                    msg = "Карта открытия";
                    break;
                case ZG_HOTEL_SUB_EV.ZG_H_EV_BLOCKCARD:
                    msg = "Карта блокирующая";
                    break;
                case ZG_HOTEL_SUB_EV.ZG_H_EV_EXFUNC:
                    msg = "Дополнительная функция";
                    break;
                case ZG_HOTEL_SUB_EV.ZG_H_EV_NEWRCARD:
                    msg = "Cоздана резервная карта";
                    break;
                case ZG_HOTEL_SUB_EV.ZG_H_EV_NETWORK:
                    msg = "";
                    break;
                case ZG_HOTEL_SUB_EV.ZG_H_EV_TIMEZONE:
                    msg = "";
                    break;
                case ZG_HOTEL_SUB_EV.ZG_H_EV_COUNTER:
                    msg = "Обновлен счетчик";
                    break;
                case ZG_HOTEL_SUB_EV.ZG_H_EV_CRYPTOKEY:
                    msg = "Обновлен криптоключ";
                    break;
                case ZG_HOTEL_SUB_EV.ZG_H_EV_PULSEZ:
                    msg = "Измененение защелки в течении 2х секунд";
                    break;
                case ZG_HOTEL_SUB_EV.ZG_H_EV_STATE:
                    msg = "Состояние защелки -если нажали ручку и отпустили более чем через 2 секунды";
                    break;
            }

            return msg;
        }

        public static string StateMaskToString(uint stateMask)
        {
            string msg = "";
            if (stateMask == uint.MaxValue)
            {
                return msg + "Данных нет";
            }
            
            msg += "Защёлка: " +
                ((stateMask & ZGIntf.ZG_HF_LATCH) != 0 ? "Включено" : "Выключено") +
                Environment.NewLine;
            msg += "Задвижка: " +
                ((stateMask & ZGIntf.ZG_HF_LATCH2) != 0 ? "Включено" : "Выключено") +
                Environment.NewLine;
            msg += "Ключ: " +
                ((stateMask & ZGIntf.ZG_HF_KEY) != 0 ? "Включено" : "Выключено") +
                Environment.NewLine;
            msg += "Карта: " +
                ((stateMask & ZGIntf.ZG_HF_CARD) != 0 ? "Включено" : "Выключено");

            return msg;
        }

        public override string ToString()
        {
            return
                "Флаги состояния: " + StateMaskString + Environment.NewLine +
                "Условие, вызвавшее событие: " + CallConditionString + Environment.NewLine +
                "Режим: " + ModeString;
        }
    }
}
