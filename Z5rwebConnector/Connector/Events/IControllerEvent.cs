﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZGuard;

namespace ControllerConnectors
{
    public interface IControllerEvent
    {
        byte[] RawData { get; }
        EventFuncType EventFuncType { get; }
        DateTime Date { get; }
        ZG_CTR_EV_TYPE EventType { get; }
        string ToString();
    }
}
