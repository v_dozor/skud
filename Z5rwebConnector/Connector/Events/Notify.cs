﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZGuard;
using ZPort;
using System.Threading;

namespace ControllerConnectors
{
    public class PassageEventArgs : EventArgs
    {
        
    }

    public partial class Z5rwebConnector
    {
        public event EventHandler<PassageEventArgs> NewPassage;

        private IntPtr NotifyHandle = IntPtr.Zero;
        private bool notifyWorking = false;

        /// <summary>
        /// Used for inner ZGuard notification system
        /// Non signal state - thread will wait
        /// </summary>
        private ManualResetEvent syncEvent = new ManualResetEvent(true);

        public void StartNotification()
        {
            
            ZP_DD_NOTIFY_SETTINGS settings = new ZP_DD_NOTIFY_SETTINGS();
            settings.nNMask = ZPIntf.ZP_NF_IPDEVICE;
            settings.hEvent = syncEvent.SafeWaitHandle;
            
            ZGIntf.ZG_SetNotification(ref NotifyHandle, ref settings);
            notifyWorking = true;
            
        }

        public void afs()
        {
            //ZGIntf.ZG_GetNextMessage()

        }
    }
}
