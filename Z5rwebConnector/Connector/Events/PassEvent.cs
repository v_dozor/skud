﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZGuard;

namespace ControllerConnectors
{
    public class PassEvent : IControllerEvent
    {
        /// <summary>
        /// Create new pass event
        /// </summary>
        /// <param name="rawData">Event raw data</param>
        /// <param name="controllerHandle"></param>
        /// <param name="code">Code of event e.g. ZG_EV_BUT_OPEN</param>
        public PassEvent(byte[] rawData, ZG_CTR_EV_TYPE type = 0, DateTime date = new DateTime(), string msg = "", byte[] rfid = null, ZG_CTR_DIRECT direct = ZG_CTR_DIRECT.ZG_DIRECT_UNDEF, BankUsage bank = BankUsage.Undefined, int keyIndex = -1)
        {
            if (this.rawData == null || this.rawData.Length == 0)
                this.rawData = new byte[8];
            Array.Copy(rawData, this.rawData, rawData.Length);

            this.eventType = type;
            this.date = date;
            this.message = msg;

            if (this.rfidKey == null || this.rfidKey.Length == 0)
                this.rfidKey = new byte[16];
            if(rfid != null)
                Array.Copy(rfid, rfidKey, rfid.Length);

            this.direct = direct;

            this.bank = bank;
            this.keyIndex = keyIndex;
        }
        
        private DateTime date;
        public DateTime Date
        {
            get
            {
                return date;
            }
        }

        EventFuncType eventFuncType = EventFuncType.Pass;
        public EventFuncType EventFuncType
        {
            get
            {
                return eventFuncType;
            }
        }

        private byte[] rawData;
        public byte[] RawData
        {
            get
            {
                return rawData;
            }
        }

        private ZG_CTR_DIRECT direct;
        public ZG_CTR_DIRECT Direct
        {
            get
            {
                return direct;
            }
        }

        private int keyIndex = -1;
        public int KeyIndex
        {
            get
            {
                return keyIndex;
            }
        }

        private BankUsage bank = BankUsage.Undefined;
        public BankUsage Bank
        {
            get
            {
                return bank;
            }
        }

        string message;
        public string Message
        {
            get
            {
                return message;
            }
        }

        private ZG_CTR_EV_TYPE eventType;
        public ZG_CTR_EV_TYPE EventType
        {
            get
            {
                return eventType;
            }
        }

        public string EventTypeString
        {
            get
            {
                return Z5rwebConnector.EventString(this.eventType);
            }
        }

        private byte[] rfidKey;
        public byte[] RfidKey
        {
            get
            {
                return rfidKey;
            }
        }

        public string RfidKeyString
        {
            get
            {
                return IdToString(rfidKey, "-");
            }
        }
        public override string ToString()
        {
            return
                "RFID: " + String.Concat(Array.ConvertAll(rfidKey, x => x.ToString("X2") + " ")) + Environment.NewLine +
                "Направление прохода: " + (direct == ZG_CTR_DIRECT.ZG_DIRECT_IN ? "Зашел" : (direct == ZG_CTR_DIRECT.ZG_DIRECT_OUT) ? "Вышел" : "Неизвестно") +
                "Тип события: " + message;
        }
        public static string IdToString(byte[] id, string separator)
        {
            string str = String.Concat(Array.ConvertAll(id, x => x.ToString("X2") + "-"));
            if (str.Length > 1)
            {
                str = str.Remove(str.Length - 1, 1);
            }
            return str;
        }
    }
}