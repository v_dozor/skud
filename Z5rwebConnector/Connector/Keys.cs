﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ZGuard;
using ZPort;
using System.Threading;
using System.Runtime.InteropServices;

namespace ControllerConnectors
{
    public partial class Z5rwebConnector
    {
        #region fields

        public Dictionary<Byte[], KeyValuePair<int, ZG_CTR_KEY>> BankBuffer0 { private set; get; }
        public Dictionary<Byte[], KeyValuePair<int, ZG_CTR_KEY>> BankBuffer1 { private set; get; }

        private int bankCount = 2;
        private int bankMaxId = 8167;
        private int bankCapacity = 8168;

        private int readItemsCapacity = 12;
        public int ReadItemsCapacity
        {
            get { return readItemsCapacity; }
        }

        private int writeItemsCapacity = 12;
        public int WriteItemsCapacity
        {
            get { return writeItemsCapacity; }
        }

        private int tryCountCnvErr = 5;
        /// <summary>
        /// Max count of tyring to get\set data from\to controller
        /// </summary>
        public int TryCountCnvErr
        {
            get
            {
                return tryCountCnvErr;
            }
            set
            {
                tryCountCnvErr = value;
            }
        }

        private int tryErrTimeout = 1000;
        /// <summary>
        /// Timeout between tryings get\write data to\from controller (milliseconds)
        /// </summary>
        public int TryErrTimeout
        {
            get
            {
                return tryErrTimeout;
            }
            set
            {
                tryErrTimeout = value;
            }
        }

        #endregion

        #region write

        /// <summary>
        /// Wrapper
        /// </summary>
        private void WriteKeys(IntPtr hHandle, int nIdx, [In, Out] ZG_CTR_KEY[] pKeys, int nCount,
            ZG_PROCESSCALLBACK pfnCallback, IntPtr pUserData = default(IntPtr), int nBankN = 0, bool fUpdateTop = true)
        {
            int hr = 0;
            for (int i = 0; i < tryCountCnvErr; ++i)
            {
                hr = ZGIntf.ZG_Ctr_WriteKeys(hHandle, nIdx, pKeys, nCount, pfnCallback, pUserData, nBankN, fUpdateTop);
                if (hr != 0 && i == tryCountCnvErr - 1)
                {
                    HandleError(hr);
                }
                if (hr == 0)
                {
                    i = tryCountCnvErr;
                }
                if(tryErrTimeout > 0 && hr != 0 && i < tryCountCnvErr - 1)
                {
                    Thread.Sleep(tryErrTimeout);
                }
            }
        }

        private ZG_CTR_KEY[] ConvertKeys(byte[][] keys, KeyType keyType, KeyAccess keyAccess, KeyFlags keyFlags)
        {
            ZG_CTR_KEY[] aKeys = new ZG_CTR_KEY[keys.Length];
            int keyNum = 0;
            foreach (byte[] item in keys)
            {
                aKeys[keyNum].fErased = false;
                if (aKeys[keyNum].rNum == null || aKeys[keyNum].rNum.Length != 16)
                    aKeys[keyNum].rNum = new byte[16];
                Array.Copy(item, aKeys[keyNum].rNum, item.Length);
                aKeys[keyNum].nType = (ZG_CTR_KEY_TYPE)keyType;
                aKeys[keyNum].nFlags = (uint)keyFlags;
                aKeys[keyNum].nAccess = (uint)keyAccess;
                ++keyNum;
            }
            return aKeys;
        }

        /// <summary>
        /// Add new key to controller bank(memory bank)
        /// </summary>
        /// <param name="keys">Rfid keys array</param>
        /// <param name="bank">Bank selection</param>
        /// <param name="keyType">Type of key, blocking by default</param>
        /// <param name="keyAccess">Access level</param>
        /// <param name="keyFlags">Additional options, if you need more then one option use | operator</param>
        /// <param name="keyId">Inner key id, if id = -1 then method automaticaly will find avalible key</param>
        public void WriteKey(byte[][] keys16, BankUsage bank = BankUsage.BothBanks, KeyType keyType = KeyType.Normal, KeyAccess keyAccess = KeyAccess.Free, KeyFlags keyFlags = KeyFlags.Default, int keyId = -1)
        {
            int topKeyBank0 = keyId == -1 ? GetTopKey(0) : keyId;
            int topKeyBank1 = keyId == -1 ? GetTopKey(1) : keyId;
            //converting keys to lib's format
            ZG_CTR_KEY[] keysZ5R = ConvertKeys(keys16, keyType, keyAccess, keyFlags);
            IEnumerable<ZG_CTR_KEY> aKeys = null;

            if (bank == BankUsage.FirstBank || bank == BankUsage.BothBanks)
            {
                //filter keys, keys that already added will be ignored
                aKeys = keysZ5R
                    .Where((x) => { return !BankBuffer0.Keys.Contains(x.rNum); });
                if (aKeys.Count() > 0)
                {
                    WriteKeys(ControllerHandle, topKeyBank0, aKeys.ToArray(), aKeys.Count(), null, IntPtr.Zero, 0);

                    byte[] tmpKey = null;
                    int indexBank0 = topKeyBank0;
                    //adding key to buffer
                    foreach (ZG_CTR_KEY item in aKeys)
                    {
                        tmpKey = item.rNum.Select((x) => { return x; }).ToArray();
                        BankBuffer0.Add(tmpKey, new KeyValuePair<int, ZG_CTR_KEY>(indexBank0, item));
                        ++indexBank0;
                    }
                }
            }
            if (bank == BankUsage.SecondBank || bank == BankUsage.BothBanks)
            {
                aKeys = keysZ5R
                    .Where((x) => { return !BankBuffer1.Keys.Contains(x.rNum); });
                if (aKeys.Count() > 0)
                { 
                    WriteKeys(ControllerHandle, topKeyBank1, aKeys.ToArray(), aKeys.Count(), null, IntPtr.Zero, 1);
                    byte[] tmpKey = null;
                    int indexBank1 = topKeyBank1;
                    //adding key to buffer
                    foreach (ZG_CTR_KEY item in aKeys)
                    {
                        tmpKey = item.rNum.Select((x) => { return x; }).ToArray();
                        if (!BankBuffer1.ContainsKey(tmpKey))
                        { 
                            BankBuffer1.Add(tmpKey, new KeyValuePair<int, ZG_CTR_KEY>(indexBank1, item));
                            ++indexBank1;
                        }
                    }
                }
            }
        }

        public void WriteLastReaded()
        {
            byte[] key = new byte[16];
            HandleError(ZGIntf.ZG_Ctr_ReadLastKeyNum(ControllerHandle, key));

            WriteKey(new byte[][] { key });
        }

        #endregion

        #region delete

        /// <summary>
        /// Wrapper
        /// </summary>
        private void ClearKeys(IntPtr hHandle, int nIdx, int nCount, ZG_PROCESSCALLBACK pfnCallback, IntPtr pUserData = default(IntPtr), int nBankN = 0, bool fUpdateTop = true)
        {
            int hr = 0;
            for (int i = 0; i < tryCountCnvErr; ++i)
            {
                hr = ZGIntf.ZG_Ctr_ClearKeys(hHandle, nIdx, nCount, pfnCallback, pUserData, nBankN, fUpdateTop);
                if (hr != 0 && i == tryCountCnvErr - 1)
                {
                    HandleError(hr);
                }
                if (hr == 0)
                {
                    i = tryCountCnvErr;
                }
                if (tryErrTimeout > 0 && hr != 0 && i < tryCountCnvErr - 1)
                {
                    Thread.Sleep(tryErrTimeout);
                }
            }
        }

        public bool ClearKey(byte[] val, BankUsage bank = BankUsage.BothBanks)
        {
            byte[] val16 = new byte[16];
            val.CopyTo(val16, 0);
            if (bank == BankUsage.FirstBank || bank == BankUsage.BothBanks)
            {
                if (BankBuffer0.ContainsKey(val16))
                {
                    ClearKeys(ControllerHandle, BankBuffer0[val16].Key, 1, null, IntPtr.Zero, 0);
                    BankBuffer0.Remove(val16);
                }
            }
            if (bank == BankUsage.SecondBank || bank == BankUsage.BothBanks)
            {
                if (BankBuffer1.ContainsKey(val16))
                {
                    ClearKeys(ControllerHandle, BankBuffer1[val16].Key, 1, null, IntPtr.Zero, 1);
                    BankBuffer1.Remove(val16);
                }
            }

            return true;
        }
        
        public void ClearAllKeys(BankUsage bank)
        {
            for(int i = 0; i < 2; ++i)
            {
                if(bank == BankUsage.BothBanks || i == 0 && bank == BankUsage.FirstBank || i == 1 && bank == BankUsage.SecondBank)
                {
                    int j = 0;
                    int BlockCount = writeItemsCapacity;
                    int topKey0 = GetTopKey(0);
                    int topKey1 = GetTopKey(1);
                    int topKey = (i == 0 ? topKey0 : topKey1);
                    int ReadRest = 0;
                    for (j = 0; j < topKey / BlockCount + 1; j++)
                    {
                        if (j * BlockCount + BlockCount < topKey)
                        {
                            ClearKeys(ControllerHandle, j * BlockCount, BlockCount, null, IntPtr.Zero, i, true);
                        }
                        else
                        {
                            ReadRest = topKey - j * BlockCount;
                            if (ReadRest != 0)
                                ClearKeys(ControllerHandle, j * BlockCount, ReadRest, null, IntPtr.Zero, i, true);
                        }
                    }
                }
            }
            BankBuffer0.Clear();
            BankBuffer1.Clear();
        }

        #endregion

        #region read

        /// <summary>
        /// Wrapper
        /// </summary>
        private void ReadKeys(IntPtr hHandle, int nIdx, ZG_CTR_KEY[] pBuf, int nCount, ZG_PROCESSCALLBACK pfnCallback, IntPtr pUserData = default(IntPtr), int nBankN = 0, bool fUpdateTop = true)
        {
            int hr = 0;
            for (int i = 0; i < tryCountCnvErr; ++i)
            {
                hr = ZGIntf.ZG_Ctr_ReadKeys(hHandle, nIdx, pBuf, nCount, pfnCallback, pUserData, nBankN);
                if (hr != 0 && i == tryCountCnvErr - 1)
                {
                    HandleError(hr);
                }
                if (hr == 0)
                {
                    i = tryCountCnvErr;
                }
                if (tryErrTimeout > 0 && hr != 0 && i < tryCountCnvErr - 1)
                {
                    Thread.Sleep(tryErrTimeout);
                }
            }
        }

        /// <summary>
        /// Method-iterator that makes possible to you to get all keys
        /// It take blick of keys from controller and handle it
        /// </summary>
        /// <returns>Key item</returns>
        public IEnumerable<KeyValuePair<int, ZG_CTR_KEY>> GetKeysFromCotroller(int Bank)
        {
            int i, j, nCount;

            //key block count
            int BlockCount = writeItemsCapacity;
            //it need in case when you try to read more data than contains in controller
            int ReadRest = BlockCount;
            //inner key index
            int keyId = 0;

            int bankCount = GetTopKey(Bank);

            ZG_CTR_KEY[] aKeys = new ZG_CTR_KEY[BlockCount];
            ZG_CTR_KEY pKey;

            List<ZG_CTR_KEY> keys = new List<ZG_CTR_KEY>();
            //если список не пуст
            if (bankCount != 0)
            {
                //iterate all keys blocks
                for (j = 0; j < bankCount / BlockCount + 1; j++)
                {
                    if (j * BlockCount + BlockCount < bankCount)
                    {
                        ReadKeys(ControllerHandle, j * BlockCount, aKeys, BlockCount, null, IntPtr.Zero, Bank);
                    }
                    else
                    {
                        ReadRest = bankCount - j * BlockCount;
                        if (ReadRest == 0)
                            yield break;
                        aKeys = new ZG_CTR_KEY[ReadRest];
                        ReadKeys(ControllerHandle, j * BlockCount, aKeys, ReadRest, null, IntPtr.Zero, Bank);
                    }

                    //iterate keys block
                    foreach (ZG_CTR_KEY item in aKeys)
                    {
                        //check if key is not empty or erased
                        if (item.rNum.Skip(1).Max() > 0 && item.fErased == false)
                        {
                            yield return new KeyValuePair<int, ZG_CTR_KEY>(keyId, item);
                        }
                        //Array.Clear(item.rNum, 0, item.rNum.Length);
                        ++keyId;
                    }
                }
            }
            else
            {
                yield break;
            }

            yield break;
        }

        public void RefreshBuffers()
        {
            BankBuffer0.Clear();
            BankBuffer1.Clear();
            int bank0_indx = 0;
            int bank1_indx = 0;
            foreach (var item in GetKeysFromCotroller(0))
            {
                if (!BankBuffer0.ContainsKey(item.Value.rNum))
                {
                    BankBuffer0.Add(item.Value.rNum.Select((x) => { return x; }).ToArray(), item);
                }
            }
            foreach (var item in GetKeysFromCotroller(1))
            {
                if (!BankBuffer1.ContainsKey(item.Value.rNum))
                {
                    BankBuffer1.Add(item.Value.rNum.Select((x) => { return x; }).ToArray(), item);
                }
            }
        }
        
        /// <summary>
        /// In this position and above we will see erased keys
        /// </summary>
        /// <param name="bankNumber"></param>
        /// <returns>Position of upper keys bound</returns>
        public int GetTopKey(int bankNumber)
        {
            int keyId = 0;
            HandleError(ZGIntf.ZG_Ctr_GetKeyTopIndex(ControllerHandle, ref keyId, bankNumber));
            return keyId;
        }

        #endregion

     }
}
