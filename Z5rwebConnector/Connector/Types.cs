﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ControllerConnectors
{
    /// <summary>
    /// Default - no flag
    /// Functional - used to switch mode
    /// Dual - controller will check 2 key numbers or key number and digital code
    /// ShortNumber - short number, controller will check only first 3 bytes
    /// </summary>
    public enum KeyFlags : uint { Default = 0, Functional = 2, Dual = 4, ShortNumber = 0x20 }

    /// <summary>
    /// Free - You can pass with out aim on schedule
    /// Blocked - Access denied
    /// TimeZone1 - Access avalible at time zone (1 ... 6)
    /// </summary>
    public enum KeyAccess : uint
    {
        Free = 0xFF,
        Blocked = 0x00,
        TimeZone1 = 0x01,
        TimeZone2 = 0x03,
        TimeZone3 = 0x07,
        TimeZone4 = 0x0F,
        TimeZone5 = 0x1F,
        TimeZone6 = 0x3F,
        TimeZone7 = 0x7F
    }

    /// <summary>
    /// Type of rfid-key
    /// </summary>
    public enum KeyType
    {
        Unknown = 0,
        Normal,
        Blocking,
        Master
    }

    public enum BankUsage : int { FirstBank = 0, SecondBank = 1, BothBanks = 2, Undefined }

    class ByteArrayComparer : IEqualityComparer<byte[]>
    {
        public bool Equals(byte[] x, byte[] y)
        {            
            return x.SequenceEqual(y);
        }

        public int GetHashCode(byte[] obj)
        {
            return obj.Sum(b => b);
        }
    }

    /// <summary>
    /// Type of function that should use to decode Event
    /// </summary>
    public enum EventFuncType
    {
        Pass = 0,
        UnknownKey,
        Electro,
        Mode,
        Fire,
        Secure,
        Hotel
    }
}
