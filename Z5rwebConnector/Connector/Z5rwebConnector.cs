﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ZGuard;
using ZPort;
using System.Threading;
using System.Runtime.InteropServices;

namespace ControllerConnectors
{
    public partial class Z5rwebConnector
    {
        #region properties
        private IntPtr ConverterHandle;
        private IntPtr ControllerHandle;

        private bool proximity = false;
        /// <summary>
        /// Checking if proximity mode enabled now
        /// </summary>
        public bool Proximity {
            get { return proximity; }
            set { proximity = value; }
        }

        private IPAddress ip;
        public IPAddress Ip
        {
            get { return ip; }
            set { ip = value; }
        }

        private ushort serial;
        public ushort Serial
        {
            get { return serial; }
            set { serial = value; }
        }

        private int port;
        public int Port
        {
            get { return port; }
            set { port = value; }
        }

        private int maxEvents;
        public int MaxEvents
        {
            get { return maxEvents; }
        }

        private ZP_CONNECTION_STATUS connected = ZP_CONNECTION_STATUS.ZP_CS_DISCONNECTED;
        public bool Connected
        {
            get
            {
                ZP_CONNECTION_STATUS status = GetConnectionStatus();
                if (status == ZP_CONNECTION_STATUS.ZP_CS_CONNECTED)
                    return true;
                return false;
            }
        }
        #endregion

        /// <summary>
        /// Make avalible to connect and use controller Z5rweb by Ethernet
        /// </summary>
        /// <param name="ip">Controller Ethernet IP</param>
        /// <param name="port">Controller Ethernet port</param>
        /// <param name="serial">Controler serial number (you can find it in doc or under controller box)</param>
        public Z5rwebConnector(IPAddress ip, ushort serial, int port = 1000)
        {
            this.ip = ip;
            this.serial = serial;
            this.port = port;

            // Checking sdk version
            UInt32 nVersion = ZGIntf.ZG_GetVersion();
            if ((((nVersion & 0xFF)) != ZGIntf.ZG_SDK_VER_MAJOR) || (((nVersion >> 8) & 0xFF) != ZGIntf.ZG_SDK_VER_MINOR))
            {
                HandleError(0,"Версия SDK некорректна");
            }
            HandleError(ZGIntf.ZG_Initialize(ZPIntf.ZP_IF_NO_MSG_LOOP));

            BankBuffer0 = new Dictionary<byte[], KeyValuePair<int, ZG_CTR_KEY>>(new ByteArrayComparer());
            BankBuffer1 = new Dictionary<byte[], KeyValuePair<int, ZG_CTR_KEY>>(new ByteArrayComparer());
            
        }

        public void Connect()
        {
            //connection to conventer
            ZG_CVT_INFO ConvInfo = new ZG_CVT_INFO();
            
            ZP_WAIT_SETTINGS ws = new ZP_WAIT_SETTINGS(
                500,
                25,
                IntPtr.Zero,
                15 * 1000,
                0,
                15 * 1000,
                15 * 1000);
            IntPtr wsPtr = Marshal.AllocHGlobal(Marshal.SizeOf(ws));
            Marshal.StructureToPtr(ws, wsPtr, false);
            ZG_CVT_OPEN_PARAMS ConvOpenParams = new ZG_CVT_OPEN_PARAMS();
            ConvOpenParams.nPortType = ZP_PORT_TYPE.ZP_PORT_IP;
            ConvOpenParams.pszName = ip.ToString() + ":" + port;
            ConvOpenParams.nCvtType = ZG_CVT_TYPE.ZG_CVT_Z5R_WEB;
            ConvOpenParams.pWait = wsPtr;
            HandleError(ZGIntf.ZG_Cvt_Open(ref ConverterHandle, ref ConvOpenParams, ConvInfo));
            //connection to controller
            ZG_CTR_INFO ContrInfo = new ZG_CTR_INFO();
            HandleError(ZGIntf.ZG_Ctr_Open(ref ControllerHandle, ConverterHandle, 0xFF, serial, ref ContrInfo));

            proximity = ((ContrInfo.nFlags & ZGIntf.ZG_CTR_F_PROXIMITY) != 0);
            bankCapacity = ContrInfo.nMaxKeys;
            maxEvents = ContrInfo.nMaxEvents;
            readItemsCapacity = ContrInfo.nOptReadItems;
            writeItemsCapacity = ContrInfo.nOptWriteItems;

            connected = ZP_CONNECTION_STATUS.ZP_CS_CONNECTED;
            if(Connected)
            {
                RefreshBuffers();
            }
        }

        public void Disconnect()
        {
            if (connected == ZP_CONNECTION_STATUS.ZP_CS_DISCONNECTED)
                return;
            else
                connected = ZP_CONNECTION_STATUS.ZP_CS_CONNECTED;
            //IntPtr t = IntPtr.Zero;
            //ZGIntf.ZG_Cvt_DettachPort(ConverterHandle, ref t);
            HandleError(ZGIntf.ZG_CloseHandle(ControllerHandle));
            HandleError(ZGIntf.ZG_CloseHandle(ConverterHandle));
            connected = ZP_CONNECTION_STATUS.ZP_CS_DISCONNECTED;
        }

        public ZP_CONNECTION_STATUS GetConnectionStatus()
        {
            ZP_CONNECTION_STATUS status = new ZP_CONNECTION_STATUS();
            ZGIntf.ZG_Cvt_GetConnectionStatus(ConverterHandle, ref status);
            return status;
        }

        public DateTime getControllerDate()
        {
            ZG_CTR_CLOCK clock = new ZG_CTR_CLOCK();
            HandleError(ZGIntf.ZG_Ctr_GetClock(ControllerHandle, ref clock));
            DateTime date = new DateTime(clock.nYear, clock.nMonth, clock.nDay, clock.nHour, clock.nMinute, clock.nSecond, 0, DateTimeKind.Utc);
            return date;
        }

        public bool IsControllerClockStopped()
        {
            ZG_CTR_CLOCK clock = new ZG_CTR_CLOCK();
            HandleError(ZGIntf.ZG_Ctr_GetClock(ControllerHandle, ref clock));
            return clock.fStopped;
        }

        public void SetControllerMode(ZG_CTR_MODE ControllerMode)
        {
            HandleError(ZGIntf.ZG_Ctr_SetCtrMode(ControllerHandle, ControllerMode));
        }

        public ZG_CTR_MODE GetControllerMode()
        {
            ZG_CTR_MODE ControllerMode = new ZG_CTR_MODE();

            uint flags = 0;
            HandleError(ZGIntf.ZG_Ctr_GetCtrModeInfo(ControllerHandle, ref ControllerMode, ref flags));
            return ControllerMode;
        }

        public void setControllerDate(DateTime date, bool stoped = false)
        {
            ZG_CTR_CLOCK clock = new ZG_CTR_CLOCK();
            clock.nYear = (ushort)date.Year;
            clock.nMonth = (ushort)date.Month;
            clock.nDay = (ushort)date.Day;
            clock.nHour = (ushort)date.Hour;
            clock.nMinute = (ushort)date.Minute;
            clock.nSecond = (ushort)date.Second;
            clock.fStopped = stoped;
            HandleError(ZGIntf.ZG_Ctr_SetClock(ControllerHandle, ref clock));
        }

        public void OpenLock(int lockNumber = 0)
        {
            HandleError(ZGIntf.ZG_Ctr_OpenLock(ControllerHandle, lockNumber));
        }

        public void CloseLock()
        {
            HandleError(ZGIntf.ZG_Ctr_CloseLock(ControllerHandle));
        }
    }
}
