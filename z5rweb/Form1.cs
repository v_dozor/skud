﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ControllerConnectors;
using ZGuard;
using ZPort;
using System.Globalization;
using SKUDDatabase.DataModels;
using SKUDDatabase;
using System.Net;
using Votum.Types;

namespace z5rweb
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Z5rwebConnector Connector;

        //byte[] test_key_0 = { 0x34, 0x9F, 0xB8, 0x81, 0x88, 0x88, 0x88, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        byte[] test_key_0 = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        byte[] test_key_1 = { 6, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        byte[] test_key_2 = { 6, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        byte[] test_key_3 = { 6, 3, 3, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        byte[] test_key_4 = { 6, 4, 4, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        byte[] test_key_5 = { 6, 5, 5, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        byte[] test_key_6 = { 6, 6, 6, 6, 6, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        byte[] test_key_7 = { 6, 7, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        byte[] test_key_8 = { 6, 8, 8, 8, 8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        byte[] test_key_9 = { 6, 9, 9, 9, 9, 9, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        byte[] test_key_10 = { 6, 10, 10, 10, 10, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        byte[] test_key_11 = { 6, 11, 11, 11, 11, 11, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        byte[] test_key_12 = { 6, 12, 12, 12, 12, 12, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        private void Form1_Load(object sender, EventArgs e)
        {
            Connector = new Z5rwebConnector(new IPAddress(new byte[] { 1, 1, 1, 1 }), 040760);
            RfidLabelZ5R label = new RfidLabelZ5R(test_key_0);
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Connector.WriteKey(
                new byte[][] {
                    test_key_1, test_key_2, test_key_3, test_key_4, test_key_5,
                    test_key_6, test_key_7, test_key_8, test_key_9, test_key_10, test_key_11,
                    test_key_12
                });
            return;
            byte j = 0;
            byte p = 0;
            byte t = 0;
            byte y = 0;
            for(int i = 0; i < 1000; ++i)
            { 
                Connector.WriteKey(
                    new byte[][] {
                        new byte[] { 6, j++, p++, p, t, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                        new byte[] { 6, j++, t++, t, p, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                        new byte[] { 6, j++, p++, y, y, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                        new byte[] { 6, j++, t++, p, y, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                        new byte[] { 6, j++, p++, t, p, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                        new byte[] { 6, j++, t++, y++, y, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
                });
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            textBox1.Text = "Bank0" + Environment.NewLine;
            foreach (var item in Connector.BankBuffer0)
            {
                string str = "";
                foreach (var val in item.Key)
                {
                    str += val.ToString("X2") + " ";
                }
                textBox1.Text += item.Value.Key + " " + str + Environment.NewLine;
            }

            textBox1.Text += "Bank1" + Environment.NewLine;
            foreach (var item in Connector.BankBuffer1)
            {
                string str = "";
                foreach (var val in item.Key)
                {
                    str += val.ToString("X2") + " ";
                }
                textBox1.Text += item.Value.Key + " " + str + Environment.NewLine;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Connector.ClearAllKeys(BankUsage.BothBanks);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Connector.WriteLastReaded();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Connector.SetControllerMode(ZG_CTR_MODE.ZG_MODE_BLOCK);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ZG_CTR_MODE mode = Connector.GetControllerMode();
            if(mode == ZG_CTR_MODE.ZG_MODE_BLOCK)
            {
                textBox1.Text = "Включен болкирующий режим";
            }
            else if(mode == ZG_CTR_MODE.ZG_MODE_FREE)
            {
                textBox1.Text = "Включен свободнй режим";
            }
            else if (mode == ZG_CTR_MODE.ZG_MODE_NORMAL)
            {
                textBox1.Text = "Включен обычный режим";
            }
            else if (mode == ZG_CTR_MODE.ZG_MODE_UNDEF)
            {
                textBox1.Text = "Неизвестный режим";
            }
            else if (mode == ZG_CTR_MODE.ZG_MODE_WAIT)
            {
                textBox1.Text = "Включен ожидащий режим";
            }
            else
            {
                textBox1.Text = "Неизвестный режим";
            }

        }

        private void button7_Click(object sender, EventArgs e)
        {
            Connector.SetControllerMode(ZG_CTR_MODE.ZG_MODE_FREE);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Connector.SetControllerMode(ZG_CTR_MODE.ZG_MODE_NORMAL);
        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        private void button9_Click_1(object sender, EventArgs e)
        {
            try
            {
                Connector.ClearKey(
                    textBox2.Text.Split(new char[] { ' ' }).
                    Select(v => byte.Parse(v, NumberStyles.HexNumber)).ToArray<byte>());
            }
            catch(Exception ex)
            {

            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            textBox1.Text = Connector.getControllerDate().ToString();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Connector.setControllerDate(DateTime.Now);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Connector.OpenLock(0);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Connector.CloseLock();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            foreach (ZG_CTR_EVENT ev in Connector.GetEventsFromCotroller())
            {
                textBox1.Text += Z5rwebConnector.EventString(ev.nType) + Environment.NewLine;
            }
            return;

            SKUDDatabaseConnection dbCon = new SKUDDatabaseConnection(new IPAddress(new byte[] { 127, 0 ,0, 1 }).ToString(), 3306, false);
            Connector.RefreshEventBuffer();
            Connector.EventsBuffer.Add(0, new PassEvent(new byte[] { 1 }, ZG_CTR_EV_TYPE.ZG_EV_PASSAGE, DateTime.Now, "", jar_key_1, ZG_CTR_DIRECT.ZG_DIRECT_IN));
            Connector.EventsBuffer.Add(1, new PassEvent(new byte[] { 2 }, ZG_CTR_EV_TYPE.ZG_EV_PASSAGE, DateTime.Now, "", jar_key_2, ZG_CTR_DIRECT.ZG_DIRECT_OUT));
            IEnumerable<Log> passageEvents = Connector.EventsBuffer.Values
                .Where((x) => { return x.EventType == ZG_CTR_EV_TYPE.ZG_EV_PASSAGE; })
                .Select<IControllerEvent, Log>((x) =>
                {
                    PassEvent pe = (PassEvent)x;
                    string dir = (pe.Direct == ZG_CTR_DIRECT.ZG_DIRECT_IN ? "IN" : (pe.Direct == ZG_CTR_DIRECT.ZG_DIRECT_OUT ? "OUT" : "UNDEFINED"));
                    return new Log(0, pe.RfidKeyString, "040763", (int)pe.EventType, pe.EventTypeString, pe.Date, pe.ToString(), dir);
                });

            if (passageEvents.Count() != 0)
                dbCon.AddLogRange(passageEvents);
            return;
            textBox1.Text = "";
        }

        private void button15_Click(object sender, EventArgs e)
        {
            Connector.ClearControllerEventBuffer();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            Connector.RefreshEventBuffer();
            textBox1.Text = "";
            foreach(var a in Connector.GetEventsByType(ZG_CTR_EV_TYPE.ZG_EV_PASSAGE))
            {
                PassEvent ptr = (PassEvent)a;
                textBox1.Text +=
                    ptr.Message +
                    String.Concat(Array.ConvertAll(ptr.RfidKey, x => x.ToString("X2") + " ")) +
                    Environment.NewLine;
            }
        }
        byte[] jar_key_1 = { 6, 0x24, 0x8B, 0x99, 0x4B, 0x46, 0x56, 0, 0, 0, 0, 0, 0, 0, 0 };
        byte[] jar_key_2 = { 6, 0x43, 0xB8, 0xB1, 0xE9, 0x2E, 0x56, 0, 0, 0, 0, 0, 0, 0, 0 };

        byte[] jar_key_3 = { 6, 0x4B, 0x90, 0xA1, 0x87, 0x58, 0x56, 0, 0, 0, 0, 0, 0, 0, 0 };
        byte[] jar_key_4 = { 6, 0x43, 0xB8, 0x89, 0x21, 0xA6, 0x56, 0, 0, 0, 0, 0, 0, 0, 0 };
        byte[] jar_key_5 = { 6, 0x43, 0xB8, 0x89, 0x47, 0x50, 0x56, 0, 0, 0, 0, 0, 0, 0, 0 };
        byte[] jar_key_6 = { 6, 0x43, 0xB8, 0x39, 0x04, 0x42, 0x57, 0, 0, 0, 0, 0, 0, 0, 0 };
        byte[] jar_key_7 = { 6, 0x9F, 0xB8, 0x81, 0xE0, 0xBD, 0x46, 0, 0, 0, 0, 0, 0, 0, 0 };


        private void button17_Click(object sender, EventArgs e)
        {
            Connector.WriteKey(new byte[][] { jar_key_1, jar_key_2, jar_key_3, jar_key_4, jar_key_5, jar_key_6, jar_key_7 }, BankUsage.BothBanks, KeyType.Blocking);
        }

        private void button18_Click(object sender, EventArgs e)
        {
            if (!Connector.Connected)
            {
                Connector.Ip = IPAddress.Parse(CIP.Text);
                Connector.Serial = UInt16.Parse(CSerial.Text);
                Connector.Connect();
                textBox1.Text = "Соединен";
            }
            else
            {
                textBox1.Text = "Вы уже подключены к контроллеру";
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            if (Connector.Connected)
            {
                Connector.Disconnect();
                textBox1.Text = "Отключен";
            }
            else
            {
                textBox1.Text = "Вы не подключены к контроллеру";
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            Connector.RefreshBuffers();
            button2_Click(null, null);
        }

        private void button21_Click(object sender, EventArgs e)
        {
            Connector.WriteKey(new byte[][] {
                new byte[] {0x34, 0x3B, 0xF0, 0x41, 0xC0, 0xC2, 0x56, 0, 0, 0, 0, 0, 0, 0, 0 }
            });
        }

        private void button22_Click(object sender, EventArgs e)
        {
            Connector.WriteKey(new byte[][] { test_key_1 });
        }

        private void listBox1_Click(object sender, EventArgs e)
        {
            string[] tmp = listBox1.SelectedItem.ToString().Split(' ');
            if(tmp != null && tmp[0] != "")
            {
                CIP.Text = tmp[1];
                CSerial.Text = tmp[0];
            }
        }

        private void CIP_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
